{
 :transactor-uri "datomic:sql://patdb?jdbc:postgresql://localhost:5432/datomic?user=datomic&password=datomic"
 :schema-file "resources/schema.edn"
 :user-schema-file "resources/user-schema.edn"
 :transactor-dir (utils.fileUtils/get-path [(System/getProperty "user.dir") ".." "external" "datomic-pro-0.9.5350"])
 :transactor-props "sql-transactor.properties"
 :transactor-log   "/tmp/transactor.txt"
 :pgres-dir "/usr/local/var/postgres"
 :pgres-log "/tmp/pgress.log"
 }
 
