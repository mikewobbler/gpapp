(ns mysql-if.core
  (:require [clojure.java.jdbc :as jdbc])
  )

(def db-spec
  {:classname "com.mysql.jdbc.Driver"
   :subprotocol "mysql"
   :subname "//localhost/testdb"
   :user "root"}
  )

(defn get-names []
  (jdbc/query db-spec ["SELECT * FROM person_name LIMIT 5"]))

(defn get-table [tname]
  (let
      [sql-stat (str "SELECT * FROM " tname " LIMIT 5;")]
    (println sql-stat)
    (jdbc/query db-spec sql-stat)))

(defn get-field [tname fname]
  (let
      [sql-stat (str "SELECT " fname " FROM " tname ";")]
    (println sql-stat)
    (jdbc/query db-spec sql-stat)))


(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
