(defproject patient_db "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories {"my.datomic.com" {:url "https://my.datomic.com/repo"
                                 :creds :gpg}}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.datomic/datomic-pro "0.9.5350"]
                 [expectations "2.1.4"]
                 [clojure.joda-time "0.6.0"]
                 [org.clojure/data.csv "0.1.3"]
                 [me.raynes/fs "1.4.6"]
                 [postgresql "9.3-1102.jdbc41"]
                 [com.taoensso/timbre "4.3.1"]
                 ;;[clojure.java-time "0.2.0"]
                 ;; To run on Datomic Pro, comment out the free
                 ;; version above, and enable the pro version below
                 ;;#_[com.datomic/datomic-pro "0.9.5130"]
                 ]
  :plugins [[lein-autoexpect "1.8.0"]
            [lein-checkouts "1.1.0"]]
  :source-paths ["src" "examples/clj"]
  :jvm-opts ^:replace ["-Xmx2g" "-server"]
  :main patient-db.patdb
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  )
