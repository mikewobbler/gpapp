(ns patient-db.core
  (:require [clojure.pprint :refer (pprint)]
            [patient-db.patdb :as pdb]
            [patient-db.handle-csv :as hcsv]
            )
  (:gen-class))

(pdb/set-conn (pdb/create-empty-in-memory-db))

(defn insert-patient
  "Insert a patient from the patient record pr."
  [pr]
  (let
      [pm {
           :first-name (:first_name pr)
           :last-name (:last_name pr)
           :title (:title pr)
           :gender (:gender pr)
           :phone (:phone pr)
           :street-address (:street-address pr)
           :city (:city pr)
           :state (:state pr)
           :country (:country pr)
           :post-code (:post-code pr)
           :email (:email pr)
           :medicare-number (:medicare-number pr)
           :dob (:dob pr)
           :start-date (:start-date pr)
           :consult-notes (:consult-notes pr)
           :test-results (:test-results pr)
           :diagnosis (:diagnosis pr)           
           :correspondence (:correspondence pr)
           :treatment-desc (:treatment-desc pr)
           :billing (:billing pr)
           }
       ]
       (pdb/add-patient pm)))

(defn populate-db
  "Populate the dataomic patient database from data in a csv file."
  [fname]
  (let
      [patient-recs (hcsv/get-patients fname)]
    (doseq [pr patient-recs]
            (insert-patient pr))))
