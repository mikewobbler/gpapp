(ns patient-db.handle-csv
  (:require [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :refer (pprint)]
            [clojure.string :as cstr]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io])
  (:gen-class))

(defrecord Patient
    [first_name last_name title gender phone street-address city
     state country post-code email medicare-number dob start-date
     consult-notes test-results diagnosis correspondence  
     treatment-desc billing])

(defn get-patients
  [fname]
  (with-open [in-file (io/reader fname)]
    (map (partial apply ->Patient)
         (rest 
          (doall
           (csv/read-csv in-file))))))



