(defproject scheduling "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.0"]
                 [ring-server "0.4.0"]
                 [liberator "0.14.0"]
                 [cheshire "5.5.0"]
                 [lib-noir "0.9.9"]
                 [selmer "1.0.4"]
                 [environ "1.0.2"]
                 [mount "0.1.10"]
                 [com.datomic/datomic-free "0.9.5130" :exclusions [joda-time]]
                 [com.taoensso/timbre "4.3.1"]
                 [org.clojure/data.csv "0.1.3"]
                 [me.raynes/fs "1.4.6"]
                 [clj-jgit "0.8.8"]                 
                 [clj-time "0.11.0"]
                 ;; get rid of these
                 [org.clojure/java.jdbc "0.6.0-alpha2"]
                 [postgresql "9.3-1102.jdbc41"]
                 [org.clojars.hozumi/clj-commons-exec "1.2.0"]
                 ]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler scheduling.handler/app
         :init scheduling.handler/init
         :destroy scheduling.handler/destroy
         :port 5000}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
