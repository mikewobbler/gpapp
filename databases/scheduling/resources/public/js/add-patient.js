define(['jquery', 'underscore', 'bootstrap', 'jquery.maskedinput'], function(require) {
    console.log("In add-patient.js");

    np = {};
    np.duplicateNames = [];
    np.duplicateMedicare = [];
    np.setupAddPatient = function() {
	console.log("In np.setupAddPatient");
	// get a list of patient names and medicare numbers to prevent adding duplicates.
	$.ajax
	(
	    "/get-patients",
	    {
		dataType: "json",
		cache : false
	    }
	).done(np.setupValidation);
    };

    function checkForm(evt) {
	console.log("In check Form...");
	var formDivs = $(".form-group > div");
	var valMap = _.map
	(
	    formDivs,
	    function(v) {
		var label = $("> label", v);
		if (label.length == 0)
		{
		    return null;
		}
		var labelVal = label.text();
		if (labelVal == "Patient Gender:") {
		    var inputs = $("input:checked", v);
		    var value = inputs.length > 0 ? inputs[0].value : "None";
		    return {l : labelVal,
			    v : value};
		}
		else {
		    var value = $("input", v)[0].value;
		    if (value.length == 0) {
			value = "None";
		    }
		    return { l : labelVal, v : value };
		}
	    }
	);
	// Get patientName value
	var pName = _.find(valMap, function(x) {
	    return x.l == "New patient name:";
	}).v;
	
	// Get .medicareNum value
	var mCare = _.find(valMap, function(x) {
	    return x.l == "Medicare Number:";
	}).v;
	
	if (-1 != _.indexOf(np.duplicateNames, {name: pName, mNum : mCare}, true))
	{
	    $("#errorDialog").modal({backdrop: 'static', show :false});
	    $("#errorMessage").html
	    (
		"Patient with name = " + pName + " and medicare number = " +
		    mCare + " already in the system.<br>"
		    + "Please check patient details again."
	    );
	    $("#errorDialog").modal('show');
	    return false;
	}
	else if (np.duplicateNames.length != 0)
	{
	    $("#addPatientDialog").modal({backdrop: 'static', show :false});
	    $("#confirmAddPatientMessage").html
	    (
		"Patient with name = " + pName + " already in the system.<br>"
		    + "Click Submit if this is correct, otherwise Cancel, and fix the form."
	    );
	    $("#addPatientDialog").modal('show');
	    return false;
	}
	else if (np.duplicateMedicare.length != 0) {
	    $("#addPatientDialog").modal({backdrop: 'static', show :false});
	    $("#confirmAddPatientMessage").html
	    (
		"There is already a patient with medicare number = " + mCare +
		    " in the system.<br>"
		    + "Click Submit if this is correct, otherwise Cancel, and fix the form."
	    );
	    $("#addPatientDialog").modal('show');
	    return false;
	}
	else {
	    // Check for empty fields, and display a confirmation message.
	    var unfilled = _.filter(valMap, function(v) {
		return (v != null) && v.v == "None"; });
	    if (unfilled.length > 0)
	    {
		var iList = "<ul>";
		_.each(unfilled, function(v) {
		    iList += "<li>" + v.l + "</li>";
		});
		iList += "</ul>";
		
		$("#addPatientDialog").modal({backdrop: 'static', show :false});

		$("#confirmAddPatientMessage").html
		(
		    "The following fields are incomplete:"
		);
		$("#confirmAddPatientMessage").append(iList);
		$("#confirmAddPatientMessage").append("<p>Confirm this is what you intend.</p>");
		
		$("#addPatientDialog").modal('show');
		return false;
	    }
	    //$("#new-patient").submit();
	    $("#confirmAddPatient").click();
	    return false;
	}
    };

    function cancelForm(evt) {
	console.log("In cancel form...");
	evt.preventDefault();
	window.history.back();
    };

    np.setupValidation = function(currP) {
	np.currentPatients =
	    _.map(currP["data-list"], function(m)
		  {
		      return { name: m["patient/fullName"],
			       mNum: m["patient/medicareNumber"]};
		  });
	np.nameSorted = _.sortBy(np.currentPatients, "name");	
	np.medicareSorted = _.sortBy(np.currentPatients, "mNum");
	var nameField = $("#patientName");
	nameField.blur(function(evt) {
	    var newName = evt.target.value;
	    np.duplicateNames = _.where(np.nameSorted, {name: newName});
	});
	var medicareField = $("#medicareNum");
	medicareField.blur(function (evt) {
	    var newMedicare = evt.target.value;
	    np.duplicateMedicare = _.where(np.medicareSorted, {mNum : newMedicare});
	});
	// Check the form when the user submits.
	$("#new-patient input[type=button").click(checkForm);
	$("#new-patient input[type=reset]").click(cancelForm);

	$("#cancelAddPatient").click
	(
	    function(evt) {
		$("#addPatientDialog").modal('hide');
		return false;
	    }
	);

	$("#confirmAddPatient").click
	(
	    function(evt) {
		evt.preventDefault();
		$("addPatientDialog").modal('hide');
		$.ajax({
		    type: "POST",
		    url: "/add-new-patient",
		    data: $("#new-patient").serialize(),
		    success: function(data) {
			$("#new-patient")[0].reset();
		    }
		});
		var pName = $("#patientName").val();
		var mCare = $("#medicareNum").val();		
		var new_val = {name: pName, mNum: mCare};
		var search_val = {name: pName.split(/\s+/).reverse().join(" "), mNum: mCare};
		var name_index = _.sortedIndex(np.nameSorted, search_val ,'name');
		np.nameSorted.splice(name_index, 0, new_val);
		var mcare_index = _.sortedIndex(np.medicareSorted, search_val, 'mNum');
		np.medicareSorted.splice(mcare_index, 0, new_val);
		return false;
	    }
	);

	$("#errorCancel").click
	(
	    function(evt) {
		$("errorDialog").modal('hide');
		return false;
	    }
	);
    };

    $(function() {
	$('[data-toggle="tooltip"]').tooltip(
	    {animation: true,
	     delay: {"show" : 500, "hide" : 100}
	    });
	$("#medicareNum").mask("9999-99999-9");
	np.setupAddPatient();
    });
});
