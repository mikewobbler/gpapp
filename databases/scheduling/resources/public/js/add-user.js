define(['jquery', 'underscore', 'bootstrap'], function(require) {
    console.log("In add-user.js");

    // Note: Will need to use setCustomValidity method for some of the validation.
    su = {};
    su.setupAddUser = function() {
	console.log("In su.setupAddUser");
	// get a list of login names and full names here.
	$.ajax
	(
	    "/current-logins",
	    {
		dataType: "json",
		cache : false
	    }
	).done(su.setupValidation);
    };

    function checkLogin(evt) {
	var newLogin = evt.target.value;
	if (_.indexOf(su.currentLogins, newLogin) == -1) {
	    su.currentLogins.push(newLogin);
	    return true;
	}
	else {
	    alert("Login " + newLogin + " has already been used.");
	    return false;
	}
    };

    function checkConfirmPassword(evt) {
	console.log("In check confirm password.");
	var ip = $("#inputPassword").val();
	var cp = $("#confirmPassword").val();
	if (ip != cp) {
	    alert("Password's don't match. Please re confirm your password.");
	    return false;
	}
    };

    function checkForm(evt) {
	var checked = $("input:checked");
	if (checked.length == 0) {
	    alert("No Role selected for user. Please select at least one role.");
	    return false;
	}
    };

    su.setupValidation = function(cl) {
	su.currentLogins = _.map(cl["login-list"], function(m) {return m["user/loginName"];})
	console.log("Current logins are: " + su.currentLogins);
	var loginField = $("#loginName");
	loginField.blur(checkLogin);
	var confirmP = $("#confirmPassword");
	confirmP.blur(checkConfirmPassword);
	$("#new-user").submit(checkForm);
    };

    $(function() {
	$('[data-toggle="tooltip"]').tooltip(
	    {animation: true,
	     delay: {"show" : 500, "hide" : 100}
	    });
	su.setupAddUser();
    });
});
