//requirejs(['widget-panel'], function(wp) {
define(['jquery', 'widget/panel', 'bootstrap'], function($, wp, bs) {
    //var wp = require('widget-panel');
    console.log("In widget-panel.js");

    // Definition of the patient list panel.
    var patient_list_panel_def = {
	title : "Patient List",
	class_name : "patient-panel",
	text : "Access the list of patients",
	link_title : "Patient List",
	href : "/patient-list",
	img : "/img/Medical-Symbol-267x300.png"
    };

    // Definition of the manage users panel.
    var manage_users_panel_def = {
	title : "Manage Users",
	class_name : "manage-users-panel",
	text : "Manage application users",
	link_title : "Manage users",
	href : "/manage-users",
	img : "/img/Custom-Icon-Design-Pretty-Office-13-Users.ico"
    };

    // Definition of the appointments calendar panel.
    var appts_panel_def = {
	title : "Schedules and Appointments",
	class_name : "appts-panel",
	text : "Manage Schedules",
	link_title : "Appointments & Schedules",
	href : "/appts-calendar",
	svg : "/img/cal_modified.svg"
    };

    //inject_html : get_date_picker_html()

    function get_date_picker_html() {
	var dp = '<div class="container-fluid" id="landing-date">';
        dp += '<div class="date-picker" data-type="year" data-date="2014/12/25">';	
	dp += '<div class="row">';
	dp += '<span data-toggle="datepicker" data-type="subtract" class="pull-left glyphicon glyphicon-triangle-left"></span>';	
        dp += '<div class="date-container date-panel col-md-8 col-md-offset-1">';
	dp += '<div class="calendar-text">';
        dp += '<p class="weekday">Monday</p>';
        dp += '<p class="date">February 4th 2014</p>';
        dp += '<p class="year"></p>';
	dp += '</div>';
	dp += '</div>';
        dp += '<span data-toggle="datepicker" class="pull-right glyphicon glyphicon-triangle-right"></span>';		     dp += '</div>';
	dp += '</div>';
	dp += '</div>';
	return dp;
    };
    
    // Create patient panel
    var patientPanel = wp.panel(null, patient_list_panel_def);
    console.log("Patient panel is:\n" + $(patientPanel.element).html());

    // Append patientPanel.
    $("#main-contents").append(patientPanel.element);

    // Create users panel
    var usersPanel = wp.panel(null, manage_users_panel_def);
    console.log("Manage users panel is:\n" + $(patientPanel.element).html());

    // Append usersPanel.
    $("#main-contents").append(usersPanel.element);

    // Create appointments
    var apptsPanel = wp.panel(null,appts_panel_def);
    console.log("Appointments panel is:\n" + $(apptsPanel.element).html());

    // Append apptsPanel.
    $("#main-contents").append(apptsPanel.element);
});
