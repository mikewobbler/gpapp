define
(
    ['require', 'jquery', 'underscore', 'bootstrap', 'd3', 'widget/userSelect',
    'widget/userSchedules'],
    function(require) {
	var ma = {};
	var u_select = require('widget/userSelect');
	var u_sched = require('widget/userSchedules');

	/**
	 * Setup the clinician roster tab.
	 */
	function setupRoster() {
	    ma.usersPanel = u_select.createPanel("selectClinician");
	    ma.usersPanel.getUsers(gotUsersCb);
	};

	/**
	 * Callback called after clinician select table has been loaded.
	 */
	function gotUsersCb() {
	    console.log("In gotUsers Callback.");
	    console.log("User Data is: " + ma.usersPanel.userData);
	};

	/**
	 * Setup the Create Schedules tab.
	 */
	function setupSchedules() {
	    ma.userScheds = u_sched.createUs("create-schedules");
	    ma.userScheds.getScheds(gotSchedulesCb);
	    return true;
	};

	function gotSchedulesCb() {
	    console.log("In callback.");
	};
	
	$(function() {
	    setupRoster();
	    setupSchedules();
	});
    }
);
