define
(
    ['require', 'jquery', 'underscore', 'bootstrap', 'd3', //'widget/userSelect',
     'widget/userRosters', 'widget/createNewRoster'],
    function(require) {
	var ma = {};
	//var u_select = require('widget/userSelect');
	var ur = require('widget/userRosters');
	var cnr = require('widget/createNewRoster');

	/**
	 * Setup the clinician roster tab.
	 */
	/*function setupRoster() {
	    ma.usersPanel = u_select.createPanel("selectClinician");
	    ma.usersPanel.getUsers(gotUsersCb);
	};*/

	/**
	 * Callback called after clinician select table has been loaded.
	 */
	/*function gotUsersCb() {
	    console.log("In gotUsers Callback.");
	    console.log("User Data is: " + ma.usersPanel.userData);
	};*/

	/**
	 * Setup the Existing Rosters tab.
	 */
	function setupRosterTable() {
	    ma.rosters = ur.createUs("rosterTable");
	    ma.rosters.getRosts(gotRostsCb);
	    return true;
	};

	function gotRostsCb() {
	    console.log("In callback.");
	};

	function setupCreateNewRoster() {
	    ma.newRostForm = cnr.createRosterForm("new-roster-tab");
	    ma.newRostForm.getSetupData(cnrSetupCb);
	};

	function cnrSetupCb() {
	    console.log("In setup create new roster form callback.");
	};
	
	$(function() {
	    setupRosterTable();
	    setupCreateNewRoster();
	});
    }
);
