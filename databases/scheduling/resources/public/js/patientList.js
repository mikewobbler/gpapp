define(['require', 'jquery', 'underscore', 'bootstrap', 'd3', 'widget/table'], function(require) {
    var ct = require('widget/table');
    var currentSelected = null;

    function renderPatients(patList) {
	//var patList = JSON.parse(pats);
	var sortField = patList["sort-key"];
	var sorted = _.sortBy(patList["data-list"], sortField);
	var route =  ""; //"/patient-record/";

	ct.createTable(
	    patList["table-id"],
	    patList["link"],
	    patList["columns"],
	    sorted,
	    route,
	    handlePatientClicked // function() { window.document.location = $(this).data("href");}
	);
	ct.addSearchHandler(patList["table-id"], "list-search", sorted, sortField);
	$("#patient-summary").css("visibility", "hidden");
    };

    function getPatients() {
	$.get("/get-patients", renderPatients, "json");
    };

    function renderSelected(pat) {
	console.log("Got:\n" + JSON.stringify(pat));
	$("#patient-fullName").text(pat["patient/fullName"] || "None");
	$("#patient-title").text(pat["patient/title"] || "None");
	$("#patient-dob").text((new Date(pat["patient/dob"])).toDateString());
	$("#patient-phone").text(pat["patient/phoneNum"].toString());
    }

    function addButtonHandlers() {
	var consultPatient = $("#consult-patient");
	var newPatient = $("#new-patient");
	var removePatient = $("#remove-patient");
	var editPatient = $("#edit-patient");

	consultPatient.click(function(evt) {
	    return (function(evt, cs) {
 		window.document.location = "/patient-record/" + cs.data("href");
	    })(evt, currentSelected);
	});
	
	newPatient.click(function(evt) {
	    window.location.href = "/add-patient";
	});

	removePatient.click(function(evt) { return rPatient(evt, currentSelected);});
	editPatient.click(function(evt) { return ePatient(evt, currentSelected);});

    };

    function ePatient(evt,cs) {
	console.log("In edit patient...");
	var pat_uri = "/patient-edit/" + cs.data("href");
	if (pat_uri.indexOf("undefined") == -1)
	{
	    $.ajax
	    (
		pat_uri,
		{
		    dataType: "json",
		    cache : false
		}
	    ).done(renderPatientProfile);
	}
    };

    function renderPatientProfile(pat) {
	console.log("In renderPatientProfile...");
	//var up = JSON.parse(userP);
	console.log("Patient Profile Data is: " + JSON.stringify(pat));
    };

    function rPatient(evt, cs) {
	console.log("In rPatient...");
	var pid = cs.data("href");
	var pname = $("td", cs).first().text();
	console.log("Current patient-id is: " + pid);
	console.log("Current patient name is: " + pname);


	$("#cancelRPatient").click
	(
	    function(evt) {
		return cancelRemove(evt);
	    }
	);

	$("#confirmRPatient").click
	(
	    function(evt) {
		return confirmRemove(evt, pid, pname);
	    }
	);

	$("#rPatientDialog").modal({backdrop: 'static', show :false});
	$("#removePatientMsg").html("Are you sure you want to remove patient:&nbsp; <b>"
				 + pname
				 + "</b>?");
	$("#rPatientDialog").modal('show');
    };

    function cancelRemove(evt, pid, pname) {
	$("#cancelRPatient").off("click");
	$("#confirmlRPatient").off("click");
	$("#rPatientDialog").modal('hide');
    };

    function confirmRemove(evt, pid, pname) {
	$("#cancelRPatient").off("click");
	$("#confirmRPatient").off("click");
	$("#rPatientDialog").modal('hide');
	console.log("Removing patient = " + pname );
	$.post("/remove-patient", {pat: pid}, renderPatients, "json");
	
    };

    function handlePatientClicked() {
	if (currentSelected != null) {
	    currentSelected.attr("style", "");
	}
	currentSelected = $(this);
	currentSelected.css('background-color', 'navy');
	var userUri = "/patient-summary/" + currentSelected.data("href");
	$.ajax
	(
	    userUri,
	    {
		dataType: "json",
		cache: false
	    }
	).done(renderSelected);
	$("button.btn[disabled]").prop("disabled", false);
	$("#patient-summary").css("visibility", "visible");
    };

    $(function() {
	getPatients();
	addButtonHandlers();
    });
});
