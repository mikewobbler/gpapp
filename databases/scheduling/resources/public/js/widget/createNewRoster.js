define(['jquery', 'd3', 'underscore'], function($) {
    'use strict';

    var cnr = {};
    cnr.createRosterForm = function(nrTabId) {
	var rform = {};

	rform.getSetupData = function(gotSetupData_cb) {
	    $.ajax
	    (
		"/create-roster-data",
		{
		    dataType: "json",
		    cache: false
		}
	    ).done(function(sd)
		   {
		       rform.setupForm(sd);
		       gotSetupData_cb();
		   });
	};
	
	rform.setupForm = function(formSetup) {
	    console.log("In setup create new roster form.");
	    console.log("formSetup data is: " + JSON.stringify(formSetup));
	    var clins = _.map(formSetup.clinicians,
			      function(val) {
				  return _.pick(val,
						'user-fullName',
						'user-uid');
			      });
	    $("#clinicianRosterTable > table > tbody").empty();
	    var tr = d3.select("#clinicianRosterTable > table > tbody")
		    .selectAll("tr")
		    .data(clins)
		    .enter()
		    .append("tr");

	    var td = tr.selectAll("td")
	    	    .data(function(d) {
			return [
			    {"name" : d['user-fullName']},
			    {"uid" : d['user-uid']}
			];
		    })
	    	    .enter()
	    	    .append("td")
		    .append(function(d) {
			if (_.keys(d)[0] == "name") {
			    var p = document.createElement("p");
			    p.innerHTML = d["name"];
			    return p;
			}
			else
			{
			    var ip = document.createElement("input");
			    ip.setAttribute("class", "form-control clinician-checkbox");
			    ip.setAttribute("type", "checkbox");
			    ip.setAttribute("name", d["uid"]);
			    return ip;
			}
		    });

	    rform.setupButtons();
	};

	function handleNewSchedule() {
	    //$("#newScheduleDialog").modal({backdrop: 'static', show: true});
	    console.log("Clicked New Schedule");
	    $("#roster-tabs").toggleClass("animate-left");
	    $("#roster-tabs").one("animationend", function(e) {
		$("#roster-tabs").toggleClass("animate-left");
		$("#roster-tabs").toggleClass("on-screen");
		$("#roster-tabs").toggleClass("off-screen-left");
	    });
	    $("#new-schedule-view").toggleClass("animate-left");
	    $("#new-schedule-view").one("animationend",
					function(e) {
		$("#new-schedule-view").toggleClass("animate-left");
		$("#new-schedule-view").toggleClass("off-screen-right");
		$("#new-schedule-view").toggleClass("on-screen");
	    });
	}

	function handleCancelNewSchedule() {
	    $("#roster-tabs").toggleClass("animate-right");
	    $("#roster-tabs").one("animationend", function(e) {
		$("#roster-tabs").toggleClass("animate-right");
		$("#roster-tabs").toggleClass("on-screen");
		$("#roster-tabs").toggleClass("off-screen-left");
	    });
	    $("#new-schedule-view").toggleClass("animate-right");
	    $("#new-schedule-view").one("animationend",
					function(e) {
		$("#new-schedule-view").toggleClass("animate-right");
		$("#new-schedule-view").toggleClass("off-screen-right");
		$("#new-schedule-view").toggleClass("on-screen");
	    });
	}

	function handleAddBreak(e) {
	    console.log("Event e is: " + e );
	    var s_time = $("#" + e.target.id + "-start-time").val() || "00:00";
	    var e_time = $("#" + e.target.id + "-end-time").val() || "23:59:59";
	    var tid = e.target.id;
	    // Create a list of all the start and end times currently recorded.
	    var blist = _.map($("ul[aria-labelledby=" + tid + "-breaks-dn] > li").has("label"),
			      function(el) {
				  return { "start" : $(":nth-child(1)", el).text(),
					   "end" : $(" :nth-child(3)", el).text()
					 };
			      });
	    $("#newSchedTable > tbody").empty();
	    if (blist.length == 0) {
		$("#newSchedTable > tbody").append("<li id=noBreaksCreated>No Breaks created.</li>");
	    }
	    else {
		_.each(blist, function(v, idx) {
		    var st = v["start"];
		    var end = v["end"];
		    var row_id = "brow_" + idx;
		    var but_id = "rb_" + idx;
		    var row_start = "<tr id=" + row_id + ">";
		    var remove_but = '<td><button type="button" class="btn btn-primary btn-sm" id="'
			    + but_id + '" style="text-align:center;">Remove Break</button></td></tr>';
		    var row_contents = "<td>" + st + "</td><td>" + end + "</td>" + remove_but;
		    var row_el = row_start + row_contents + "</tr>";
		    $("#newSchedTable > tbody").append(row_el);
		    $("#" + but_id).click(function(e) {
			return removeSchedBreak(row_id, tid, idx);
		    });
		});
	    }
		    
	    $("#startBreak").attr("min", s_time);
	    $("#startBreak").attr("max", e_time);
	    $("#endBreak").attr("min", s_time);
	    $("#endBreak").attr("max", e_time);	    
	    $("#addBreakDialog").modal({backdrop: 'static', show : true});
	    var addHandler = createAddHandler
	    (
		tid,
		_.map(blist, parse_to_date)
	    );
					      
 	    $("#saveBreak").click(addHandler);
	    $("#doneBreak").click(function(e) {
		$("#saveBreak").off("click");
		$("#doneBreak").off("click");
		// close the modal.
		$("#addBreakDialog").modal("hide");
	    });
	}

	function removeSchedBreak(rId, list_id, ridx) {
	    console.log("Removing break with row id = " + rId);
	    var first_row = $("#" + rId);
	    // Note: We need to add 2 instead of 1, because the first <li> element contains
	    // the Edit breaks button.
	    var second_row = $("li:nth-of-type(" + (ridx + 2) + ")",
			       $("ul[aria-labelledby=" + list_id + "-breaks-dn]"));
	    first_row.remove();
	    second_row.remove();
	    return;
	};

	function createAddHandler(list_id, brk_list) {
	    var blist = brk_list;
	    return function(evt) {
		return handleBreak(evt, list_id, blist);
	    };
	};

	function parse_to_date(ival) {
	    var start = ival["start"].split(":");
	    var start_hours = parseInt(start[0]);
	    var start_mins = parseInt(start[1]);
	    var end = ival["end"].split(":");
	    var end_hours = parseInt(end[0]);
	    var end_mins = parseInt(end[1]);
	    var sd = new Date(0, 0, 0, start_hours, start_mins, 0, 0);
	    var ed = new Date(0, 0, 0, end_hours, end_mins, 0, 0);
	    return { "start" : sd, "end" : ed };
	};
	
	function handleBreak(evt, list_id, blist) {
	    console.log("Got handle break.");
	    var sb = $("#startBreak").val();
	    var sb_list = sb.split(":");
	    var sbd = new Date(0, 0, 0, parseInt(sb_list[0]), parseInt(sb_list[1]), 0, 0);
	    var eb = $("#endBreak").val();
	    var eb_list = eb.split(":");
	    var ebd = new Date(0, 0, 0, parseInt(eb_list[0]), parseInt(eb_list[1]), 0, 0);
	    if (ebd <= sbd) {
		$("#schedAlertText").empty();
		$("#schedAlertText").html
		(
		    "Start Time: " + sb + " must be earlier than end time: " + eb
		);
		$("#schedAlert").fadeIn("slow");
		return;
	    }

	    var ic = intervalClashes(sbd, ebd, blist);
	    if (ic.length != 0) {
		$("#schedAlertText").empty();
		var ic_list = "<ul>" +
			_.reduce(ic, function(memo, val) {
			    return "<li>" + val["start"].getHours() + ":" + val["start"].getMinutes()
				+ " to " + val["end"].getHours() + ":" + val["end"].getMinutes() + "</li>";
			}, "") +
			"</ul>";
		var etext = "<p>Interval: " + sb + " to " + eb + " clashes with:&nbsp;</p>" + ic_list;
		$("#schedAlertText").html(etext);
		$("#schedAlert").fadeIn("slow");
		return;
	    }

	    blist.push({ "start" : sbd, "end" : ebd});
	    var nrows = $("#newSchedTable > tbody >tr").length;
	    var row_id = "brow_" + nrows;
	    var but_id = "rb_" + nrows;
	    var row_el = '<tr id="' + row_id + '"><td>' + sb + '</td><td>' + eb + '</td>';
	    row_el += '<td><button type="button" class="btn btn-primary btn-sm" id="'
			    + but_id + '" style="text-align:center;">Remove Break</button></td></tr>';

	    $("#noBreaksCreated").remove();
	    $("#newSchedTable > tbody").append(row_el);
	    $("#" + but_id).click(function(e) {
		return removeSchedBreak(row_id, list_id, nrows);
	    });

	    var label1 = '<label class="label label-info">' + sb + "</label>";
	    var label2 = '<label class="label label-info">' + eb + "</label>";
	    $("ul[aria-labelledby=" + list_id + "-breaks-dn]").append
	    (
		"<li>" + label1 + "<label>&nbsp;to&nbsp;</label>" + label2 + "</li>"
	    );
	};

	function intervalClashes(si, ei, ilist) {
	    return _.filter(ilist, function(ival) {
		var sj = ival["start"];
		var ej = ival["end"];
		return (si < ej && ei > sj);
	    });
	};

	rform.copied = null;

	function handleCopy(e) {
	    var cbutton = e.target;
	    console.log("cbutton is : " + cbutton.id);
	    var row = cbutton.closest("tr");
	    var stime = $("[id*=-start-time]", row).val();
	    var etime = $("[id*=-end-time]", row).val();
	    var dur = $("[id*=-appt-duration]", row).val();
	    var breaks = _.rest($("td > div > ul > li", row));
	    rform.copied =
		{
		    "st" : stime,
		    "et" : etime,
		    "dur" : dur,
		    "brks" : _.map(
			breaks,
			function(el) {
			    return $(el).clone()[0];
			}
		    )
		};

	    // Enable all the past buttons.
	    $("#new-weekly-schedule > tbody > tr > td > div > .paste-btn").prop("disabled", false);
	};

	function handlePaste(e) {
	    if (rform.copied != null) {
		handleClear(e);
		var pbutton = e.target;
		var row = pbutton.closest("tr");
		$("[id*=-start-time]", row).val(rform.copied.st);
		$("[id*=-end-time]", row).val(rform.copied.et);
		$("[id*=-appt-duration]", row).val(rform.copied.dur);
		$("td > .dropdown > ul > li", row).has("label").remove();
		_.each(rform.copied.brks, function(bk) {
		    $("td > .dropdown > ul", row).append($(bk).clone());
		});
	    }
	    return true;
	};

	function handleClear(e) {
	    var pbutton = e.target;
	    var row = pbutton.closest("tr");
	    $("[id*=-start-time]", row).val("");
	    $("[id*=-end-time]", row).val("");
	    $("[id*=-appt-duration]", row).val("");
	    $("td > .dropdown > ul > li", row).has("label").remove();
	    return true;
	};

	function handleWeeklyRow(row_el) {
	    var tds = $("td", row_el);
	    var day_of_week = _.first(tds).textContent;
	    var rostered = $("[type=checkbox]", row_el).prop("checked");
	    if (rostered) {
		var daySched = {};
		daySched.stime = $("[id*=-start-time]", row_el).val();
		daySched.etime = $("[id*=-end-time]", row_el).val();
		daySched.dur = $("[id*=-appt-duration]", row_el).val();
		var blist = _.rest($("td > div > ul > li", row_el));
		daySched.breaks = _.map(blist, function(li_el) {
		    var labels = $("label", li_el);
		    var sb = _.first(labels).textContent;
		    var eb = _.last(labels).textContent;
		    return { "start" : sb, "end": eb};
		});
		return {"wd" : day_of_week, "roster" :daySched };
	    }
	    else {
		return { "wd" : day_of_week, "roster" : "notRostered"};
	    }
	};
	
	function handleSaveWeekly(e) {
	    var rows = $("#new-weekly-schedule > tbody > tr");
	    var dayList = _.map(rows, handleWeeklyRow);
	    var startSched = $("#schedFirstDate").val();
	    var endSched = $("#schedLastDate").val();
	    $.post("new-weekly-schedule",
		   {
		       start: startSched,
		       end: endSched,
		       weeklySched: dayList
		   },
		   function(resp) {
		       console.log("Got response = " + resp);
		   },
		   "json");
	    return true;
	};

	function handleClearWeekly(e) {
	    var nws = $("#new-weekly-schedule");
	    $("[id*=-start-time]", nws).val("");
	    $("[id*=-end-time]", nws).val("");
	    $("[id*=-appt-duration]", nws).val("");
	    $("td > .dropdown > ul > li", nws).has("label").remove();
	    return true;
	};
	
	rform.setupButtons = function() {
	    console.log("Setting up buttons.");
	    $("#new-schedule").click(handleNewSchedule);
	    $("#cancelNewSchedule").click(handleCancelNewSchedule);
	    $("#new-weekly-schedule > tbody > tr > td > div > ul > li > a").click(handleAddBreak);
	    $("#new-weekly-schedule > tbody > tr > td > div > .copy-btn").click(handleCopy);
	    $("#new-weekly-schedule > tbody > tr > td > div > .paste-btn").click(handlePaste);
	    $("#new-weekly-schedule > tbody > tr > td > div > .clear-btn").click(handleClear);
	    $("#saveWeeklySchedule").click(handleSaveWeekly);
	    $("#clearWeeklySchedule").click(handleClearWeekly);	    
	};

	return rform;
    };

    return cnr;
});
