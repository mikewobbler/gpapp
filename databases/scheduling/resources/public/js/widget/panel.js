define(['jquery'], function($) {
    'use strict';
    var gp_p = {};

    /*
     *
     */
    gp_p.panel = function(parent, panel_def)
    {
	var p = {};
	p.parent = parent;
	p.desc = panel_def;

	var title = panel_def.title;
	var cname = panel_def.class_name;
	var t = panel_def.text || "";
	var lTitle = panel_def.link_title;
	var hr = panel_def.href;
	var img = panel_def.img || false;
	//var inj_html = panel_def.inject_html || false;
	var svg = panel_def.svg || false;
	
	p.place_image = function(img, pbody) {
	    var img_width = "100px";
	    var img_height = "100px";
	    var image_el = document.createElement("img");
	    $(image_el).attr("src", img);
	    $(image_el).attr("style", "float:right;width:" + img_width + ";height:" + img_height + ";");
	    $(pbody).append(image_el);
	};

	p.inject_html = function(html_string, pbody) {
	    $(pbody).append(html_string);
	};

	p.add_svg = function(svg, pbody) {
	    var obj = document.createElement("object");
	    $(obj).attr("style", "float:right;width:100px;height:100px;");
	    $(obj).attr("id", "svg-cal");
	    $(obj).attr("type", "image/svg+xml");
	    $(obj).attr("data", svg);
	    obj.onload = function() {
		var svgDoc = $("#svg-cal")[0].contentDocument;
		var dl = (new Date()).toString().split(" ");
		$("svg", svgDoc).attr("width", 100);
		$("svg", svgDoc).attr("height", 100);
		$("#month-day", svgDoc).text(dl[2]);
		$("#week-day", svgDoc).text(dl[0]);
		$("#month", svgDoc).text(dl[1]);
	    };
	    $(pbody).append(obj);
	};

	p.createPanel = function() {
	    var top = document.createElement("div");
	    $(top).addClass("col-md-3");
	    var panel_top = document.createElement("div");
	    $(top).append($(panel_top));
	    $(panel_top).addClass("panel panel-default " + cname);
	    var panel_body = document.createElement("div");
	    $(panel_top).append($(panel_body));
	    $(panel_body).addClass("panel-body");
	    var para = document.createElement("p");
	    $(para).text(t);
	    $(panel_body).append($(para));
	    para = document.createElement("p");
	    $(panel_body).append($(para));
	    var a = document.createElement("a");
	    $(a).addClass("btn btn-default");
	    $(a).attr("href", hr);
	    $(a).attr("role", "button");
	    $(a).text(lTitle);
	    $(para).append($(a));
	    if (img) {
		p.place_image(img, panel_body);
	    }
	    if (svg) {
		p.add_svg(svg, panel_body);
	    }
	    return top;
	};

	p.element = p.createPanel();

	return p;
    };

    return gp_p;
});
