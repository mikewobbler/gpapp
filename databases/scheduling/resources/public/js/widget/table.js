define(['jquery', 'underscore', 'bootstrap', 'd3'], function(require) {
    console.log("In table.js");    
    console.log("JQuery version is: " + $.fn.jquery);
    console.log("Underscore Version is: " + _.VERSION);
    console.log("d3 Version is: " + d3.version);

    var ct = {};

    ct.createTable = function(table_id, link_column, columns, sorted, route, fn) {
	var tableEl = $("#" + table_id);
	tableEl.empty();
	tableEl.append("<thead><tr></tr></thead>");	
	var colNames = _.values(columns);
	var colDataFields = _.keys(columns);

	var th = d3.select("#" + table_id + " tr")
		.selectAll("th")
		.data(colNames)
		.enter()
		.append("th")
		.append("a")
		.attr("href", "#")
		.text(function(d) { return d;});

	// FIXME: Add a sorting function on click to the above, but only
	// for cases where a different column is clicked to the current sort
	// column.
	// $("#" + elId + " th a").on("click", function(event) {
	//    resort(event, sorted, tableObject);
	//   });
	//
	$(tableEl).append("<tbody></tbody>");
	
	var tr = d3.select("#" + table_id + " tbody")
		.selectAll("tr")
		.data(sorted)
		.enter()
		.append("tr")
		.classed("clickable-row", true)
		.attr("data-href", function(d) {
		return route + d[link_column]});

	var td = tr.selectAll("td")
		.data(function(d) {
		    return _.values(_.pick(d, colDataFields));
		})
		.enter()
		.append("td")
		.text(function(d) { return d;});

	$(".clickable-row").click(fn);
    };


    ct.addSearchHandler = function(tId, sForm, sorted, sortField) {
	var currentlySelected = null;
	$("#" + sForm + " .form-control").off("keyup");
	console.log("in addSearchHandler");
	$("#" + sForm + " .form-control").keyup(
	    function(evt) {
		var searchString = $("#" + sForm + " > input").val();
		if (searchString == "")
		{
		    return;
		}
		var i = _.findIndex(sorted, ct.searchPred(searchString, sortField));
		if (i != -1) {
		    var el = $("#" + tId + " > tbody > tr:eq(" + i + ")");
		    var parent = $("#" + tId + " > tbody");
		    if (currentlySelected != null) {
			currentlySelected.attr("style", "");
		    }
		    currentlySelected = el;
		    el.css('background-color', 'blue');

		    $(parent).animate(
			{
			    scrollTop:
			    $(parent).scrollTop() + $(el).offset().top - $(parent).offset().top
			},
			{ duration: 'slow', easing: 'swing'}
		    );
		}
	    });
    };

    ct.searchPred = function(searchString, sortField) {
	return function(listEl) {
	    var sort_field = listEl[sortField];
	    sort_field = (sort_field.length == 0) ? "zzzzzzzz" : sort_field;
	    return sort_field.search(searchString) == 0;
	};
    };

    return ct;

});
