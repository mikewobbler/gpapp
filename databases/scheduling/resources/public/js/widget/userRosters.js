define(['jquery', 'd3', 'underscore'], function($) {
    'use strict';

    var ur = {};
    ur.createUs = function(rostTabId)
    {
	var urost = {};
	urost.tId = rostTabId;

	urost.getRosts = function(gotRosts_cb)
	{
	    $.ajax
	    (
		"/get-rosters",
		{
		    dataType: "json",
		    cache : false
		}
	    ).done(function(rosts)
		   {
		       urost.setupRostTable(rosts);
		       gotRosts_cb();
		   });
	};

	urost.setupRostTable = function(rosts) {
	    console.log("In setup RostTable.");
	    console.log("Rosts are: " + JSON.stringify(rosts));
	    if ("None" == rosts.dataList) {
		// No rosters as yet.
		$("#" + urost.tId + " >tbody>tr").empty();
		$("#" + urost.tId + " >tbody>tr").append
		(
		    "<td>No rosters currently in database.</td><td/><td/><td/><td/>"
		);
	    }
	    else {
		
	    }
	};

	urost.createTableRows = function() {
	    return true;
	};
	
	return urost;
    };
	
    return ur;
});
