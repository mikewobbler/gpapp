define(['jquery', 'd3', 'underscore'], function($) {
    'use strict';

    var us_rost = {};
    us_rost.createUs = function(rostTabId)
    {
	var urost = {};
	urost.tId = rostTabId;

	urost.getRosts = function(gotRosts_cb)
	{
	    $.ajax
	    (
		"/get-rosters",
		{
		    dataType: "json",
		    cache : false
		}
	    ).done(function(rosts)
		   {
		       urost.setupRostTable(rosts);
		       gotRosts_cb();
		   });
	};

	urost.setupRostTable = function(rosts) {
	    console.log("In setup RostTable.");
	    console.log("Rosts are: " + JSON.stringify(rosts));
	    urost.rosts = rosts;
	};

	urost.createTableRows = function() {
	    return true;
	};
	
	return urost;
    };
	
    return us_rost;
});
