define(['jquery', 'd3', 'underscore'], function($) {
    'use strict';

    var us_panel = {};
    us_panel.createPanel = function(tableId)
    {
	var usp = {};
	usp.tId = tableId;

	usp.getUsers = function(gotUsers_cb)
	{
	    $.ajax
	    (
		"/get-users",
		{
		    dataType: "json",
		    cache : false
		}
	    ).done(function(userD)
		   {
		       usp.setupUserData(userD);
		       gotUsers_cb();
		   });
	};

	usp.setupUserData = function(userD) {
	    usp.userData =
		_.map(userD["data-list"],
		      function(val) {
			  return _.pick(val, 'user/fullName', 'user/uid');
		      });
	    usp.createTableRows();
	};

	usp.createTableRows = function() {
	    var tr = d3.select("#" + usp.tId + " tbody")
		    .selectAll("tr")
		    .data(usp.userData)
		    .enter()
		    .append("tr");

	    var td = tr.selectAll("td")
		    .data(function(d) {
			return [d["user/fullName"], false];
		    })
		    .enter()
		    .append("td")
	    	    .append(function(d) {
			if (d) {
			    var el = document.createElement("p");
			    el.textContent = d;
			    return el;
			}
			else {
			    var el = document.createElement("input");
			    el.setAttribute("type", "checkbox");
			    el.checked = true;
			    return el;
			}
		    });
	};
	return usp;
    };
	
    return us_panel;
});
