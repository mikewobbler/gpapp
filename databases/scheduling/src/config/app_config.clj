(ns config.app-config
  (:require [mount.core :refer [defstate start stop]]
            [utils.fileUtils :refer :all]
            [clojure.pprint :refer (pprint)]))

(defn get-conf
  "Read in the configuration from the file specified by path."
  []
  (read-string (slurp
                (get-path [(System/getProperty "user.dir") "db_conf.edn"]))))

(defstate gp-config
  :start  (get-conf)
  :stop (fn [] true))

(defn modify-config
  [mod-key mod-val]
  (let
      [conf (get-conf)
       new-conf (assoc conf mod-key mod-val)]
    (spit
     (get-path [(System/getProperty "user.dir") "db_conf.edn"])
     (with-out-str (pprint new-conf)))
    (stop #'config.app-config/gp-config)
    (start #'config.app-config/gp-config)
    true
    ))
  




  
