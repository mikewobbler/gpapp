(ns config.setup-seed
  (:require
   [datomic.api :as d :exclusions [joda-time]]
   [clojure.pprint :refer (pprint)]
   [compojure.coercions :refer :all]
   [clj-time.format :as f]
   [clj-time.coerce :as c]
   [clojure.string :as cstr]
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]
   [taoensso.timbre :as timbre
    :refer (log trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)]
   [scheduling.models.datomic-types :refer :all]
   ;;[gp.models.db :refer [add-patient add-user]]
   )
  (:import [java.lang Exception])
  )

;;(vec-form [this])

(defn db-after-to-map
  [tx]
  (let
      [dba (seq (d/q '[:find ?e ?aname ?v ?added
                        :in $ [[?e ?a ?v _ ?added]]
                        :where
                        [?e ?a ?v _ ?added]
                        [?a :db/ident ?aname]]
                     (:db-after @tx)
                      (:tx-data @tx)))]
    (reduce #(assoc %1 (nth %2 1) (nth %2 2)) {} dba)
    )
  )

(defn add-rec-to-datomic
  [conn rec part test-transact]
  (let
      [dmap (map-form rec)
       p (keyword (str "db.part/" part))
       tx (d/transact conn [(assoc dmap :db/id (d/tempid p))])]
    (if test-transact
      (let
          [db-after-map (db-after-to-map tx)]
        (if (= 0 (count (remove #(contains? db-after-map (key %1)) dmap))) true
            (do
              (timbre/error "Error adding record to datomic. Record is:")
              (timbre/error (str dmap))
              false)
          )
        )
      @tx
      )
    )
  )

(defn show-transaction
  [tx]
  (let
      [dba (:db-after @tx)
       txd (:tx-data @tx)]
    (pprint (seq (d/q '[:find ?e ?aname ?v ?added
                        :in $ [[?e ?a ?v _ ?added]]
                        :where
                        [?e ?a ?v _ ?added]
                        [?a :db/ident ?aname]]
                      dba
                      txd)))
    )
  )

(defn get-seed-patient
  [fname]
  (with-open [in-file (io/reader fname)]
    (map (partial apply ->Patient)
         (rest 
          (doall
           (csv/read-csv in-file))))))


(defn get-seed-user
  [fname]
  (with-open [in-file (io/reader fname)]
    (map (partial apply ->User)
         (rest 
          (doall
           (csv/read-csv in-file))))))

(defn setup-seed-data
  [conn test-setup]
  (when (:load-seed-data test-setup)
    (let
        [pat-data (get-seed-patient (:seed-patient-data-loc test-setup))
         user-data (get-seed-user (:seed-user-data-loc test-setup))]
      (doseq [p (take 5 pat-data)]
        (do
          (timbre/info (str "Adding Patient = " (:patient-firstName p)))
          (add-rec-to-datomic conn p "user" true))
        )
      (doseq [u user-data]
        (do
          (timbre/info (str "Adding User = " (:user-fullName u)))
          (add-rec-to-datomic conn u "user" true))))
      ))

(defn add-patients-test
  [conn pat-data]
  (let
      [num-recs (atom 0)]
    (doseq [p pat-data]
      (do
        (timbre/info (str "Adding Patient = " (:patient-firstName p)))
        (add-rec-to-datomic conn p "user" true)
        (swap! num-recs inc)))
    @num-recs))
