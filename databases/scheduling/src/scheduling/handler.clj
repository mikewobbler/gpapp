(ns scheduling.handler
  (:require
   [compojure.core :refer :all]
   [ring.middleware.resource :refer [wrap-resource]]
   [ring.middleware.file-info :refer [wrap-file-info]]
   [ring.middleware.reload :refer [wrap-reload]]
   [liberator.core
    :refer [defresource resource request-method-in]]
   [liberator.dev :refer [wrap-trace]]
   [noir.response :refer [redirect]]
   [noir.session :as session]   
   [selmer.middleware :refer [wrap-error-page]]
   [hiccup.middleware :refer [wrap-base-url]]   
   [compojure.handler :as handler]
   [compojure.route :as route]
   [mount.core :as mount]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]
   [taoensso.timbre.appenders.core :as appenders]
   [config.setup-db :refer [conn]]
   [scheduling.routes.rostering :refer [rostering-routes]]))

(defn init []
  (timbre/merge-config!
   {:appenders {:spit (appenders/spit-appender {:fname "/tmp/scheduling.log"})}})
  (timbre/info "scheduling is starting.")
  (mount/start)
  )

(defn destroy []
  (timbre/info "scheduling is shutting down")
  (mount/stop)
  )

(defroutes app-routes
  (route/resources "/")
  (GET "/" [] (redirect "/manage-rosters"))
  (route/not-found "Not Found"))

;; (def app
;;   (wrap-defaults rostering-routes app-routes site-defaults))
(def app
  (-> (handler/site
       (routes rostering-routes app-routes))
      (wrap-trace :header :ui)
      wrap-reload
      (wrap-base-url)
))
