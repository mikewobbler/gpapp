(ns scheduling.models.appts.appt-utils
  (:require [clojure.pprint :refer (pprint)]
            [clj-time.core :as t]
            [clj-time.local :as l]
            [clj-time.format :as f]
            [clj-time.periodic :as p]
            [clj-time.coerce :as c]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy get-env log-env)]            
            ))

(defn local-from-iso
  "Create a local time object from an iso date string."
  [iso-str]
  (l/to-local-date-time iso-str)
  )

(defn utc-from-iso
  "Create a utc time object from an iso date string."
  [iso-str]
  (f/parse (f/formatters :date-time) iso-str)
  )

(defn get-interval
  "Create an interval from a start time and a duration in minutes."
  [start-time mins]
  (t/interval start-time (t/plus start-time (t/minutes mins))))

(defn make-break-list
  "Make a list of break intervals from a list of maps containing :st and :et
values."
  [[brk & rst]]
  (loop [brks [] b brk r rst]
    (if (not b) brks
        (recur (conj brks (t/interval (:st b) (:et b))) (first r) (rest r))
        ))
  )

(defn compare-intervals
  "Comparator for use in sorting a collection of intervals."
  [i1 i2]
  (compare (t/start i1) (t/start i2))
  )


;; Interval functions
;; within?
;; overlaps?
;; abuts?
;; end
;; start
;; extend
;; in-days in-hours in-millis in-minutes in-months in-seconds in-weeks in-years
(defn split-intervals
  "Remove the break brk from intervals, returning a new ordered list of
 intervals that doesn't include brk."
  [brk intervals]
  (when (not intervals) [])
  (let
      [curr (first intervals)]
    (cond (or (t/before? (t/end brk) (t/start curr))) intervals
          (or (t/after? (t/start brk) (t/end curr))) (concat [curr] (split-intervals brk (rest intervals)))
          :else ;; (t/overlaps? brk curr)
          (if (or (= (t/start brk) (t/start curr)) (t/before? (t/start brk) (t/start curr)))
                 (cond (t/before? (t/end brk) (t/end curr)) ;; entirely contained within current
                       (concat [(t/interval (t/end brk) (t/end curr))] (rest intervals))
                       (= (t/end brk) (t/end curr)) (rest intervals)
                 :else
                 (split-intervals brk (rest intervals)))
                 ;; (> (t/start brk) (t/start curr))
                 (concat [(t/interval (t/start curr) (t/start brk))]
                                      (split-intervals brk
                                                       (concat [(t/interval (t/start brk) (t/end curr))]
                                                               (rest intervals)))))
          )))

(defn interval-list-to-string
  "Return a string representation of the intervals in i-list."
  [i-list]
  (loop [curr (first i-list) remaining (rest i-list) s ""]
    (if (not curr) s
        (recur (first remaining) (rest remaining)
         (str s "\n" (f/unparse (f/formatters :rfc822) (t/start curr)) " ---> "
              (f/unparse (f/formatters :rfc822) (t/end curr))))
        )
    )
  )

(defn inst-list-to-string
  "Return a string representation of the date-times in i-list."
  [i-list]
  (loop [curr (first i-list) remaining (rest i-list) s ""]
    (if (not curr) s
        (recur (first remaining) (rest remaining)
         (str s "\n" (f/unparse (f/formatters :rfc822) curr) "\n"))
        )
    )
  )

(defn make-interval-list
  "Create an ordered list of available intervals for appointments, given start
  and end times and a list of breaks."
  [start end breaks]
  {:pre [(t/before? start end)]
   :post [true]
   }
  (let
      [brks (sort compare-intervals breaks)
       intervals [(t/interval start end)]]
    (loop [b brks i intervals]
      (if (= 0 (count b)) i
          (recur (rest b) (split-intervals (first b) i))
          )
      )
  ))


(defn setup-appointment-block
  "Setup an apointment block between times start and end, with the supplied
breaks, where each apointment is duration minutes long, and the appointments
are for the supplied clinician."
  [start end breaks duration clinician]
  {
   :start-time start
   :end-time end
   :available-interval-list (make-interval-list start end breaks)
   :clinician-breaks breaks
   :appts-with clinician
   :appt-duration duration
   }
  )

(defn contained-in
  "Return true if interval i1 is fully containe in interval i2."
  [i1 i2]
  (let [i1sm (c/to-long (t/start i1))
        i1em (c/to-long (t/end i1))
        i2sm (c/to-long (t/start i2))
        i2em (c/to-long (t/end i2))]
    (and (>= i1sm i2sm) (<= i1em i2em)))
  )

;; NOTE: Need to guarantee that no two intervals in
;; (:available-interval-list appt-block) abut.
(defn get-available-appts
  "Return a list of available appointments for the given appointment block."
  [appt-block]
  (let [i-list (:available-interval-list appt-block)
        appt-duration (t/minutes (:appt-duration appt-block))]
    ;; Loop over intervals in i-list
    (if (= 0 (count i-list)) []
        (loop [appts []
               it (first i-list)
               i-rest (rest i-list)]
          (let
              [found-appts (concat appts
                                   ;; Loop while we can fit periods from period list in the current interval.
                                   (loop [period-list (p/periodic-seq (t/start it) appt-duration)
                                          ft (first (take 1 period-list))
                                          st (first (take 1 (next period-list)))
                                          cur-appts []]
                                     (let [new-int (t/interval ft st)]
                                     (if (not (contained-in new-int it)) cur-appts
                                         (recur
                                          (next period-list)
                                          st
                                          (first (take 1 (drop 2 period-list)))
                                          (conj cur-appts new-int))))
                                   ))]
          (if (= 0 (count i-rest)) found-appts
              (recur
               found-appts
               (first i-rest)
               (rest i-rest))))))
    )
  )
  
