(ns scheduling.models.appts.dt-appts
  (:require [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :refer (pprint)]
            [compojure.coercions :refer :all]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy get-env log-env)]
            [clj-time.core :as t]
            [clj-time.local :as l]
            [clj-time.format :as f]
            [clj-time.periodic :as p]
            [clj-time.coerce :as c]
            [clojure.string :as cstr]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [utils.err-wrap :as err-handler]            
            [utils.fileUtils :refer :all]
            [utils.db-utils :refer :all]
            [utils.shell-utils :refer :all]
            [scheduling.models.appts.appt-utils :as appt-utils]
            [config.setup-db :refer [conn]])
  (:import [java.util.Date])
  )

;; We want an appointment block structure like the following:
(comment
  (def appt-block
    {
     :start-time              "The time/date of the start of the appt-block"
     :end-time                "The time/date of the end of the appt-block"   
     :clinician-breaks        "A list of breaks"
     :appts-with              "The clinician the appointment block is for."
     :appt-duration           "The duration of standard bookings."
     :available-appointments  "A list of available intervals."
     :booked-appointments     "A list of booked appointments."     
     }
    )

  ;; and an appointment structure should look like this:
  (def appt
    {
     :start-time              "The time/date of the start of the appointment."
     :expected-duration       "The expected duration of this appointment."
     :end-time                "The time/date of the end of the appointment."
     :patient                 "The patient being seen."
     :clinician               "The clinician seeing the patient."
     :notes                   "Notes for the clinician made when the appointment was made"
     :status                  "An enumerated value"
     }
    )
  )

;; Get an appointment block for a given clinician on a given start day:
;;
;; - If the database contains an existing appointment block for the clinician
;;   on the requested date, populate the appt-block from the database.
;; - else, create a new appointment block for the given day with known
;;   start times and end times, and a list of breaks.
;;
;; First - let's create the database and load the schema:
(comment
  (def schema-file
    (get-path [(System/getProperty "user.dir") "resources" "appt-schema.edn"]))

  ;; "/Users/michaelw/gpApp/gp/src/utils/datomic_experiments/appt-schema.edn")

  (def curr-database-name "appoints")
  (def xact-uri
    (clojure.string/replace
     "datomic:sql://patdb?jdbc:postgresql://localhost:5432/datomic?user=datomic&password=datomic"
     #"/datomic\?" (str "/" curr-database-name "?"))
    ))
;; (start-pgres)
(comment
  (defn create-stuff
    []
    ;; Create the new postgres database, and start it.
    ;;(start-datomic-new-db curr-database-name)
    ;; Create the datomic database.
    (d/create-database xact-uri)
    ;; Get the connection to the datomic transactor.
    (def appt-conn (d/connect xact-uri))
    ))

(comment
  (defn list-partitions
    []
    (d/q '[:find ?i :where
           [:db.part/db :db.install/partition ?p] [?p :db/ident ?i]]
         (d/db conn))
    )

                                        ; Add a partition
  (defn add-part
    []
    @(d/transact conn
                 [{:db/id #db/id[:db.part/db],
                   :db/ident :appointments,
                   :db.install/_partition :db.part/db}]))
  (defn load-s
  []
  (let [schema (load-file schema-file)]
    @(d/transact conn schema)))
  )




;; First - let's setup some users
(comment
  (def users ["Michael James Webster" "Ian William Webster" "Willie Wonker"])

  (defn add-users
    [us]
    (for [u us]
      @(d/transact conn
                   [{:db/id (d/tempid :db.part/user)
                     :user/fullName u
                     :user/uid (d/squuid)}])))

  (def patients
    (with-open [in-file (io/reader "/Users/michaelw/gpApp/gp/dev-resources/MOCK_DATA.csv")]
      (map #(str (nth %1 0) " " (nth %1 1))
           (rest
            (doall
             (csv/read-csv in-file))))
      )))

;; calling (add-patients (take 20 patients))
(comment
  (defn add-patients
    [pats]
    (for [p pats]
      @(d/transact conn
                   [{:db/id (d/tempid :db.part/user)
                     :patient/fullName p
                     :user/pid (d/squuid)}])))

  ;; Let's create a schedule for our doctors.
  (def sched1
    {
     :schedule/first-date "2016-02-01"               ;; use formatter :date
     :schedule/last-date  "2016-07-30T23:59:59.999Z" ;; user formatter :date-time
     :schedule/start-time "08:30"                    ;; user formatter :hour-minute
     :schedule/end-time   "17:30"                    ;; user formatter :hour-minute
     :schedule/breaks     [
                           {:schedule/start-break "11:00" :schedule/end-break "11:15"}
                           {:schedule/start-break "12:30" :schedule/end-break "13:15"}
                           {:schedule/start-break "14:00" :schedule/end-break "14:15"}
                           ]
     :schedule/appointment-duration 15
     }
    )

  (def sched2
    {
     :schedule/first-date "2016-08-01"               ;; use formatter :date
     :schedule/last-date  "2016-09-02T23:59:59.999Z" ;; user formatter :date-time
     :schedule/start-time "13:15"                    ;; user formatter :hour-minute
     :schedule/end-time   "19:30"                    ;; user formatter :hour-minute
     :schedule/breaks     [
                           {:schedule/start-break "17:00" :schedule/end-break "17:30"}
                           ]
     :schedule/appointment-duration 15
     }
    )

  (def sched3
    {
     :schedule/first-date "2016-10-01"               ;; use formatter :date
     :schedule/last-date  "2016-11-02T23:59:59.999Z" ;; user formatter :date-time
     :schedule/start-time "08:15"                    ;; user formatter :hour-minute
     :schedule/end-time   "19:30"                    ;; user formatter :hour-minute
     :schedule/breaks     [
                           {:schedule/start-break "11:00" :schedule/end-break "11:30"}
                           {:schedule/start-break "13:00" :schedule/end-break "13:45"}
                           {:schedule/start-break "17:00" :schedule/end-break "17:30"}
                           ]
     :schedule/appointment-duration 15
     }
    )

  (def sched4
    {
     :schedule/first-date "2016-11-04"               ;; use formatter :date
     :schedule/last-date  "2016-12-02T23:59:59.999Z" ;; user formatter :date-time
     :schedule/start-time "08:15"                    ;; user formatter :hour-minute
     :schedule/end-time   "19:30"                    ;; user formatter :hour-minute
     :schedule/breaks     [
                           {:schedule/start-break "11:00" :schedule/end-break "11:30"}
                           {:schedule/start-break "13:00" :schedule/end-break "13:45"}
                           {:schedule/start-break "17:00" :schedule/end-break "17:30"}
                           ]
     :schedule/appointment-duration 15
     }
    ))

(defn add-sched
  "Add a schedule of the above type, to the database."
  [sched]
  (let
      [first-day (c/to-date (f/parse (f/formatters :date) (:schedule/first-date sched)))
       last-day (c/to-date (f/parse (f/formatters :date-time) (:schedule/last-date sched)))
       start-time (c/to-date (f/parse (f/formatters :hour-minute) (:schedule/start-time sched)))
       end-time (c/to-date (f/parse (f/formatters :hour-minute) (:schedule/end-time sched)))
       breaks (mapv
               #(conj {}
                      {:schedule/interval-start
                       (c/to-date (f/parse (f/formatters :hour-minute) (:schedule/start-break %1)))}
                      {:schedule/interval-end
                       (c/to-date (f/parse (f/formatters :hour-minute) (:schedule/end-break %1)))})
               (:schedule/breaks sched))
       duration (:schedule/appointment-duration sched)
       tid (d/tempid :appointments)
       ;; Add the schedule without the breaks.
       ]
    (d/transact conn
                [{:db/id tid
                  :schedule/first-date first-day
                  :schedule/last-date last-day
                  :schedule/start-time start-time
                  :schedule/end-time end-time
                  :schedule/appointment-duration duration
                  :schedule/schedid (d/squuid)
                  :schedule/breaks breaks
                  }
                 ]
                )
    )
  )

(defn same-date?
  [date1 date2]
  (let
      [dt1 (or (and (= (type date1) org.joda.time.DateTime) date1) (c/from-date date1))
       dt2 (or (and (= (type date2) org.joda.time.DateTime) date2) (c/from-date date2))]
      (and (= (t/day dt1) (t/day dt2)) (= (t/month dt1) (t/month dt2))
           (= (t/year dt1) (t/year dt2))))
  )


(def appt-rules
  '[[(covers-date? ?d ?e)
     [?e :schedule/last-date ?d-end]
     [(.before ^java.util.Date ?d ?d-end)]
     [?e :schedule/first-date ?d-start]
     [(.after ^java.util.Date ?d ?d-start)]]
    
    [(user-schedules ?user-ent ?scheds)
     [?user-ent :user/schedules ?scheds]]

    [(user-schedule-for-date ?user-ent ?d ?sched)
     [?user-ent :user/schedules ?sched]
     [?sched :schedule/last-date ?d-end]
     [(.before ^java.util.Date ?d ?d-end)]
     [?sched :schedule/first-date ?d-start]
     [(.after ^java.util.Date ?d ?d-start)]
     ]

    [(user-appt-block-on-date ?uid ?date ?appt-block)
     [?e :user/uid  ?uid]
     [?e :user/appt-blocks  ?appt-block]
     [?appt-block :appt-block/start-time ?st]
     [(scheduling.models.appts.dt-appts/same-date? ?st ?date)]]])



(defn sched-from-entity
  "Create a map from the supplied entity."
  [entity dat-db]
  (let [ks (keys entity)]
    (loop [rst (rest ks) k (first ks) sched (atom {})]
      (cond (= k :schedule/first-date)
            (swap! sched assoc k (f/unparse (f/formatters :date) (c/from-date (k entity))))
            (= k :schedule/last-date)
            (swap! sched assoc k (f/unparse (f/formatters :date-time) (c/from-date (k entity))))
            (= k :schedule/start-time)
            (swap! sched assoc k (f/unparse (f/formatters :hour-minute) (c/from-date (k entity))))
            (= k :schedule/end-time)
            (swap! sched assoc k (f/unparse (f/formatters :hour-minute) (c/from-date (k entity))))
            (= k :schedule/breaks)
            (swap! sched assoc k
                   (mapv #(assoc {} :schedule/interval-start (:schedule/interval-start %1)
                                 :schedule/interval-end (:schedule/interval-end %1)) (k entity)))
            :else 
            (swap! sched assoc k (k entity))
            )
      (if (> (count rst) 0) (recur (rest rst) (first rst) sched)
          @sched)))
  )

 ;; (swap! sched assoc k
 ;;     (mapv #(d/pull dat-db [:schedule/interval-start :schedule/interval-end] (:db/id %1))
 ;;         (k entity)))
;;
;; Find the schedule entities for that cover the requested date.
;;
(defn get-schedules
  "Return a list of schedule maps that cover the requested date."
  [& [date-time]]
  (let [d (or (and date-time (c/to-date date-time)) false)]
    (->>
     (if d
       (d/q '[:find [?sched ...]
              :in $ % ?date
              :where 
              (covers-date? ?date ?sched)
              ]
            (d/db conn)
            appt-rules
            d)
       (d/q '[:find [?sched ...]
              :in $
              :where 
              [?sched :schedule/first-date _]
              ]
            (d/db conn))
       )
       (map (fn [eid]
              (let [entity (d/entity (d/db conn) eid)]
                (sched-from-entity entity (d/db conn)))))
       ))
  )

;; Assign a schedule to a doctor....
;;
(defn assign-schedule
 [user-id-string schedule-id-string]
 (let
     [user-id (as-uuid user-id-string)
      sched-id (as-uuid schedule-id-string)
      dat-db (d/db conn)]
   (d/transact conn
               [[:db/add [:user/uid user-id]
                 :user/schedules [:schedule/schedid sched-id]]])
   ))

;; Obtain an appointment block for a user on a given day.
;; (defn has-appointment-block?
;;   [uid-string for-day])
(defn list-users
  []
  (d/q '[:find ?n ?u
         :in $ 
         :where
         [?e :user/fullName ?n]
         [?e :user/uid ?u]
         ]
       (d/db conn)
       ))

(defn get-user-scheds
  [uid-string & [date-time]]
  (let [uid (as-uuid uid-string)
        d (or (and date-time (c/to-date date-time)) false)]
    (if d (d/q '[:find [?scheds ...]
               :in $ % [?uid ?d]
               :where
               (user-schedule-for-date ?uid ?d ?scheds)]
               (d/db conn)
               appt-rules
               [[:user/uid uid] d]
               )
        (d/q '[:find [?scheds ...]
               :in $ % ?uent
               :where
               (user-schedules ?uent ?scheds)]
             (d/db conn)
             appt-rules
             [:user/uid uid]
             )
        )
    )
  )

(defn appointment-block-from-db
  "Return an appointment block for the user identified by uid-string for the day
 for-day in the DB if there is one, and return it. Otherwise return false."
  [uid-string for-day]
   (d/q '[:find ?appt-block
          :in $  % [?uid ?date]
          :where
          (user-appt-block-on-date ?uid ?date ?appt-block)]
        (d/db conn)
        appt-rules
        [(as-uuid uid-string) for-day]))
          
(defn create-appointment-block
  "Create an appointment block for the given user for the given day."
   [uid-string for-day dat-db]
  (let
      [schedule (d/entity dat-db (first (get-user-scheds uid-string for-day)))
       start-hours (t/hours (t/hour (c/from-date (:schedule/start-time schedule))))
       start-mins (t/minutes (t/minute (c/from-date (:schedule/start-time schedule))))
       end-hours (t/hours (t/hour (c/from-date (:schedule/end-time schedule))))
       end-mins (t/minutes (t/minute (c/from-date (:schedule/end-time schedule))))]
    (appt-utils/setup-appointment-block
     (t/plus for-day start-hours start-mins)
     (t/plus for-day end-hours end-mins)
     (mapv (fn [brk]
             (let
                 [brk-start (c/from-date (:schedule/interval-start brk))
                  brk-end (c/from-date (:schedule/interval-end brk))
                  brk-hours-start (t/hours (t/hour brk-start))
                  brk-mins-start (t/minutes (t/minute brk-start))
                  brk-hours-end (t/hours (t/hour brk-end))
                  brk-mins-end (t/minutes (t/minute brk-end))]
               (t/interval (t/plus for-day brk-hours-start brk-mins-start)
                           (t/plus for-day brk-hours-end brk-mins-end))))
           (:schedule/breaks schedule))
     (:schedule/appointment-duration schedule)
     uid-string
     )
    )
  )

(defn add-appointment-block
  "Add the appointmen block a-block to the database."
  [a-block]
  (let
      [
       user-ent [:user/uid (as-uuid (:appts-with a-block))]
       breaks (mapv
               #(conj {}
                      {:schedule/interval-start (c/to-date (t/start %1))}
                      {:schedule/interval-end   (c/to-date (t/end %1))})
               (:clinician-breaks a-block))
       avail-appts (mapv
              #(conj {}
                      {:appt/start-time (c/to-date (t/start %1))}
                      {:appt/end-time   (c/to-date (t/end %1))}
                      {:appt/expected-duration (:appt-duration a-block)}
                      {:appt/clinician user-ent})
              (appt-utils/get-available-appts a-block)
              )
       ]
    (d/transact conn
                [{:db/id (d/tempid :appointments -1)
                  :appt-block/clinician user-ent
                  :appt-block/start-time (c/to-date (:start-time a-block))
                  :appt-block/end-time (c/to-date (:end-time a-block))
                  :appt-block/breaks breaks
                  :appt-block/available-appointments avail-appts}
                 {:db/id user-ent
                  :user/appt-blocks (d/tempid :appointments -1)}])))



(defn get-appointment-block
  "Return an appointment block for the given user for the given day, from the
  database, if it exists, or if not, create one, add it to the database, and
  and return the version from the database."
  [uid-string for-day]
  (let [dat-db (d/db conn)
        a-block (appointment-block-from-db uid-string for-day)]
    (assert (<= (count a-block) 1)
            (str "Error: There is more than one appointment block for user " uid-string
                 " on day " (f/unparse (f/formatters :date-time) for-day)))
    (if (> (count a-block) 0) a-block
        (let
            [new-a-block (create-appointment-block uid-string for-day dat-db)]
          (add-appointment-block new-a-block)
          (appointment-block-from-db uid-string for-day)
          ))
    ))

;; Take an appointment


;; Return an appointment
