(ns scheduling.models.datomic-types
  (:require
   [datomic.api :as d :exclusions [joda-time]]
   [clojure.pprint :refer (pprint)]
   [compojure.coercions :refer :all]
   [clj-time.format :as f]
   [clj-time.coerce :as c]
   [clojure.string :as cstr]
   [taoensso.timbre :as timbre
    :refer (log trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)]
   )
  (:import [java.lang Exception])
  (:gen-class)
  )

(defn translate-datomic-entity-map
  [dt-map]
  (reduce (fn [memo map-ent]
            (let [k (subs (str (key map-ent)) 1)
                  v (val map-ent)]
              (assoc memo
                     (keyword (cstr/replace k "/" "-"))
                     (if (vector? v)
                       (mapv #(translate-datomic-entity-map %1) v) v)
                     )))
          {}
          dt-map
          ))

;;(vec-form [this])
(defprotocol INTO_DATOMIC
  (map-form [this])
  )

;; Patient record for loading seed data, and creating a datomic compatible
;; representation for insertion into the datomic database.
(defrecord Patient
    [patient-firstName patient-lastName patient-title patient-gender
     patient-phoneNum patient-streetAddress patient-city
     patient-state patient-country patient-postCode patient-email
     patient-medicareNumber patient-dob patient-startDate
     patient-consultNotes patient-testResults patient-diagnosis
     patient-correspondence  
     patient-treatmentDesc patient-billing]
  INTO_DATOMIC
  (map-form [pat]
    (let
        [pat-fields
         (reduce
          #(assoc %1
                  (keyword (cstr/replace (name (key %2)) "-" "/"))
                  (val %2)) {} pat)
         full-name (str (:patient-firstName pat) " " (:patient-lastName pat))
         sort-name (str (:patient-lastName pat) " " (:patient-firstName pat))
         start-date (java.util.Date.)
         d-formatter (f/formatter "MM/dd/yyyy")
         dob (c/to-date (f/parse d-formatter (:patient-dob pat)))
         pid (d/squuid)
         gender (keyword (str "person.gender/" (:patient-gender pat)))
         ]
      (assoc  (dissoc pat-fields :patient/dob :patient/gender)
              :patient/fullName full-name
              :patient/sortName sort-name
              :patient/startDate start-date
              :patient/dob dob
              :patient/pid pid
              :patient/gender gender
              )))
  )

(defrecord User
    [user-loginName user-fullName user-title user-phoneNum user-streetAddress
     user-city user-state user-country user-postCode user-email
     user-medicareProviderNumber user-startDate
     user-roles]
  INTO_DATOMIC
  (map-form [user]
    (let
        [user-fields
         (reduce
          #(assoc %1
                  (keyword (cstr/replace (name (key %2)) "-" "/"))
                  (val %2)) {} user)
         user-roles
         (mapv #(keyword (str "user.role/" %1)) (cstr/split (cstr/trim (:user/roles user-fields)) #":"))
         start-date (c/to-date (f/parse (f/formatters :date) (cstr/trim (:user/startDate user-fields))))
         uid (d/squuid)
         ]
      (assoc
       (dissoc user-fields :user/roles :user/startDate)
       :user/role user-roles
       :user/startDate start-date
       :user/uid uid
       :user/password (:user/loginName user-fields))
      )))

;; A schedule interval is defined by:
;;
;; - schedule-intervalStart: A string :hour-minute time representation.
;; - schedule-intervalEnd: A string :hour-minute time representation.
(defrecord SchedInterval
    [schedule-intervalStart schedule-intervalEnd]
  INTO_DATOMIC
  (map-form [interval]
    (reduce
     #(assoc %1
             (keyword (cstr/replace (name (key %2)) "-" "/"))
             (c/to-date (f/parse (f/formatters :hour-minute) (val %2)))) {} interval)
      )
  )

(defn schedInterval
  "SchedInterval constructor. Takes a map of the form:
{:schedule-startInterval \"11:00\" :schedule-endInterval \"11:15\"}
as input."
  [int-map]
  (map->SchedInterval int-map)
  )

;; A day-schedule contains:
;; - schedule-startTime
;; - end-time
;; - breaks:  a list of schedule intervals.
;; - appointment length
;; - unique id
;; "Schedule record."
(defrecord DaySchedule
    [schedule-startTime schedule-endTime schedule-breaks schedule-apptDuration]
  INTO_DATOMIC
  (map-form [sched]
    (let
        [brks (mapv #(map-form %1) (:schedule-breaks sched))
         sched-map
         (reduce #(apply assoc %1
                         (cond (or (= (key %2) :schedule-startTime)
                                   (= (key %2) :schedule-endTime))
                               [(keyword (cstr/replace (name (key %2)) "-" "/"))
                                (c/to-date (f/parse (f/formatters :hour-minute) (val %2)))]
                               :else
                               [(keyword (cstr/replace (name (key %2)) "-" "/"))
                                (val %2)])
                         ) {} (dissoc sched :schedule-breaks))
         ]
      (assoc sched-map (keyword (cstr/replace "schedule-breaks" "-" "/")) brks))
    ))

  
(defn daySchedule
  "Constructor for daySchedule records."
  [sched-map]
  (let [bks (mapv #(schedInterval %1) (:schedule-breaks sched-map))
        first-part (dissoc sched-map :schedule-breaks)]
    (map->DaySchedule (assoc first-part :schedule-breaks bks
                          :schedule-dsid (d/squuid)))))


;; roster-weekly contains two vectors:
;;
;; - day-indices: A vector of length 7 that for each index in 0..6 contains an index
;; into day-records for a map containing the DaySchedule for that day.
;; - day-records: A vector of maps containing a daySchedule or no schedule for each
;;   of the specified rostered days of the week.
(defrecord RosterWeekly
    [schedule-dayIndices schedule-rosterDays]
  INTO_DATOMIC
  (map-form [rw]
    (let
        [di-vec (:schedule-dayIndices rw)
         dr-vec (mapv #(if (= %1 :schedule-notRostered)
                         (keyword "schedule/notRostered") (map-form %1))
                      (:schedule-rosterDays rw))]
      (assoc {} (keyword (cstr/replace (name :schedule-dayIndices) "-" "/")) di-vec
             (keyword (cstr/replace (name :schedule-rosterDays) "-" "/")) dr-vec))
    ))

(defn rosterWeekly
  [r-map]
  (let
      [day-recs (mapv #(if (= %1 :schedule-notRostered) :schedule-notRostered
                           (daySchedule %1)) (:schedule-rosterDays r-map))
       first-part (dissoc r-map :schedule-rosterDays)]
    (map->RosterWeekly (assoc first-part :schedule-rosterDays day-recs))
    )
  )


(defrecord RosterDaily
    [schedule-rosterDays]
    INTO_DATOMIC
    (map-form [rw]
      (let
          [dr-vec (mapv #(if (= %1 :schedule-notRostered) (keyword "schedule/notRostered")
                             (map-form %1))
                        (:schedule-rosterDays rw))]
        (assoc {} (keyword (cstr/replace (name :schedule-rosterDays) "-" "/")) dr-vec)
      )
    )
  )

(defn rosterDaily
  [r-map]
  (let
      [day-recs (mapv #(if (= %1 :schedule-notRostered) :schedule-notRostered (daySchedule %1))
                      (:schedule-rosterDays r-map))]
    (map->RosterDaily (assoc {} :schedule-rosterDays day-recs))))


;; (defrecord RosterDays
;;     [schedule-rosterDays]
;;   INTO_DATOMIC
;;   (map-form [rw]
;;     (assoc {} (keyword (cstr/replace (name :schedule-rosterDays) "-" "/"))
;;            (if (contains? (:schedule-rosterDays rw) :schedule-weeklyRoster)
;;              (assoc {} (keyword (cstr/replace (name :schedule-weeklyRoster) "-" "/"))
;;                     (map-form (:schedule-weeklyRoster (:schedule-rosterDays rw))))
;;              (assoc {} (keyword (cstr/replace (name :schedule-dailyRoster) "-" "/"))
;;                     (map-form (:schedule-dailyRoster (:schedule-rosterDays rw))))))))

(defrecord RosterSpec
    [schedule-rosterSpec]
  INTO_DATOMIC
  (map-form [rw]
    (if (contains? (:schedule-rosterSpec rw) :schedule-weeklyRoster)
    (assoc {} (keyword (cstr/replace (name :schedule-weeklyRoster) "-" "/"))
           (map-form (:schedule-weeklyRoster (:schedule-rosterSpec rw))))
    (assoc {} (keyword (cstr/replace (name :schedule-dailyRoster) "-" "/"))
           (map-form (:schedule-dailyRoster (:schedule-rosterSpec rw)))))))


(defn rosterSpec
  [rd-map]
  (if (contains? rd-map :schedule-weeklyRoster)
    (->RosterSpec (assoc {} :schedule-weeklyRoster (rosterWeekly (:schedule-weeklyRoster rd-map))))
    (->RosterSpec (assoc {} :schedule-dailyRoster (rosterDaily (:schedule-dailyRoster rd-map))))
    )
  )


(defrecord RosterSched
    [schedule-rsFirstDate schedule-rsLastDate schedule-daySpec schedule-rsExceptions]
  INTO_DATOMIC
  (map-form [rs]
    (reduce
     #(if (not (val %2)) %1
          (apply assoc %1
                 (cond
                   (= (key %2) :schedule-rsFirstDate)
                   [(keyword (cstr/replace (name (key %2)) "-" "/"))
                    (c/to-date (f/parse (f/formatters :date) (val %2)))]
                   (= (key %2) :schedule-rsLastDate)
                   [(keyword (cstr/replace (name (key %2)) "-" "/"))
                    (c/to-date (f/parse (f/formatters :date-time) (val %2)))]
                   (= (key %2) :schedule-daySpec)
                   (let
                       [kw1 (keyword (cstr/replace (name (key %2)) "-" "/"))
                        v (map-form (val %2))]
                     [kw1  v])
                   :else
                   [(keyword (cstr/replace (name (key %2)) "-" "/"))
                    (val %2)]
                   )))
     {} rs)
    )
  )

(defn rosterSched
  [r-map]
  (let [s-roster-ds (rosterSpec (get r-map :schedule-daySpec))]
    (map->RosterSched (assoc (dissoc r-map :schedule-daySpec)
                             :schedule-daySpec s-roster-ds))
    )
  )

(defrecord ClinicianRoster
    [schedule-crFirstDate schedule-crLastDate schedule-addedBy schedule-description
     schedule-name schedule-rosterUsers schedule-rosterScheds]
    INTO_DATOMIC
    (map-form [cr]
      {})
    )

(defn clinicianRoster
  [cr-map]
  (let
      [s-roster-scheds (mapv #(rosterSched %1) (:schedule-rosterScheds cr-map))]
    (map->ClinicianRoster (assoc cr-map :schedule-rosterScheds s-roster-scheds)))
  )
