(ns scheduling.models.db-recs
  (:require
   [datomic.api :as d :exclusions [joda-time]]
   [clojure.pprint :refer (pprint)]
   [compojure.coercions :refer :all]
   [clj-time.format :as f]
   [clj-time.coerce :as c]
   [clojure.string :as cstr]
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]
   [taoensso.timbre :as timbre
    :refer (log trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)]
   [scheduling.models.datomic-types :refer :all]
   )
  (:import [java.lang Exception]
           [org.joda.time DateTime]
           [scheduling.models.datomic_types SchedInterval DaySchedule RosterWeekly
            RosterDaily RosterSpec RosterSched ClinicianRoster])
  )

;;(vec-form [this])

(defn db-after-to-map
  [tx]
  (let
      [dba (seq (d/q '[:find ?e ?aname ?v ?added
                        :in $ [[?e ?a ?v _ ?added]]
                        :where
                        [?e ?a ?v _ ?added]
                        [?a :db/ident ?aname]]
                     (:db-after @tx)
                      (:tx-data @tx)))]
    (reduce #(assoc %1 (nth %2 1) (nth %2 2)) {} dba)
    )
  )

(defn add-rec-to-datomic
  "Simple function to add a record to the requested partition in dtomic."
  [conn rec part test-transact]
  (let
      [dmap (map-form rec)
       p (keyword (str "db.part/" part))
       tx (d/transact conn [(assoc dmap :db/id (d/tempid p))])]
    (when test-transact
      (let
          [db-after-map (db-after-to-map tx)]
        (if (= 0 (count (remove #(contains? db-after-map (key %1)) dmap))) true
            (do
              (timbre/error "Error adding record to datomic. Record is:")
              (timbre/error (str dmap))
              false)
          )
        )
      )
    @tx
    )
  )

(defn add-rec-to-datomic-return-eid
  "Simple function to add a record to the requested partition in dtomic."
  [conn rec part test-transact]
  (let
      [dmap (map-form rec)
       p (keyword (str "db.part/" part))
       tid (d/tempid p)
       tx (d/transact conn [(assoc dmap :db/id tid)])]
    (when test-transact
      (let
          [db-after-map (db-after-to-map tx)]
        (if (= 0 (count (remove #(contains? db-after-map (key %1)) dmap))) true
            (do
              (timbre/error "Error adding record to datomic. Record is:")
              (timbre/error (str dmap))
              false)
          )
        )
      )
    (d/resolve-tempid (d/db conn) (:tempids @tx) tid)
    )
  )

(defn show-transaction
  [tx]
  (let
      [dba (:db-after @tx)
       txd (:tx-data @tx)]
    (pprint (seq (d/q '[:find ?e ?aname ?v ?added
                        :in $ [[?e ?a ?v _ ?added]]
                        :where
                        [?e ?a ?v _ ?added]
                        [?a :db/ident ?aname]]
                      dba
                      txd)))
    )
  )

(def sched-rules
  '[[(sched-interval-entities ?se)
     [?se :schedule/intervalStart _]
     [?se :schedule/intervalEnd _]]
    [(day-schedule-entities ?dse)
     [?dse :schedule/dsid _]]
    [(roster-weekly-entities ?rwe)
     (?rwe :schedule/rosterDays _)]
    ])

(def user-rules
  '[[(user-entities ?ue)
     (?ue :user/uid _)]
    [(user-for-login ?login ?eid)
     (?eid :user/loginName ?login)]
    [(clinician-entities ?ce)
     (?ce :user/role :user.role/clinician)]])

(defn get-user-eids
  [conn]
  (d/q '[:find [?ue ...]
         :in $ %
         :where (user-entities ?ue)]
       (d/db conn)
       user-rules
       )
  )

(defn get-clinician-eids
  [conn]
  (d/q '[:find [?ce ...]
         :in $ %
         :where (clinician-entities ?ce)]
       (d/db conn)
       user-rules
       )
  )

(defn get-user-eid-for-login
  [conn login]
  (d/q '[:find ?eid .
         :in $ % ?login
         :where (user-for-login ?login ?eid)]
       (d/db conn)
       user-rules
       login
       )
  )

(defn user-entity-to-map
  [ety]
  (translate-datomic-entity-map ety)
  )

(defn get-users
  [conn]
  (mapv #(-> (d/entity (d/db conn) %1) user-entity-to-map) (get-user-eids conn)))

(defn get-clinicians
  [conn]
  (mapv #(-> (d/entity (d/db conn) %1) user-entity-to-map) (get-clinician-eids conn)))

(defn get-sched-interval-eids
  "Return a list of all schedule interval entities."
  [conn]
  (d/q '[:find [?se ...]
         :in $ %
         :where (sched-interval-entities ?se)]
       (d/db conn)
       sched-rules
       )
  )

(defn sched-interval-entity-to-map
  [ent]
  (let         
      [db-rec (translate-datomic-entity-map ent)]
    (reduce
     #(assoc %1
             (key %2)
             (f/unparse (f/formatters :hour-minute) (DateTime. (val %2))))
     {} db-rec))
  )

(defn sched-interval-to-map
  [conn eid]
  (sched-interval-entity-to-map (d/entity (d/db conn) eid)))


(defn get-sched-interval-maps
  "Return a list of all SchedInterval records."
  [conn]
  (let
      [eids (get-sched-interval-eids conn)]
    (loop [eid (first eids) next (rest eids) recs []]
      (if (not eid) recs
          (recur (first next) (rest next)
                 (conj recs (sched-interval-to-map conn eid))))
          )))


(defn get-day-sched-eids
  "Return a list of all DaySchedul entities."
  [conn]
  (d/q '[:find [?se ...]
         :in $ %
         :where (day-schedule-entities ?se)]
       (d/db conn)
       sched-rules
       )
  )

(defn get-day-sched-map
  "Return a list of all DaySchedule records."
  [ds-entity]
  (let
      [tmp (into {} ds-entity)
       breaks (mapv #(sched-interval-entity-to-map %1) (:schedule/breaks tmp))
       db-map (translate-datomic-entity-map (dissoc tmp :schedule/breaks))
       st (f/unparse (f/formatters :hour-minute) (DateTime. (:schedule-startTime db-map)))
       et (f/unparse (f/formatters :hour-minute) (DateTime. (:schedule-endTime db-map)))
       ]
    (assoc db-map
           :schedule-breaks breaks
           :schedule-startTime st
           :schedule-endTime et)
    ))

(defn get-day-sched-maps
  "Return a list of all DaySchedule records."
  [conn]
  (let
      [eids (get-day-sched-eids conn)]
    (loop [eid (first eids) next (rest eids) recs []]
      (if (not eid) recs
          (let
              [new-rec (get-day-sched-map (d/entity (d/db conn) eid))
               ]
            (recur (first next) (rest next)
                   (conj recs
                         new-rec)))))))


;; 
(defn add-roster-weekly-to-datomic
  [conn ^RosterWeekly rec part]
  (let
      [
       p (keyword (str "db.part/" part))
       di (get-in rec [:schedule-weeklyRoster :schedule-dayIndices])
       dr (mapv #(if (= :schedule-notRostered %1) (d/entid (d/db conn) :schedule/notRostered)
                     (add-rec-to-datomic-return-eid conn %1 "appointments" false))
                (get-in rec [:schedule-weeklyRoster :schedule-rosterDays]))
       tid (d/tempid p)
       dt-transact (assoc {} :db/id tid
                          :schedule/rosterDays 
                          (loop [cur-i (first di) rest-i (rest di) idx 0 nested-items []]
                            (if (not cur-i) nested-items
                                (recur (first rest-i) (rest rest-i) (+ 1 idx)
                                       (conj nested-items
                                             {:list/index idx :list/datum (nth dr cur-i)})))
                            )
                          :schedule/specType (d/entid (d/db conn) :schedule/specWeekly))
       tx (d/transact conn
                      [dt-transact])]
    (println "ADD-ROSTER-WEEKLY-TO-DATOMIC:")
    (pprint  dt-transact)
    (d/resolve-tempid (d/db conn) (:tempids @tx) tid))
  )

(defn get-roster-weekly-eids
  "Return a list of all DaySchedul entities."
  [conn]
  (d/q '[:find [?rwe ...]
         :in $ %
         :where (roster-weekly-entities ?rwe)]
       (d/db conn)
       sched-rules
       )
  )

(defn get-roster-weekly-map
  "Return the RosterWeekly map for entity id eid."
  [conn ety]
  (let
      [
       nr-eid (d/entid (d/db config.setup-db/conn) :schedule/notRostered)
       nr-entity (d/entity (d/db conn) nr-eid)
       days (:schedule/rosterDays ety)
       dl-start {:datums {} :dlist [-1 -1 -1 -1 -1 -1 -1]}
       day-map (loop [el (first days) el-rest (rest days) dl dl-start]
                 (if (not el) dl
                     (let
                         [l-index (:list/index el)
                          l-entity (if (= :schedule/notRostered (:list/datum el))
                                     {nr-eid nr-entity}
                                     {(:db/id (:list/datum el)) (:list/datum el)}
                                     )]
                         (recur (first el-rest) (rest el-rest)
                                (assoc dl
                                       :dlist (assoc (:dlist dl) (:list/index el) (key (first l-entity)))
                                       :datums (if (contains? (:datums dl) (key (first l-entity)))
                                                 (:datums dl)
                                                 (assoc (:datums dl) (key (first l-entity)) (val (first l-entity))))
                                       ))))
                 )]
    (loop
        [dl (:dlist day-map) dt (:datums day-map) days [] day-recs []]
      (if (empty? dl) {:schedule-dayIndices days :schedule-rosterDays day-recs}
          (let
              [curr (first dl)
               new-day-recs (if (= (type (get dt curr)) java.lang.Long) day-recs
                                (conj day-recs (if (= nr-eid curr) :schedule/notRostered
                                                   (get-day-sched-map (get dt curr)))))
               new-dt (if (= (type (get dt curr)) java.lang.Long) dt
                          (assoc dt curr (- (count new-day-recs) 1)))
               ]
              (recur (rest dl) new-dt (conj days (get new-dt curr)) new-day-recs))))
    ))


(defn add-roster-daily-to-datomic
  "Add a RosterDaily record to datomic"
  [conn ^RosterDaily rec part]
  (let
      [
       p (keyword (str "db.part/" part))
       dr (mapv #(if (= :schedule-notRostered %1) (d/entid (d/db conn) :schedule/notRostered)
                     (add-rec-to-datomic-return-eid conn %1 "appointments" false))
                ;;(get-in rec [:schedulerosterSpec :schedule-dailyRoster :schedule-rosterDays])
                (:schedule-rosterDays (:schedule-dailyRoster rec))
                )
       tid (d/tempid p)
       dt-transact (assoc {} :db/id tid
                          :schedule/rosterDays 
                          (loop [cur-eid (first dr) rest-eids (rest dr) idx 0 nested-items []]
                            (if (not cur-eid) nested-items
                                (recur (first rest-eids) (rest rest-eids) (+ 1 idx)
                                       (conj nested-items
                                             {:list/index idx :list/datum cur-eid})))
                            )
                          :schedule/specType (d/entid (d/db conn) :schedule/specDaily)
                          )
       tx (d/transact conn
                      [dt-transact])]
    (println "ADD-ROSTER-DAILY-TO-DATOMIC:")
    (pprint dt-transact)
    (d/resolve-tempid (d/db conn) (:tempids @tx) tid))
  )

(defn get-roster-daily-map
  "Return the RosterDaily map for entity id eid."
  [ety]
  (let [
        days (sort-by :list/index (:schedule/rosterDays ety))
        ]
    (assoc {} :schedule-rosterDays
           (mapv #(if (= (:list/datum %1) :schedule/notRostered) :schedule/notRostered
                      (get-day-sched-map (:list/datum %1))) days))
    ))

(defn add-roster-spec-to-datomic
  "Add a RosterSpec record to datomic"
  [conn ^RosterSpec rec part]
  (let
      [day-spec (if (= :schedule-dailyRoster (first (keys (:schedule-rosterSpec rec))))
                  (add-roster-daily-to-datomic conn (:schedule-rosterSpec rec) part)
                  (add-roster-weekly-to-datomic conn (:schedule-rosterSpec rec) part))
       dmap (assoc {} :schedule/rosterSpec day-spec)
       p (keyword (str "db.part/" part))
       tid (d/tempid p)
       tx (d/transact conn [(assoc dmap :db/id tid)])
       ]
    (pprint (assoc dmap :db/id tid))
    (d/resolve-tempid (d/db conn) (:tempids @tx) tid))
    )

(defn get-roster-spec-map
  "Return the RosterDays map for entity id eid."
  [conn ety]
  (let [
        ;;ety (d/entity (d/db conn) eid)
        ds-map (if (= (:schedule/specType (:schedule/rosterSpec ety)) :schedule/specWeekly)
                 (assoc {} :schedule-rosterSpec (get-roster-weekly-map conn (:schedule/rosterSpec ety))
                        :schedule-specType :schedule-specWeekly)
                 (assoc {} :schedule-rosterSpec (get-roster-daily-map (:schedule/rosterSpec ety))
                        :schedule-specType :schedule-specDaily)
                 )
        ]
    ds-map
    ))

(defn add-roster-sched-to-datomic
  "Add a ClinicianRoster record to datomic"
  [conn ^RosterSched rec part]
  (let
      [day-spec (add-roster-spec-to-datomic conn (:schedule-daySpec rec) "appointments")
       dmap (assoc (map-form rec) :schedule/daySpec day-spec)
       p (keyword (str "db.part/" part))
       tid (d/tempid p)
       tx (d/transact conn [(assoc dmap :db/id tid)])
       ]
    (pprint (assoc dmap :db/id tid))
    (d/resolve-tempid (d/db conn) (:tempids @tx) tid))
    )

(defn get-roster-sched-map
   "Return the RosterDaily map for entity id eid."
   [conn ety]
   (let [
         first-date (f/unparse (f/formatters :date) (DateTime. (:schedule/rsFirstDate ety)))
         last-date (f/unparse (f/formatters :date-time) (DateTime. (:schedule/rsLastDate ety)))
         roster-spec (get-roster-spec-map conn (:schedule/daySpec ety))
         ]
     (assoc {} :schedule-rsFirstDate first-date
            :schedule-rsLastDate last-date
            :schedule-daySpec roster-spec)
     ))

(defn make-inc
  []
  (let [inc-val (atom -1)]
    (fn []
      (swap! inc-val inc)
      @inc-val)))

(defn add-clinician-roster-to-datomic
  [conn ^ClinicianRoster rec part]
  (let
      [
       p (keyword (str "db.part/" part))
       tid (d/tempid p)
       cr-map (assoc {}
                     :db/id tid
                     :schedule/crFirstDate (c/to-date (f/parse (f/formatters :date) (:schedule-crFirstDate rec)))
                     :schedule/crLastDate (c/to-date (f/parse (f/formatters :date-time) (:schedule-crLastDate rec)))
                     :schedule/description (:schedule-description rec)
                     :schedule/name (:schedule-name rec)
                     :schedule/crid (d/squuid)
                     :schedule/addedBy (get-user-eid-for-login conn (:schedule-addedBy rec)))
       r-scheds-idx (make-inc)
       roster-scheds (mapv #(assoc {}
                                   :list/index (r-scheds-idx)
                                   :list/datum (add-roster-sched-to-datomic conn %1 "appointments"))
                           (:schedule-rosterScheds rec))
       roster-users (mapv #(assoc {}
                                  :schedule/forUser (get-user-eid-for-login conn (key %1))
                                  :schedule/rsIndices (val %1))
                          (:schedule-rosterUsers rec))
       final-map (assoc cr-map
                        :schedule/rosterScheds roster-scheds
                        :schedule/rosterUsers roster-users)
       tx (d/transact conn
                      [final-map])]
    (println "ADD-CLINICIAN-ROSTER-TO-DATOMIC:")
    (pprint  final-map)
    (d/resolve-tempid (d/db conn) (:tempids @tx) tid))
  )

(defn get-clinician-roster-map
  "Return the clinicianRoster map for entity ety."
  [conn ety]
  (let
      [
       first-date (f/unparse (f/formatters :date) (DateTime. (:schedule/crFirstDate ety)))
       last-date (f/unparse (f/formatters :date-time) (DateTime. (:schedule/crFirstDate ety)))
       desc (:schedule/description ety)
       nm (:schedule/description ety)       
       crid (:schedule/crid ety)
       added-by (:user/loginName (:schedule/addedBy ety))
       roster-scheds (mapv #(get-roster-sched-map conn (:list/datum %1))
                           (sort-by :list/index (:schedule/rosterScheds ety)))
       roster-users (reduce
                     #(assoc %1 (:user/loginName (:schedule/forUser %2)) (vec (:schedule/rsIndices %2)))
                     {}
                     (:schedule/rosterUsers ety))
       
       ]
    (assoc {}
           :schedule-crFirstDate first-date
           :schedule-crLastDate last-date
           :schedule-description desc
           :schedule-name desc           
           :schedule-crid crid
           :schedule-addedBy added-by
           :schedule-rosterScheds roster-scheds
           :schedule-rosterUser roster-users)
       ))

(def roster-rules
  '[[(sched-roster-eids ?eid)
     [?eid :schedule/crFirstDate _]]
    ])

(defn get-clinician-roster-eids
  "Return a list of all DaySchedul entities."
  [conn]
  (d/q '[:find [?eid ...]
         :in $ %
         :where (sched-roster-eids ?eid)]
       (d/db conn)
       roster-rules
       )
  )
