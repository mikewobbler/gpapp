(ns scheduling.models.sched-dt
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]            
            [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :refer (pprint)]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clj-time.periodic :as p]
            [clj-time.coerce :as c]            
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy get-env log-env)]            
            [clojure.string :as cstr]
            [utils.err-wrap :as err-handler]            
            [utils.fileUtils :refer :all]
            [config.setup-db :refer [conn]]
            ))

(def sched-rules
  '[[(covers-date? ?d ?e)
     [?e :schedule/last-date ?d-end]
     [(.before ^java.util.Date ?d ?d-end)]
     [?e :schedule/first-date ?d-start]
     [(.after ^java.util.Date ?d ?d-start)]]
    
    [(user-schedules ?user-ent ?scheds)
     [?user-ent :user/schedules ?scheds]]

    [(user-schedule-for-date ?user-ent ?d ?sched)
     [?user-ent :user/schedules ?sched]
     [?sched :schedule/last-date ?d-end]
     [(.before ^java.util.Date ?d ?d-end)]
     [?sched :schedule/first-date ?d-start]
     [(.after ^java.util.Date ?d ?d-start)]
     ]

    [(user-appt-block-on-date ?uid ?date ?appt-block)
     [?e :user/uid  ?uid]
     [?e :user/appt-blocks  ?appt-block]
     [?appt-block :appt-block/start-time ?st]
     [(scheduling.models.appts.dt-appts/same-date? ?st ?date)]]])



(defn sched-from-entity
  "Create a map from the supplied entity."
  [entity dat-db]
  (let [ks (keys entity)]
    (loop [rst (rest ks) k (first ks) sched (atom {})]
      (cond (= k :schedule/first-date)
            (swap! sched assoc k (f/unparse (f/formatters :date) (c/from-date (k entity))))
            (= k :schedule/last-date)
            (swap! sched assoc k (f/unparse (f/formatters :date-time) (c/from-date (k entity))))
            (= k :schedule/start-time)
            (swap! sched assoc k (f/unparse (f/formatters :hour-minute) (c/from-date (k entity))))
            (= k :schedule/end-time)
            (swap! sched assoc k (f/unparse (f/formatters :hour-minute) (c/from-date (k entity))))
            (= k :schedule/breaks)
            (swap! sched assoc k
                   (mapv #(assoc {} :schedule/interval-start (:schedule/interval-start %1)
                                 :schedule/interval-end (:schedule/interval-end %1)) (k entity)))
            :else 
            (swap! sched assoc k (k entity))
            )
      (if (> (count rst) 0) (recur (rest rst) (first rst) sched)
          @sched)))
  )

;;
;; Find the schedule entities for that cover the requested date.
;;
(defn get-schedules
  "Return a list of schedule maps that cover the requested date."
  [& [date-time]]
  (let [d (or (and date-time (c/to-date date-time)) false)]
    (->>
     (if d
       (d/q '[:find [?sched ...]
              :in $ % ?date
              :where 
              (covers-date? ?date ?sched)
              ]
            (d/db conn)
            sched-rules
            d)
       (d/q '[:find [?sched ...]
              :in $
              :where 
              [?sched :schedule/first-date _]
              ]
            (d/db conn))
       )
       (map (fn [eid]
              (let [entity (d/entity (d/db conn) eid)]
                (sched-from-entity entity (d/db conn)))))
       ))
  )

;; Assign a schedule to a doctor....
;;
(defn assign-schedule
 [user-id-string schedule-id-string]
 (let
     [user-id (as-uuid user-id-string)
      sched-id (as-uuid schedule-id-string)
      dat-db (d/db conn)]
   (d/transact conn
               [[:db/add [:user/uid user-id]
                 :user/schedules [:schedule/schedid sched-id]]])
   ))

(defn get-user-scheds
  [uid-string & [date-time]]
  (let [uid (as-uuid uid-string)
        d (or (and date-time (c/to-date date-time)) false)]
    (if d (d/q '[:find [?scheds ...]
               :in $ % [?uid ?d]
               :where
               (user-schedule-for-date ?uid ?d ?scheds)]
               (d/db conn)
               sched-rules
               [[:user/uid uid] d]
               )
        (d/q '[:find [?scheds ...]
               :in $ % ?uent
               :where
               (user-schedules ?uent ?scheds)]
             (d/db conn)
             sched-rules
             [:user/uid uid]
             )
        )
    )
  )
