(ns scheduling.models.sched-test
  (:require
   [datomic.api :as d :exclusions [joda-time]]
   [clj-time.core :as t]
   [clj-time.local :as l]
   [clj-time.format :as f]
   [clj-time.periodic :as p]
   [clj-time.coerce :as c]
   [clojure.string :as cstr]
   [clojure.pprint :refer (pprint)]))



(def si-test1
  {:schedule-intervalStart "11:00" :schedule-intervalEnd "11:30"}
  )

(def si-test2
  {:schedule-intervalStart "11:30" :schedule-intervalEnd "12:30"}
  )


(def ds-test1
  {:schedule-startTime "09:00"
   :schedule-endTime "17:30"
   :schedule-breaks  [{:schedule-intervalStart "11:00" :schedule-intervalEnd "11:15"}
                      {:schedule-intervalStart "12:30" :schedule-intervalEnd "13:15"}
                      {:schedule-intervalStart "14:00" :schedule-intervalEnd "14:15"}]
   :schedule-apptDuration 15}
  )


;; An example roster weekly map with:
;; Monday, Tuesday and Friday having daySchedule:
;; {:schedule-startTime "09:00"
;;    :schedule-endTime "17:30"
;;    :schedule-breaks  [{:schedule-intervalStart "11:00" :schedule-intervalEnd "11:15"}
;;                       {:schedule-intervalStart "12:30" :schedule-intervalEnd "13:15"}
;;                       {:schedule-intervalStart "14:00" :schedule-intervalEnd "14:15"}]
;;    :schedule-apptDuration 15}
;; Wednesday has schedule:
;; {:schedule-startTime "12:00"
;;    :schedule-endTime "19:30"
;;    :schedule-breaks  [{:schedule-intervalStart "14:30" :schedule-intervalEnd "14:45"}
;;                       {:schedule-intervalStart "17:00" :schedule-intervalEnd "17:15"}]
;;    :schedule-apptDuration 15}
;; Thursday has schedule:
;; {:schedule-startTime "09:00"
;;    :schedule-endTime "16:30"
;;    :schedule-breaks  [{:schedule-intervalStart "12:30" :schedule-intervalEnd "13:30"}]
;;    :schedule-apptDuration 30}
;; Saturday and Sunday have:
;; :schedule-not-rostered
(def rw-test1
  {
   :schedule-dayIndices [0 0 1 2 0 3 3]
   :schedule-rosterDays
   [{:schedule-startTime "09:00"
     :schedule-endTime "17:30"
     :schedule-breaks  [{:schedule-intervalStart "11:00" :schedule-intervalEnd "11:15"}
                        {:schedule-intervalStart "12:30" :schedule-intervalEnd "13:15"}
                        {:schedule-intervalStart "14:00" :schedule-intervalEnd "14:15"}]
     :schedule-apptDuration 15}
    
    {:schedule-startTime "12:00"
     :schedule-endTime "19:30"
     :schedule-breaks  [{:schedule-intervalStart "14:30" :schedule-intervalEnd "14:45"}
                        {:schedule-intervalStart "17:00" :schedule-intervalEnd "17:15"}]
     :schedule-apptDuration 15}
    
    {:schedule-startTime "09:00"
     :schedule-endTime "16:30"
     :schedule-breaks  [{:schedule-intervalStart "12:30" :schedule-intervalEnd "13:30"}]
     :schedule-apptDuration 30}
    :schedule-notRostered
    ]})

(def rd-test1
  {
   :schedule-rosterDays
   [{:schedule-startTime "09:00"
     :schedule-endTime "17:30"
     :schedule-breaks  [{:schedule-intervalStart "11:00" :schedule-intervalEnd "11:15"}
                        {:schedule-intervalStart "12:30" :schedule-intervalEnd "13:15"}
                        {:schedule-intervalStart "14:00" :schedule-intervalEnd "14:15"}]
     :schedule-apptDuration 15}
    
    {:schedule-startTime "12:00"
     :schedule-endTime "19:30"
     :schedule-breaks  [{:schedule-intervalStart "14:30" :schedule-intervalEnd "14:45"}
                        {:schedule-intervalStart "17:00" :schedule-intervalEnd "17:15"}]
     :schedule-apptDuration 15}
    
    {:schedule-startTime "09:00"
     :schedule-endTime "16:30"
     :schedule-breaks  [{:schedule-intervalStart "12:30" :schedule-intervalEnd "13:30"}]
     :schedule-apptDuration 30}
    
    :schedule-notRostered
    
    {:schedule-startTime "09:00"
     :schedule-endTime "16:30"
     :schedule-breaks  [{:schedule-intervalStart "12:30" :schedule-intervalEnd "13:30"}]
     :schedule-apptDuration 30}
    
    :schedule-notRostered
    
    :schedule-notRostered
    
    {:schedule-startTime "12:00"
     :schedule-endTime "19:30"
     :schedule-breaks  [{:schedule-intervalStart "14:30" :schedule-intervalEnd "14:45"}
                        {:schedule-intervalStart "17:00" :schedule-intervalEnd "17:15"}]
     :schedule-apptDuration 15}    
    ]})

;; NEED to make rosterSpec have no daySpec component.
(def rDaysTest1 (assoc {} :schedule-dailyRoster rd-test1))
(def rDaysTest2 (assoc {} :schedule-weeklyRoster rw-test1))

;; 
(def c-roster-test1
  (assoc {}
         :schedule-daySpec rDaysTest1
         :schedule-rsFirstDate "2016-01-01"
         :schedule-rsLastDate "2016-06-30T23:59:59.999Z"
         :schedule-rsExceptions false
         ))

(def c-roster-test2
  (assoc {}
         :schedule-daySpec rDaysTest2
         :schedule-rsFirstDate "2016-01-01"
         :schedule-rsLastDate "2016-03-30T23:59:59.999Z"
         :schedule-rsExceptions false
         ))

(def c-roster-test3
  (assoc {}
         :schedule-daySpec rDaysTest1
         :schedule-rsFirstDate "2016-04-01"
         :schedule-rsLastDate "2016-06-30T23:59:59.999Z"
         :schedule-rsExceptions false
         ))

;; A clinicanRoster contains:
;; - A start date
;; - An end date
;; - A mapping between user logins, and RosterScheds
(def clinicianRoster-test1
  {
   :schedule-crFirstDate "2016-01-01"
   :schedule-crLastDate "2016-06-30T23:59:59.999Z"
   :schedule-addedBy "sjones"
   :schedule-description "First half 2016 roster for floreyh, isem and parko."
   :schedule-name "Schedule1"   
   :schedule-rosterUsers {
                          "floreyh" [0]
                          "isem" [1 2]
                          "parko" [0]}
   :schedule-rosterScheds [c-roster-test1 c-roster-test2 c-roster-test3]
   })


