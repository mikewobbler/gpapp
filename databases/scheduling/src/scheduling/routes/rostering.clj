(ns scheduling.routes.rostering
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]
            [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :as pp]
            [liberator.core
             :refer [defresource resource request-method-in]]
            [cheshire.core :refer :all]
            [taoensso.timbre :as timbre
              :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]
            [scheduling.views.layout :as layout]
            [config.setup-db :refer [conn]]
            [scheduling.models.db-recs :as dbr]
            )
  (:import (java.io StringWriter)))

(defn manage-rosters-page
  "Display the appointments for the current user."
  []
  (let
      [template-file "manage-rosters.html"
       ;;user (mu/ssget :user)
       ]
    (timbre/info "In manage-rosters route.")
    ;; (if user
      (layout/common template-file
                     {:title "Manage Rosters"
                      :css ["/css/bootstrap.min.css"
                            "/css/gpApp.css"]
                      :js-file "manage-rosters.js"
                      :heading "Manage Rosters: "
                      ;;:user user
                       })
      ;;(redirect "/login")
      ;; )
  ))

(defn get-roster-list
  "Return a list of rosters from the database."
  []
  (let
      [
       rlist (mapv #(dbr/get-clinician-roster-map conn (d/entity (d/db conn) %1))
                   (dbr/get-clinician-roster-eids conn))
       ]
    (generate-string {
                      :sortKey :schedule/crFirstDate
                      :link :schedule/crid
                      :dataList (or (and (empty? rlist) "None") rlist)}
                     )))

(defn get-create-form-data
  "Return the clinicians list, and any other data required for the create
  roster form."
  []
  (let [
        clinicians (dbr/get-clinicians conn)
        rlist (mapv #(dbr/get-clinician-roster-map conn (d/entity (d/db conn) %1))
                    (dbr/get-clinician-roster-eids conn))
        roster-id (d/squuid)
        ]
    (generate-string
     {
      :clinicians clinicians
      :rlist rlist
      :rid roster-id}
     ))
  )

(defresource get-rosters
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (get-roster-list))
  :etag "fixed-etag"
  :available-media-types ["application/json"])


(defresource roster-create-data
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (get-create-form-data))
  :etag "fixed-etag"
  :available-media-types ["application/json"])


(defn add-weekly-schedule
  [ws]
  (timbre/info "Got weekly schedule")
  (clojure.pprint/pprint ws)
  )

(defresource roster-new-weekly
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:post]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :post! (fn [context]
               (println "Testing")
               (timbre/info "Got request.")
               (timbre/info (str "Context is: " context))
               (let [dlist (get-in context [:request :params])]
                 (add-weekly-schedule dlist)))
  :handle-created (fn [context] (generate-string
                                 {
                                  :success true
                                  :x "Hi Mike"
                                  })
                    )
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"])

(defn tmp
  []
  (println "GOT A POST"))

(defroutes rostering-routes
  (POST "/new-weekly-schedule" request roster-new-weekly)
  (GET "/manage-rosters" [] (manage-rosters-page))
  (GET "/get-rosters" request get-rosters)
  (GET "/create-roster-data" request roster-create-data)
)

