(ns scheduling.views.layout
  (:require [selmer.parser :refer :all]))

;; Set the selmer resource path to the projects resources/templates directory
(set-resource-path! (clojure.java.io/resource "templates"))


(defn common [temp-name context-map]
  (render-file temp-name context-map))

(defn debug [context-map]
  (render "{% debug %}"
          context-map))


