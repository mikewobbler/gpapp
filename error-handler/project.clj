(defproject error-handler "0.1.0-SNAPSHOT"
  :description "Wrap functions in exception and error handling."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [com.taoensso/timbre "4.3.1"]]
  :source-paths ["src"]
  :main error-handler.err-wrap
  :target-path "target/%s"
    :profiles {:uberjar {:aot :all}}
  )
