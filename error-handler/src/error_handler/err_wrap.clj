(ns error-handler.err-wrap
  (:require (clojure [pprint :refer (pprint)]
                     [string :as cstr]
                     [walk :as walk])
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
              logf tracef debugf infof warnf errorf fatalf reportf
              spy get-env log-env)])  
  (:gen-class))

;; Example of a function that might use wrap-in-ehandler.
;;
;; (defn ^{:exceptions [java.lang.ArithmeticException Exception]}
;;   div-with-meta
;;   [a b]
;;   (/ a b)
;;   )

;; This function is called like this:
;; (def safe-func (wrap-in-exception-handler func (:exceptions (meta #'func))))
(defmacro wrap-in-ehandler
  [func & [catches]]
  `(fn [& args#]
     (try (apply ~func args#)
          ~@(map
             #(let [e (gensym)]
                (list `catch % e
                      (list `timbre/error "Caught Exception: "
                            ;;`(.getCause ~e) " "
                            `(.toString ~e)
                            )))
             (eval catches))
          )
     ))

;; This function is called like this:
;; (def safe-func (wrap-in-exception-handler func (:exceptions (meta #'func))))
(defmacro wrap-in-exception-handler
  [func & [catches]]
  `(fn [& args#]
     (try (apply ~func args#)
          ~@(map
             #(let [e (gensym)]
                (list `catch % e
                      (list `timbre/error "Caught Exception: "
                            `(.toString ~e)
                            )))
             (eval catches))
          )
     ))

;; Return function wrapped to catch exceptions.
(defn wrap-fn
  "Return a version of func wrapped in a try catch for handling exceptions."
  [func]
  (fn [& args]
    (try
      (apply func args)
      (catch Exception e
        (println (timbre/error "Caught Exception: " (.getMessage e)))
        )
      )
    )
  )

;; Macro that just wraps func in an a try/catch for the top level exception.
(defmacro wf
  [func]
  `(fn [& args#]
    (try (apply ~func args#)
         (catch Exception e# (timbre/error "Caught Exception: " (.getMessage e#)
                                  "\n" "Exception object is: " `(.toString e#))))))


