(defproject gp "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :repositories {"my.datomic.com" {:url "https://my.datomic.com/repo"
                                 :creds :gpg}}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.0"]
                 [hiccup "1.0.5"]
                 [ring-server "0.4.0"]
                 [liberator "0.14.0"]
                 [cheshire "5.5.0"]
                 [lib-noir "0.9.9"]
                 [selmer "1.0.4"]
                 [environ "1.0.2"]
                 [mount "0.1.10"]
                 [com.datomic/datomic-pro "0.9.5350"]
                 [org.clojure/java.jdbc "0.6.0-alpha2"]
                 [postgresql "9.3-1102.jdbc41"]
                 [clojure.joda-time "0.6.0"]
                 [org.clojure/data.csv "0.1.3"]
                 [me.raynes/fs "1.4.6"]
                 [com.taoensso/timbre "4.3.1"]
                 [clj-jgit "0.8.8"]
                 [clj-time "0.11.0"]
                 [org.clojars.hozumi/clj-commons-exec "1.2.0"]
                 ]
  :plugins [[lein-ring "0.8.12"]
            [lein-checkouts "1.1.0"]
            [lein-environ "1.0.2"]]
  :ring {:handler gp.handler/app
         :init gp.handler/init
         :destroy gp.handler/destroy}
  :profiles
  {:uberjar {:aot :all}
   :production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}}
   :dev
   {:dependencies [[ring-mock "0.1.5"] [ring/ring-devel "1.3.1"]]
    :env {:dev "true"}}})
