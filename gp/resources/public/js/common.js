// Setup requirejs to find modules, both ours, and external ones.
requirejs.config({
    baseUrl: '/js',
    // Modules starting with key's in the path's object, will be loaded from
    // the directory with that key's value name.
    paths: {
	"widget": "widget",
	"bootstrap": "../bower_components/bootstrap/dist/js"
    }
});
requirejs(['jquery', 'underscore', 'bootstrap.min'], function($, _) {
});
