define(['require', 'jquery', 'underscore', 'bootstrap', 'd3', 'widget/table'], function(require) {
    var ct = require('widget/table');
    var currentSelected = null;

    function renderUsers(userList) {
	var sortField = userList["sort-key"];
	var sorted = _.sortBy(userList["data-list"], sortField);
	var route = "/user-select/";

	ct.createTable
	(
	    userList["table-id"],
	    userList["link"],
	    userList["columns"],
	    sorted,
	    route,
	    handleUserClicked
	);
	ct.addSearchHandler(userList["table-id"], "list-search", sorted, sortField);
	$("#user-summary").css("visibility", "hidden");
    };

    function addButtonHandlers() {
	var newUser = $("#new-user");
	var removeUser = $("#remove-user");
	var editProfile = $("#edit-profile");

	newUser.click(function(evt) {
	    window.location.href = "/add-user";
	});

	removeUser.click(function(evt) { return rUser(evt, currentSelected);});
	editProfile.click(function(evt) { return eUser(evt, currentSelected);});

    };

    function eUser(evt,cs) {
	var user_uri = cs.data("href");
	if (user_uri.indexOf("undefined") == -1)
	{
	    $.ajax
	    (
		user_uri,
		{
		    dataType: "json",
		    cache : false
		}
	    ).done(renderUserProfile);
	}
    };

    function renderUserProfile(userP) {
	console.log("In renderUserProfile...");
	//var up = JSON.parse(userP);
	console.log("User Profile Data is: " + JSON.stringify(userP));
    };

    function rUser(evt, cs) {
	console.log("In rUser.");
	var uid = cs.data("href");
	var uname = $("td", cs).first().text();
	var lname = $("td", cs).last().text();
	console.log("Current userid is: " + uid);
	console.log("Current user name is: " + uname);
	console.log("Current login is: " + lname);

	$("#cancelRuser").click
	(
	    function(evt) {
		return cancelRemove(evt, uid, uname, lname);
	    }
	);

	$("#confirmRuser").click
	(
	    function(evt) {
		return confirmRemove(evt, uid, uname, lname);
	    }
	);

	$("#ruserDialog").modal({backdrop: 'static', show :false});
	$("#removeUserMsg").html("Are you sure you want to remove user:&nbsp; <b>"
				 + uname
				 + "</b>?");
	$("#ruserDialog").modal('show');
    };

    function cancelRemove(evt, uid, uname, lname) {
	$("#cancelRuser").off("click");
	$("#confirmlRuser").off("click");
	$("#ruserDialog").modal('hide');
    };

    function confirmRemove(evt, uid, uname, lname) {
	$("#cancelRuser").off("click");
	$("#confirmRuser").off("click");
	$("#ruserDialog").modal('hide');
	console.log("Removing user = " + uname );
	$.post("remove-user", {uid: uid.match(/[^/]+$/)[0]}, renderUsers, "json");
	
    };
    
    function handleUserClicked() {
	if (currentSelected != null) {
	    currentSelected.attr("style", "");
	}
	currentSelected = $(this);
	currentSelected.css('background-color', 'navy');
	var userUri = currentSelected.data("href");
	if (userUri.indexOf("undefined") == -1)
	{
	    $.ajax
	    (
		userUri,
		{
		    dataType: "json",
		    cache: false
		}
	    ).done(renderSelected);
	}
	$("button.btn[disabled]").prop("disabled", false);
	$("#user-summary").css("visibility", "visible");
    };

    function renderSelected(su) {
	console.log("Got:\n" + JSON.stringify(su));
	$("#user-fullName").text(su["user/fullName"] || "None");
	$("#user-login").text(su["user/loginName"] || "None");
	var roles = su["user/role"] &&
		_.reduce
	        (
		    su["user/role"],
		    function(memo, x)
		    {
			var m = (memo.length == 0) ? memo : ", " + memo;
			return /[^/]+$/.exec(x)[0] + m;},
		    ""
		) || "None";
	$("#user-roles").text(roles);
    }

    function getUsers() {
	//$.get("/get-users", renderUsers);
	$.ajax
	(
	    "/get-users",
	    {
		dataType: "json",
		cache : false
	    }
	).done(renderUsers);
    };

    $(function() {
	getUsers();
	addButtonHandlers();	
    });

});
