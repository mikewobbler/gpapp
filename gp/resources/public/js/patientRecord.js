define(['jquery', 'underscore', 'bootstrap', 'd3', 'tinymce'], function(require) {
    console.log("In patientRecord.js");

    function enableTabs() {
	$('#patient-record-tabs a').click(function(e) {
	    e.preventDefault();
	    $(this).tab('show');
	});
    };

    function populatePatientDetails(pdata) {
	$("#patient-fullName").text(pdata["patient/fullName"]);
	$("#patient-title").text(pdata["patient/title"]);
	var streetAddress = pdata["patient/streetAddress"];
	var city = pdata["patient/city"];
	var state = pdata["patient/state"];
	var country = pdata["patient/country"];
	var pcode = pdata["patient/postCode"];
	var pnum =  _.reduce(pdata["patient/phoneNum"],
			     function(memo, pnum) { return pnum + memo + "<br>";}, "");
	var email = _.reduce(pdata["patient/email"],
			     function(memo, pnum) { return pnum + memo + "<br>";}, "");
	var addr = streetAddress + "<br>" + city + ", " + state + ", " + country + "<br>";
	addr += "<strong>Phone: </strong> " + pnum + "<br>";
	addr += "<strong>Email: </strong> " + email;
	$("#pat-address").html(addr);

	// second side.
	$("#patient-medicareNumber").text(pdata["patient/medicareNumber"]);
	$("#patient-dob").text(
	    function() {
		var d = new Date(pdata["patient/dob"]);
		return d.toDateString();
	    });
	$("#patient-gender").text(
	    function() {
		var gender = pdata["patient/gender"];
		if (gender == "person.gender/Male")
		{
		    return "Male";
		}
		else if (gender == "person.gender/Female")
		{
		    return "Female";
		}
		else
		{
		    return "Other";
		}
	    });
    };

    var consultNote = {
	isDirty : false,
	pData : null,
	editor : null,
	save: null,
	cancel: null,
	consultNoteSubmit : function(evt) {
	    evt.preventDefault();
	    var editor = evt.data.cNote.editor;
	    if (editor.isDirty()) {
		var content = editor.getContent({format : 'html'});
		editor.startContent = content;
		editor.undoManager.add();
		var url = "/patient/consult-note";		
		$.post(url, {content : content, id : evt.data.cNote.pData['patient/pid']})
		    .done(function(data) {
			alert("Data sent: " + data);
		    });
	    }
	},

	setupConsultNote : function(pdata) {
	    this.isDirty = false;
	    this.pData = pdata;
	    this.save = $("#save-consult-note");
	    this.cancel = $("#cancel-consult-note");
	    this.editor = tinymce.activeEditor;
	    this.editor.setContent(pdata['patient/consultNotes']);
	    this.editor.undoManager.add();
	    //this.form = $("#consult-note");
	    this.save.on("click",
			 { cNote: this },
			 this.consultNoteSubmit);
	    this.cancel.on("click",
			   { cNote: this },
			   function(evt) {
			       evt.preventDefault();
			       var editor = evt.data.cNote.editor;
			       editor.undoManager.undo();
			       editor.undoManager.add();
			   });
	}
    };
    

    function setupConsult(pdata) {
	$("#consult-name").text(pdata["patient/fullName"]);
	var height = 0.7 * $("#consult-content").height();
	var width = 0.7 * window.innerWidth;
	$("#consult-editor1").width(width);
	$("#consult-editor1").height(height);
	tinymce.init({
	    selector: "#consult-editor1",
	    skin: 'gp-app', 
	    theme: 'modern',
	    width: width,
	    height: height,
	    setup: function(ed) {
		ed.on('init', function() {
		    //tinymce.activeEditor.setContent(pdata['patient/consultNotes']);
		    consultNote.setupConsultNote(pdata);
		});
	    }
	});
    };

    //$(document).on("pageload", function() {
    $(function() {
	enableTabs();
	var pdata = JSON.parse($("#patient-data")[0].dataset["content"]);
	populatePatientDetails(pdata);
	setupConsult(pdata);
    });
});
