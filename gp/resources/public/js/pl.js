define(function(require) {
    var $ = require('jq/jquery'),
	_ = require('uscore/underscore');
    console.log("In pl.js.\n");
    console.log("JQuery version is: " + $.fn.jquery);
    console.log("Underscore Version is: " + _.VERSION);

    function renderPatients(pats) {
	var patList = JSON.parse(pats);
	console.log("Got patients = " + patList);
	$('#patient-list').empty();
	for (var i = 0; i < patList.length; i++) {
	    $('#patient-list').append($('<li/>', {html: patList[i]}));
	}
    }
    function getPatients() {
	$.get("/patients", renderPatients);
    }

    $(function() {
	getPatients();
    });
});
