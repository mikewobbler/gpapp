var require = {
    // Setup requirejs to find modules, both ours, and external ones.
    baseUrl: '/js',

    // Modules starting with key's in the path's object, will be loaded from
    // the directory with that key's value name.
    paths: {
	"widget": "widget",
	"bootstrap": "../bower_components/bootstrap/dist/js/bootstrap.min",
	"jquery": "../bower_components/jquery/dist/jquery.min",
	"underscore": "../bower_components/underscore/underscore",
	"d3" : "../bower_components/d3/d3.min",
	"tinymce" : "../bower_components/tinymce/tinymce.min"
    },

    // Some dependencies we usually want loaded.
    deps: ['jquery', 'underscore', 'd3']
};
    

