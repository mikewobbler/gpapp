define(['jquery', 'd3', 'underscore'], function($) {
    'use strict';

    var us_sched = {};
    us_sched.createUs = function(schedTabId)
    {
	var usched = {};
	usched.tId = schedTabId;

	usched.getScheds = function(gotScheds_cb)
	{
	    $.ajax
	    (
		"/get-schedules",
		{
		    dataType: "json",
		    cache : false
		}
	    ).done(function(scheds)
		   {
		       usched.setupSchedTable(scheds);
		       gotScheds_cb();
		   });
	};

	usched.setupSchedTable = function(scheds) {
	    console.log("In setup SchedTable.");
	    console.log("Scheds are: " + JSON.stringify(scheds));
	    usched.scheds = scheds;
	};

	usched.createTableRows = function() {
	    return true;
	};
	
	return usched;
    };
	
    return us_sched;
});
