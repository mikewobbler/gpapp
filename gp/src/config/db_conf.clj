(ns config.db-conf
  (:require [mount.core :refer [defstate]]
            [clj-commons-exec :as exec]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [datomic.api :as d]
            [utils.fileUtils :as fu]
                        ;;[utils.fileUtils :refer :all]
            [utils.shell-utils :refer :all]
            [config.app-config :refer [gp-config]]
            [config.pgres-conf :refer [pgres-running]]
            ;;[config.db-new :refer [start-datomic-new-db]]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)])
  (:import [org.apache.commons.exec ExecuteWatchdog Watchdog]))


(defn start-transactor
  "Start the transactor - if it stops, restart it."
  [cmd dir props log]
  (let [ostream (io/output-stream log)]
    (timbre/info "Starting transactor.")
    (exec/sh [cmd props]
              {:dir dir :out ostream :err ostream
               :close-out? true :close-err? true
               :shutdown true})
    ))

(defn stop-transactor
  "Kill the transactor."
  []
  (timbre/info "Stopping transactor - calling shutdown...")
  (d/shutdown false)
  (kill-procs "datomic-transactor"))

(defn replace-db-name
  [db-name s]
  (let
      [match (re-matches #"(^sql-url.*)/\w*" s)]
    (if match (str (nth match 1) "/" db-name "\n") 
        (str s "\n"))))


(defn create-transactor-props
  "Create a transactor properties file for database db-name."
  [db-name if of]
  (fu/pmap-file (partial replace-db-name db-name) if of))

(defn start-datomic-with-db
  "Create a new datomic db and load it's schema. Returns the connection."
  [db-name]
  (timbre/info "In start-datomic-with-db...")
  (let
      [new-props-name (str (:transactor-props gp-config) "." db-name)
       dt-dir  (eval (:transactor-dir gp-config))
       dt-log  (:transactor-log gp-config)
       ostream (io/output-stream dt-log)]
    (create-transactor-props db-name
                             (str dt-dir "/"(:transactor-props gp-config))
                             (str dt-dir "/" new-props-name))
    (kill-procs "datomic-transactor")
    (exec/sh [(fu/get-path ["bin" "transactor"]) new-props-name]
             {:dir dt-dir :out ostream :err ostream
              :close-out? true :close-err? true
              :shutdown true}
             )
    ))

(defn start-datomic
  []
  (timbre/info "In start-datomic...")
  (if pgres-running
      (let
          [t-uri
           (clojure.string/replace
            (:transactor-uri gp-config) #"/datomic\?"
            (str "/" (:db-name gp-config) "?"))]
      (timbre/info "pgres is running...")
      (start-datomic-with-db (:db-name gp-config))
      t-uri)
    (timbre/error "Postgres server is not running..."))
    )
  

(defstate t-uri
  :start ((fn []
             (timbre/info "Starting datomic transactor...")
             (start-datomic)))
  :stop (stop-transactor)
  )

;; (defn try-connection
;;   "Return a database connection using the uri supplied in the db-config."
;;   ([]
;;    (let
;;        [uri (:transactor-uri gp-config)]
;;      (try
;;        (d/connect uri)
;;        (catch Exception e
;;          (do
;;            (timbre/error "Caught Exception: " (.toString e))
;;            (timbre/error "Exception map is: " (ex-data e))
;;            nil
;;            )))
;;      ))
;;   ([x] (try-connection)))

(comment
  (def transact-future (atom nil))
  (def transactor-monitor nil)

  (defn stop-transactor-monitored
    "Cancel the future restarting the transactor, and kill the transactor."
    []
    (if (not (= nil @transact-future))
      (do
        ;; cancel the current transact-future
        (timbre/info "In stop-transactor - cancelling future...")
        (future-cancel @transact-future)
        (kill-procs "datomic-transactor")
        (if (future-cancelled? @transact-future)
          (timbre/info "transact-future cancelled as expected...")
          (do
            (timbre/error "ERROR: transact-future NOT cancelled as expected...")
            ))
        (reset! transact-future nil))
      ))

  (defn stop-monitor
    []
    (when transactor-monitor
      (do
        (future-cancel transactor-monitor)
        (loop [cancelled (future-cancelled? transactor-monitor)]
          (when (not cancelled)
            (future-cancel transactor-monitor)
            (Thread/sleep 5000)
            (recur (future-cancelled? transactor-monitor))))
        (def transactor-monitor nil)
        (stop-transactor)))
    )

  (defn restart-transactor
    "Restart the transactor in a future that will restart it if it stops."
    []
    (
     (timbre/info "In restart transactor.")
     (stop-transactor)
     (kill-procs "datomic-transactor")
     (if (ensure-pgres)
       ;; Use a deref with a timeout here.
       (reset! transact-future
               (future (start-transactor
                        (fu/get-path ["bin" "transactor"])
                        (eval (:transactor-dir gp-config))
                        (:transactor-props gp-config) (:transactor-log gp-config))))
       false))
    )

  ;; NOTE: Need to use transactor pid, and possibly ps here.
  ;; This because of problem when two threads are attempting to run the monitor
  ;; at the same time.
  (defn monitor-transactor
    []
    (future
      (when (= nil @transact-future)
        (do (timbre/info "monitor-transactor: Transactor is nil....")
            (restart-transactor)))
      (loop [tf @transact-future]
        (timbre/info "In Monitor transactor....")
        (timbre/info "Thread is: " (.toString (Thread/currentThread)))
        (try
          (deref tf)
          (catch org.apache.commons.exec.ExecuteException e
            (timbre/error "Transactor Exited")
            (timbre/error (str "Message is: " (.getMessage e))))
          (catch InterruptedException e
            (timbre/error "Monitor Interrupted")
            (timbre/error (str "Message is: " (.getMessage e)))
            (throw e)
            )
          (catch Exception e
            (timbre/error "Unknown Error: Transactor terminated...")
            (timbre/error (str "Message is: " (.getMessage e))))
          )
        (when (not (Thread/interrupted))
          (restart-transactor)
          (timbre/info "Monitor transactor should have restarted....")
          (recur @transact-future))
        )
      ))

  (defn start-monitor
    []
    (when (= nil transactor-monitor)
      (def transactor-monitor (monitor-transactor)))
    )
  )
