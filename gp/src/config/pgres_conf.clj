(ns config.pgres-conf
  (:require [mount.core :refer [defstate]]
            [clj-commons-exec :as exec]
            [clojure.java.io :as io]
            [utils.shell-utils :refer :all]
            [config.app-config :refer [gp-config modify-config]]
            [config.pgres-new :as pgres-new]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)])
  (:import [org.apache.commons.exec ExecuteWatchdog Watchdog]))

;; Check if postgres is running.
(defn pgres-running?
  []
  (let
      [pg-dir (:pgres-dir gp-config)
       wd (ExecuteWatchdog. 5000)
       pg-rc @(exec/sh ["pg_ctl" "status" "-D" pg-dir] {:watchdog wd})
       exit-status (:exit pg-rc)]
    (cond (.killedProcess wd) (do (timbre/info "Timed out looking for postgres...") false)
          (= exit-status 0) (do (timbre/info "Postgres server is running...") true)
          (= exit-status 3) (do (timbre/info "Postgres server is NOT running...") false)
          (= exit-status 4) (do (timbre/info "Postgres  - incorrect directory supplied: " pg-dir) false)
          :else (do (timbre/info "Postgres  - unknown error...") false)))
  )

;; Start the postgres server
(defn start-pgres
  []
  (timbre/info "Starting postgres server...")
  (let
      [pg-dir (:pgres-dir gp-config)
       log (:pgres-log gp-config)
       ostream (io/output-stream log)]
    (exec/sh ["pg_ctl" "start" "-D" pg-dir]
                       {:out ostream :err ostream}
                       :close-out? true :close-err? true
                       :shutdown true)))

;; Stop the postgres server
(defn stop-pgres
  []
  (timbre/info "Stopping postgress server...")
  (let
      [pg-dir (:pgres-dir gp-config)]
    @(exec/sh ["pg_ctl" "stop" "-D" pg-dir])))

(defn ensure-pgres
  "If postgres isn't running, start it, and return true if it started ok, false
otherwise. Return true if postgres is already running."
  []
  (if (not (pgres-running?))
    (do
      (start-pgres)
      (Thread/sleep 10000)
      (let
          [retry (atom 0)
           pgr (atom (pgres-running?))]
        (while (and (not @pgr) (< @retry 10))
          (Thread/sleep 1000)
          (reset! pgr (pgres-running?))
          (swap! retry inc)
          )
        @pgr))
    true))

(defstate pgres-running
  :start (
          (fn []
            (if (ensure-pgres)
                   (if (:pgres-is-setup gp-config) true
                       (if (pgres-new/create-new-pgres-db (:db-name gp-config))
                         (modify-config :pgres-is-setup true)
                         (do
                           (timbre/error "Fatal: Failed to create postgres database.")
                           false))
                       )
                   ;; else
                   (do
                     (timbre/error "Fatal: Couldn't start postgres server.")
                     false))))
  :stop  (stop-pgres)
  )
