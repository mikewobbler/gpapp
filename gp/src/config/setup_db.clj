(ns config.setup-db
  (:require
   [datomic.api :as d :exclusions [joda-time]]
   [clojure.pprint :refer (pprint)]
   [mount.core :refer [defstate]]
   [config.app-config :refer [gp-config modify-config]]
   [config.db-conf :refer [t-uri]]
   [config.setup-seed :refer [setup-seed-data]]
   [taoensso.timbre :as timbre
    :refer (log trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)]
   )
  (:import [java.lang Exception])
  )

;; (defn addp
;;   [conn parts]
;;   (timbre/info (str "In addp, parts are: " parts))
;;   (Thread/sleep 5000)
;;   (for [p parts]
;;     (do
;;       (timbre/info (str "Inserting partition: " p))
;;       @(d/transact conn
;;                    [{:db/id #db/id[:db.part/db -1]
;;                      :db/ident p
;;                      :db.install/_partition :db.part/db}]))))

(defn addp
  [conn parts]
  (timbre/info (str "In addp, parts are: " parts))
  (Thread/sleep 5000)
  (doseq [p parts]
    (do
      (println (str "Inserting partition: " p))
      (let [tid (d/tempid :db.part/db)]
          @(d/transact conn
                       [{:db/id tid
                         :db/ident p}
                        [:db/add :db.part/db :db.install/partition tid]])))))

(defn load-schema
  "Merge all the schema in s-list into a single "
  [conn s-list]
  (timbre/info "In load-schema..." )
  (let [schema-list
        (loop [sl s-list ms []]
          (if (<= (count sl) 0) ms
            (recur (rest sl) (conj ms (load-file (first sl)))))
            )
        ]
    (doseq [s schema-list]
      (do
        (timbre/info (str "Adding schema: " s))
        @(d/transact conn s))
      )
    true
    )
  )

(defn get-datomic-connection
  "Create a new datomic db and load it's schema. Returns the connection."
  []
  (if (:datomic-is-setup gp-config)
    (let [retry (atom 0)
          conn (atom false)]
      (while (and (not @conn) (< @retry 10))
        (try
          (reset! conn (d/connect t-uri))
          (catch Exception e
            (timbre/info (str "(d/connect " t-uri "): Caught exception."))
            (timbre/info (str "Exception is: " (.getMessage e)))
            (Thread/sleep 5000)
            (swap! retry inc))
          )
        )
      @conn
       )
      (let
          [retry (atom 0)
           success (atom false)]
        (timbre/info "In get-datomic-connection - about to create db...")        
        (while (and (not @success) (< @retry 10))
          (try
            (d/create-database t-uri)
            (swap! success not)
            (catch Exception e
              (timbre/info (str "d/create-database: Caught Exception: " (.getMessage e)))
              (swap! retry inc)
              (Thread/sleep 5000)
              )))
        (when (not @success) (throw (Exception. (str "Couldn't create database."))))
        (Thread/sleep 10000)
        (let
            [conn (d/connect t-uri)]
          (d/sync conn)
          (addp conn [:appointments :patient :clinician])
          (load-schema conn (:schema gp-config))
          (modify-config :datomic-is-setup true)
          (setup-seed-data conn (:test-setup gp-config))
          conn
          ))))

(defstate conn
  :start (get-datomic-connection)
  :stop ((fn [] (when conn (d/release conn)))))

    ;;     [conn ]
    ;;   ;; Load up the schema
    ;; (timbre/info (str "Called add-parts... conn is: " conn))
    ;; (if conn
    ;;   (do
    ;;     (timbre/info (str "add-parts succeeded!"))
    ;;     conn)
    ;;   (do
    ;;     (timbre/info (str "add-parts failed!"))
    ;;     false)
    ;;   )
    ;;   )
    ;;   ;;(add-parts conn [:patient :clinician])
    ;;   ;;(add-parts conn)
    ;;   ;;(load-schema conn (:schema gp-config))
    ;; ))

;; Add a partitions
;; (defn add-partitions
;;   [conn p]
;;   (for [part p]
;;     @(d/transact conn
;;                  [{:db/id #db/id[:db.part/db -1]
;;                    :db/ident part}
;;                   [:db.add :db.part/db :db.install/partition #db/id[:db.part/db -1]]]
;; )))

;; (defn add-parts
;;   [turi parts]
;;   (let 
;;       [conn (atom (d/connect turi))]
;;     (for [p parts]
;;       (let
;;           [retry (atom 0)
;;            success (atom false)
;;            ]
;;         (while (and  (not @success) (< @retry 10))
;;           (try
;;             @(d/transact @conn
;;                          [{:db/id #db/id[:db.part/db]
;;                            :db/ident p
;;                            :db.install/_partition :db.part/db}])
;;             (swap! success not)
;;             (catch Exception e
;;               (timbre/info (str "add-parts: Caught Exception: " (.getMessage e)))
;;               (swap! retry inc)
;;               (Thread/sleep 5000)
;;               (reset! conn (d/connect turi))
;;               )))
;;         (when (not @success) (throw (Exception. (str "Couldn't add partition " p " to db."))))))
;;     @conn))
