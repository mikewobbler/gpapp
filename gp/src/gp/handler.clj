(ns gp.handler
  (:require [compojure.core :refer [defroutes routes]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [ring.middleware.reload :refer [wrap-reload]]
            [selmer.middleware :refer [wrap-error-page]]
            [environ.core :refer [env]]
            [hiccup.middleware :refer [wrap-base-url]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [gp.routes.home :refer [home-routes]]
            [gp.routes.auth :refer [auth-routes]]
            [gp.routes.patients :refer [patient-routes]]
            [gp.routes.users :refer [add-user-routes]]
            [gp.routes.manage-appts :refer [appt-routes]]
            [gp.models.db :refer [shutdown-datomic]]
            [noir.session :as session]
            [noir.validation
             :refer [wrap-noir-validation]]
            [ring.middleware.session.memory
             :refer [memory-store]]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]
            [taoensso.timbre.appenders.core :as appenders]
            [mount.core :as mount]
            [config.setup-db :refer [conn]]))

(defn init []
  (timbre/merge-config!
   {:appenders {:spit (appenders/spit-appender {:fname "/tmp/gpApp.log"})}})
  (timbre/info "gp is starting")
  (mount/start)
)

(defn destroy []
  (timbre/info "gp is shutting down")
  ;;(shutdown-datomic)
  (mount/stop)
  )

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found")
  )

;; db-resource-routes
(def app
  (-> (handler/site
       (routes home-routes auth-routes patient-routes add-user-routes appt-routes app-routes))
      (session/wrap-noir-session {:store (memory-store)})
      wrap-reload
      (wrap-base-url)
      (wrap-noir-validation)))

;;#(if (env :dev) (wrap-error-page %) %)

