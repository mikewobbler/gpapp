(ns gp.models.appointments
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]            
            [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :refer (pprint)]
            [clojure.instant :as inst]
            [joda-time :as t]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy get-env log-env)]            
            [clojure.string :as cstr]
            [utils.err-wrap :as err-handler]            
            [utils.fileUtils :refer :all]
            [config.setup-db :refer [conn]]
            ))

;; Test date using:
;; (.format (java.text.DateFormat/getDateTimeInstance) (java.util.Date.))
;;
(defn make-appointment
  "Add an appointment for patient with pid pid-string with the supplied date
  expected time and optional note."
  [pid-string date expected-time & [note]]
  (let
      [lref [:patient/pid (as-uuid pid-string)]
       app-inst (java.util.Date. date)
       appt-id (d/tempid :db.part/user)
       tx1 (d/transact conn
                       (into [[:db/add appt-id :appointment/date app-inst]
                              [:db/add appt-id :appointment/expectedDuration expected-time]
                              [:db/add appt-id :appointment/status :appointment/pending]]
                             (if note [[:db/add appt-id :appointment/note note]] nil)
                              )
                  )]
    (d/transact conn
                [[:db/add lref :patient/appointments
                  (d/resolve-tempid (d/db conn) (:tempids @tx1) appt-id)]]
                )))


(defn get-patients-appointments
  "Return all appointments for the patient with id in pid-string."
  [pid-string]
  (let
      [pid (as-uuid pid-string)]
    (d/q '[:find [?app-id ...]
           :in $ ?pid
           :where
           [?eid :patient/pid ?pid]
           [?eid :patient/appointments ?app-id]]
         (d/db conn)
         pid)
    )
  )

(defn get-patients-pending-appointments
  "Return all pending appointments for the patient with id in pid-string."
  [pid-string]
  (let
      [pid (as-uuid pid-string)]
    (d/q '[:find [?app-id ...]
           :in $ ?pid
           :where
           [?eid :patient/pid ?pid]
           [?eid :patient/appointments ?app-id]
           [?app-id :appointment/status :appointment/pending]]
         (d/db conn)
         pid)
    )
  )

(defn get-appointment-by-id
  [appt-id]
  (d/q '[:find ?app-date ?exp-duration
           :in $ ?appt-id
           :where
           [?appt-id :appointment/date ?app-date]
           [?eid :appointment/expectedDuration ?exp-duration]]
         (d/db conn)
         appt-id)
  )

;; Get all patient appointments for a given day.








