(ns gp.models.db
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]
            [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :refer (pprint)]
            [clojure.instant :as inst]
            [joda-time :as t]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy get-env log-env)]            
            [clojure.string :as cstr]
            [utils.err-wrap :as err-handler]            
            [utils.fileUtils :refer :all]
            [config.app-config :refer [gp-config]]
            [mount.core :as mount])
  (:import [java.text DateFormat SimpleDateFormat]))

;;[config.db-conf :refer [conn]]

;; (defn get-conf
;;   "Read in the databse configuration from the file specified by path."
;;   [path]
;;   (read-string (slurp path)))

;; Setup the database connection.
;; (defn set-connection
;;   "Return a database connection."
;;   []
;;   (let
;;       [c (try-connection)]
;;     (if c (def conn c)
;;         (do
;;           (when (= nil transactor-monitor)
;;             ;; The transactor hasn't been started, try and start it.
;;             (start-monitor))
;;           (let [conn (atom (try-connection))
;;                 retries (atom 0)]
;;             (while (and (nil? @conn) (< @retries 10))
;;               (do
;;                 (Thread/sleep 5000)
;;                 (swap! retries inc)
;;                 (swap! conn try-connection)))
;;             (def conn @conn))
;;           )
;;         ))
;; )

;; (defn get-connection
;;   []
;;   conn)

(defn shutdown-datomic
  []
  (mount/stop)
  )

(defn sortable
  "Rearrange a name so that sorting can occur starting with the surname."
  [name]
  (let
      [name-list (cstr/split name #" ")
       surname (last name-list)]
    (cstr/join " " (conj (drop-last name-list) surname))
    )
  )

;; Patient Map should look like this:
;;
;; {
;;     :name  "Patient's name",
;;     :gender  - Enumerated value
;;     :dob - Date of birth
;;     :phoneNum  - Phone Number
;;     :address - Adress.
;;     :medicareNumber - The medicare number of the patient.
;; }
;;
;; NOTE: Some of the fields can be empty, including phone, address and
;; medicareNumber
;;
;; Database attributes we need to generate include:
;; :patient/startDate - Today's date.
;; :patient/pid - A squuid
;; :consultNotes - An empty or arbitrary string.
;; :testResults - An empty string - or just "Test Results"
;; :correspondence - An empty string or just "Correspondence"
;; :billing - An empty string or just "Billing"
;; :appointments - empty - don't do anything.
;;
(defn add-patient
  "Add a patient to the patient database."
  [conn patient-map]
  ;; {:pre [(and (or (map? patient-map) (record? patient-map))
  ;;             (contains? patient-map :last-name))]
  ;;  :post [true]}
  (let [
        first-name (patient-map :first-name "")
        last-name  (patient-map :last-name)
        full-name  (patient-map :full-name (str first-name last-name))
        gender (cond (= (patient-map :gender) "Male") :person.gender/Male
                     (= (patient-map :gender) "Female") :person.gender/Female
                     :else :person.gender/Other)
        sdf (SimpleDateFormat. "yyyy/MM/dd")
        dob (.parse sdf (patient-map :dob))
        start-date (java.util.Date.)
        pid (patient-map :pid (d/squuid))
        ]
    @(d/transact conn [{:db/id (d/tempid :db.part/user)
                        :patient/firstName          first-name
                        :patient/lastName           last-name
                        :patient/fullName           full-name
                        :patient/sortName           (sortable full-name)
                        :patient/title              (patient-map :title "")
                        :patient/gender             gender
                        :patient/phoneNum           (patient-map :phone "")
                        :patient/streetAddress      (patient-map :street-address "")
                        :patient/city               (patient-map :city "")
                        :patient/state              (patient-map :state "")
                        :patient/country            (patient-map :country "")
                        :patient/postCode           (patient-map :post-code "")
                        :patient/email              (patient-map :email "")
                        :patient/medicareNumber     (patient-map :medicare-number "")
                        :patient/dob                dob
                        :patient/startDate          start-date ;;(patient-map :start-date)
                        :patient/consultNotes       (patient-map :consult-notes "")
                        :patient/testResults        (patient-map :test-results "")
                        :patient/diagnosis          (patient-map :diagnosis "")
                        :patient/correspondence     (patient-map :correspondence "")
                        :patient/billing            (patient-map :billing "")
                        :patient/pid                pid
                        }])))

;; Patient queries...
(defn get-patient-eid-pid
  "Return the entity id for the patient with personal identification uuid pid."
  [conn pid]
  (d/q '[:find ?eid
         :in $ ?pid
         :where [?eid :patient/pid ?pid]]
        (d/db conn)
        pid))

(defn get-patient-details-pid
  "Get all the details for a patient."
  [conn pid]
  (->> (get-patient-eid-pid conn pid)
       ffirst
       (d/entity (d/db conn))))


(defn get-patient-entities
  "Return a list of all patient entities - assuming all patients have a name."
  [conn]
  (d/q '[:find [?eid ...]
              :where [?eid :patient/fullName _]]
            (d/db conn)
            ))

(defn get-patient-list
  "Return a list containing the required fields for each patient entity in the
 database"
  [conn required-fields]
    (->> (get-patient-entities conn)
         (d/pull-many (d/db conn) required-fields)))

(defn get-patient-fields
  [conn pat-id required-fields]
  (let [lref [:patient/pid (as-uuid pat-id)]]
       (d/pull (d/db conn) required-fields lref)))


(defn get-patient-consult-notes
  "Return the patient consultation notes."
  [conn pid-string]
  (let
      [lref [:patient/pid (as-uuid pid-string)]]
    (:patient/consultNotes (d/pull (d/db conn) '[:patient/consultNotes] lref))
    ))

(defn set-patient-consult-notes
  "Set the patient's consult notes."
  [conn pid-string consult-notes]
  (let
      [lref [:patient/pid (as-uuid pid-string)]]
    (d/transact conn [[:db/add lref :patient/consultNotes consult-notes]]))
    )

(defn make-retract-vector
  [v eid]
  (timbre/info (str "In make-retract-vector"))
  (let [rv
        (reduce
         (fn [ memo val]
           (let [key (first val)]
             (if
                 (instance? clojure.lang.PersistentHashSet (second val))
               (do
                 (timbre/info "hash set: " (second val))
                 (into memo (mapv #(into [:db/retract eid key] [%1]) (second val))))
               (into memo [(into [:db/retract eid key] [(second val)])])))
           )
         []
         v)]
    (timbre/info (str "retract vector = " rv))
    rv))

;;  (instance? java.util.Date (second val))
;; (into memo [(into [:db/retract eid key] [(.toString (second val))])])

(defn del-patient
  "Remove a user by uid."
  [conn pid-string]
  (let [pid (as-uuid pid-string)
        eid (ffirst (get-patient-eid-pid conn pid))
        entity (d/touch (get-patient-details-pid conn pid))
        ]
    (timbre/info (str "Mr. Flibble is: " (make-retract-vector entity eid)))
    (Thread/sleep 1000)
    @(d/transact conn (make-retract-vector entity eid))
    )
  )

;; Users - ie clinicians, administrators.
(defn get-user-eid-uid
  [conn uid]
  (d/q '[:find ?eid
         :in $ ?uid
         :where [?eid :user/uid ?uid]]
        (d/db conn)
        uid))

(defn get-user-details-uid
  "Get all the details for a user."
  [conn uid-string]
  (timbre/info (str "Getting uid = " uid-string))
  (->> uid-string
       as-uuid
       (get-user-eid-uid conn)
       ffirst
       (d/entity (d/db conn))))

(defn get-user-entities
  "Return a list of all patient entities - assuming all patients have a name."
  [conn]
  (d/q '[:find [?eid ...]
              :where [?eid :user/loginName _]]
            (d/db conn)
            ))

(defn get-user-list
  "Return a list of all users containing the required fields for each user."
  [conn required-fields]
  (->> (get-user-entities conn) (d/pull-many (d/db conn) required-fields)))

(defn get-user-eid
  "Return the eid for the given login name.
 NOTE: login-name must be unique in the database."
  [conn login-name]
  (d/q '[:find ?eid
         :in $ ?lname
         :where [?eid :user/loginName ?lname]]
        (d/db conn)
        login-name))

(defn excise-user
  "Completely remove a user from the database.
NOTE: Probably not for common use in a production system."
  [conn eid]
  (d/transact conn
                [{:db/id #db/id[db.part/user],
                  :db/excise eid}])
  )

(defn get-password-for-validation
  "Return the password for the given login name."
  [conn lname]
   (d/q '[:find ?pword .
          :in $ ?lname
          :where
          [?eid :user/loginName ?lname]
          [?eid :user/password ?pword]
          ]
        (d/db conn)
        lname
        ))

(defn is-valid
  "Check that user id and password are valid login credentials."
  [conn uid pass]
  (= pass (get-password-for-validation conn uid)))

(defn add-user
  "Add a user to the patient database, with the minimum set of attributes
to allow the user to use the application."
  [conn user-name login-name pword roles]
  (let
      [tid (d/tempid :db.part/user)
       uid (d/squuid)
       role-part (for [r roles
                       :let [rpart []]]
                   (do
                     (timbre/info "r is " r)
                     (into rpart [:db/add tid :user/role r])))]
    @(d/transact conn
                 (into
                  [[:db/add tid :user/fullName user-name]
                   [:db/add tid :user/loginName login-name]
                   [:db/add tid :user/password pword]
                   [:db/add tid :user/uid uid]
                   [:db/add tid :user/sortName (sortable user-name)]]
                  role-part))))


(defn del-user
  "Remove a user by login name."
  [conn login]
  (let
      [eid (ffirst (get-user-eid conn login))
       entity (d/entity (d/db conn) eid)
       retract-vector (make-retract-vector entity eid)]
    @(d/transact conn retract-vector)
      )
  )

(defn del-user-uid
  "Remove a user by uid."
  [conn uid-string]
  (let [eid (ffirst (get-user-eid-uid conn (as-uuid uid-string)))
        entity (d/entity (d/db conn) eid)
        retract-vector (make-retract-vector entity eid)]
    (timbre/info (str "retract vector is: " retract-vector))
    @(d/transact conn retract-vector)
    )
  )

(comment
  ;; Basic loading of the database
  (defn load-schema
    "Load the database schema from gp-config into the database."
    [conn]
    (let
        [uri (:transactor-uri gp-config)
         conn (d/connect uri)
         patient-schema (load-file (:schema-file gp-config))
         user-schema (load-file (:user-schema-file gp-config))]
      (d/transact conn patient-schema)
      (d/transact conn user-schema)))


  ;; Only for creation.
  (defn create-new-psql-db
    "Create a new database in postgress with the schema and uri from gp-config."
    [conn]
    (let
        [uri (:transactor-uri gp-config)]
      (d/create-database uri)
      (let [conn (d/connect uri)
            schema (load-file (:schema-file gp-config))]
        (d/transact conn schema)
        )))

  ;; For development and testing.
  (defn create-empty-in-memory-db []
    (let [uri "datomic:mem://patient-db"]
      (d/delete-database uri)
      (d/create-database uri)
      (let [conn (d/connect uri)
            schema (load-file "resources/schema.edn")]
        (d/transact conn schema)
        conn
        ))))
