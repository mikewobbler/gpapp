(ns gp.models.into-datomic
  (:require
   [datomic.api :as d :exclusions [joda-time]]
   [clojure.pprint :refer (pprint)]
   [compojure.coercions :refer :all]
   [clj-time.format :as f]
   [clj-time.coerce :as c]
   [clojure.string :as cstr]
   [taoensso.timbre :as timbre
    :refer (log trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy get-env log-env)]
   )
  (:import [java.lang Exception])
  (:gen-class)
  )

;;(vec-form [this])
(defprotocol INTO_DATOMIC
  (map-form [this])
  )

;; Patient record for loading seed data, and creating a datomic compatible
;; representation for insertion into the datomic database.
(defrecord Patient
    [patient-firstName patient-lastName patient-title patient-gender
     patient-phoneNum patient-streetAddress patient-city
     patient-state patient-country patient-postCode patient-email
     patient-medicareNumber patient-dob patient-startDate
     patient-consultNotes patient-testResults patient-diagnosis
     patient-correspondence  
     patient-treatmentDesc patient-billing]
  INTO_DATOMIC
  (map-form [pat]
    (let
        [pat-fields
         (reduce
          #(assoc %1
                  (keyword (cstr/replace (name (key %2)) "-" "/"))
                  (val %2)) {} pat)
         full-name (str (:patient-firstName pat) " " (:patient-lastName pat))
         sort-name (str (:patient-lastName pat) " " (:patient-firstName pat))
         start-date (java.util.Date.)
         d-formatter (f/formatter "MM/dd/yyyy")
         dob (c/to-date (f/parse d-formatter (:patient-dob pat)))
         pid (d/squuid)
         gender (keyword (str "person.gender/" (:patient-gender pat)))
         ]
      (assoc  (dissoc pat-fields :patient/dob :patient/gender)
              :patient/fullName full-name
              :patient/sortName sort-name
              :patient/startDate start-date
              :patient/dob dob
              :patient/pid pid
              :patient/gender gender
              )))
  )

(defrecord User
    [user-loginName user-fullName user-title user-phoneNum user-streetAddress
     user-city user-state user-country user-postCode user-email
     user-medicareProviderNumber user-startDate
     user-roles]
  INTO_DATOMIC
  (map-form [user]
    (let
        [user-fields
         (reduce
          #(assoc %1
                  (keyword (cstr/replace (name (key %2)) "-" "/"))
                  (val %2)) {} user)
         user-roles
         (mapv #(keyword (str "user.role/" %1)) (cstr/split (cstr/trim (:user/roles user-fields)) #":"))
         start-date (c/to-date (f/parse (f/formatters :date) (cstr/trim (:user/startDate user-fields))))
         uid (d/squuid)
         ]
      (assoc
       (dissoc user-fields :user/roles :user/startDate)
       :user/role user-roles
       :user/startDate start-date
       :user/uid uid
       :user/password (:user/loginName user-fields))
      )))

(defrecord SchedInterval
    [schedule-intervalStart schedule-intervalEnd]
  INTO_DATOMIC
  (map-form [interval]
    (reduce
     #(assoc %1
             (keyword (cstr/replace (name (key %2)) "-" "/"))
             (c/to-date (f/parse (f/formatters :hour-minute) (val %2)))) {} interval)
      )
)

(defn sched-interval
  "SchedInterval constructor. Takes a map of the form:
{:schedule-startBreak \"11:00\" :schedule-endBreak \"11:15\"}
as input."
  [{:keys [schedule-startBreak schedule-endBreak]}]
  (->SchedInterval schedule-startBreak schedule-endBreak)
  )

;; "Schedule record."
(defrecord Schedule
    [schedule-firstDate schedule-lastDate schedule-startTime schedule-endTime
     schedule-breaks schedule-appointmentDuration schedule-id
     schedule-name]
  INTO_DATOMIC
  (map-form [sched]
    (let
        [brks (mapv #(map-form %1) (:schedule-breaks sched))
         sched-map
         (reduce #(apply assoc %1
                         (cond (= (key %2) :schedule-firstDate)
                               [(keyword (cstr/replace (name (key %2)) "-" "/"))
                                (c/to-date (f/parse (f/formatters :date) (val %2)))]
                               (= (key %2) :schedule-lastDate)
                               [(keyword (cstr/replace (name (key %2)) "-" "/"))
                                (c/to-date (f/parse (f/formatters :date-time) (val %2)))]
                               (or (= (key %2) :schedule-startTime)
                                   (= (key %2) :schedule-endTime))
                               [(keyword (cstr/replace (name (key %2)) "-" "/"))
                                (c/to-date (f/parse (f/formatters :hour-minute) (val %2)))]
                               :else
                               [(keyword (cstr/replace (name (key %2)) "-" "/"))
                                (val %2)])
                         ) {} (dissoc sched :schedule-breaks))
         ]
      (assoc sched-map :schedule-breaks brks))
    ))

  
(defn schedule
  "Constructor for Schedule records."
  [sched-map]
  (let [bks (mapv #(sched-interval %1) (:schedule-breaks sched-map))
        first-part (dissoc sched-map :schedule-breaks)]
    (map->Schedule (assoc first-part :schedule-breaks bks
                          :schedule-id (d/squuid)))))


;; For testing.
(comment
  (def sched1
    {
     :schedule-firstDate "2016-02-01"               ;; use formatter :date
     :schedule-lastDate  "2016-07-30T23:59:59.999Z" ;; user formatter :date-time
     :schedule-startTime "08:30"                    ;; user formatter :hour-minute
     :schedule-endTime   "17:30"                    ;; user formatter :hour-minute
     :schedule-breaks     [
                           {:schedule-startBreak "11:00" :schedule-endBreak "11:15"}
                           {:schedule-startBreak "12:30" :schedule-endBreak "13:15"}
                           {:schedule-startBreak "14:00" :schedule-endBreak "14:15"}
                           ]
     :schedule-appointmentDuration 15
     :schedule-name "Schedule 1"
     }
    )
  (def sched2
    {
     :schedule-firstDate "2016-08-01"               ;; use formatter :date
     :schedule-lastDate  "2016-09-02T23:59:59.999Z" ;; user formatter :date-time
     :schedule-startTime "13:15"                    ;; user formatter :hour-minute
     :schedule-endTime   "19:30"                    ;; user formatter :hour-minute
     :schedule-breaks     [
                           {:schedule-startBreak "17:00" :schedule-endBreak "17:30"}
                           ]
     :schedule-appointmentDuration 15
     :schedule-name "Schedule 2"
     }
    )
  )

