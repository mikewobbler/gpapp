(ns gp.routes.auth
  (:require [compojure.core :refer [defroutes GET POST]]
            [gp.views.layout :as layout]
            [config.setup-db :refer [conn]]
            [gp.models.db :as db]
            [hiccup.form :refer
              [form-to label text-field password-field submit-button]]
            [hiccup.core :refer [h]]
            [noir.response :refer [redirect]]
            [noir.session :as session]
            [noir.validation
             :refer [rule errors? has-value? on-error]]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy get-env log-env)]))

(defn input-form-el [field name text]
  [:div.form-group
   [:label {:for name :class "col-lg-2 control-label"} text]
   [:div.col-lg-10
    [:input.form-control {:type field :name name :placeholder text}]]])

(defn button-form-el [label-reset label-submit]
  [:div.form-group
   [:div {:class "col-lg-10 col-lg-offset-2"}
    [:input {:class "btn btn-default login-button" :type "reset" :value label-reset}]
    [:input {:class "btn btn-primary login-button" :type "submit" :value label-submit}]]])

(defn login-page [& [error]]
  (layout/l-form
   "Login Page:"
   [:div {:class "container login-container"}
    (if error [:div.error "Login error: " error])
    [:div.jumbotron
     [:div.row
      [:div.col-lg-6]
      [:form {:method "POST", :action (h "/login") :class "form-horizontal"}
       [:legend "Login:"]
       [:fieldset
        (input-form-el "text" :id "user name")
        (input-form-el "password" :pass "Password")
        (button-form-el "Reset" "Submit")]]]]]))

(defn handle-login [id pass]
  (timbre/info (str "Id is: " id " password is: " pass))
  (cond (not (has-value? id))
        (do
          (timbre/error "No login name supplied.")
          (login-page "login name is required")
          )
        (not (has-value? pass))
        (do
          (timbre/error "No password supplied.")
          (login-page "password is required")
          )
        (not (db/is-valid conn id pass))
        (do
          (timbre/error "User credentials don't match.")
          (login-page "incorrect username or password")
          )
        :else
        (do
          (session/put! :user id)
          (redirect "/"))))

(defroutes auth-routes
  (GET "/login" [] (login-page))
  (POST "/login" [id pass]
          (handle-login id pass)))



