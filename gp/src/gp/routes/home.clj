(ns gp.routes.home
  (:require [compojure.core :refer :all]
            [gp.views.layout :as layout]
            [gp.routes.misc-utils :as mu]
            [liberator.core
             :refer [defresource resource request-method-in]]
            [noir.response :refer [redirect]]
            [noir.session :as session]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]            
            ))


;; FIXME: Change so that we pass what method we've matched. So for example,
;; for the ANY method on "/" pass:
;; (ANY "/" [:as r] (handle-route :ANY r))
;;
;; So this is a call to the handle-route function, specifying the method
;; as a keyword, and the request map as r.
;; (defroutes home-routes
;;   (ANY "/" [] (handle-route :/ :ANY)))
(defn home []
  (let
      [template-file "base.html"
       user (mu/ssget :user)]
    (if user
        (layout/common template-file
                       {:title "Gp App Home"
                        :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                        :js-file "landingPage.js"
                        :heading (str "Welcome " user "!")})
        (redirect "/login"))))

(defroutes home-routes
  (GET "/" [] (home))
  )
