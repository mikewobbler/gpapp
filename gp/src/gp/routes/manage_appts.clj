(ns gp.routes.manage-appts
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]
            [noir.response :refer [redirect]]
            [noir.session :as session]
            [clojure.pprint :as pp]
            [liberator.core
             :refer [defresource resource request-method-in]]
            [cheshire.core :refer :all]
            [taoensso.timbre :as timbre
              :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]
            [gp.views.layout :as layout]
            [gp.routes.misc-utils :as mu]
            [gp.models.schedules :as gp-sched]
            [config.setup-db :refer [conn]]            
            )
  (:import (java.io StringWriter)))

(defn appointments-page
  "Display the appointments for the current user."
  []
  (let
      [template-file "manage-appts.html"
       user (mu/ssget :user)]
    (timbre/info "In manage-appts route.")
    (if user
      (layout/common template-file
                     {:title "Appointments"
                      :css ["/css/bootstrap.min.css"
                            "/css/gpApp.css"]
                      :js-file "manage-appts.js"
                      :heading "Manage Appointments: "
                      :user user
                       })
      (redirect "/login"))))

(defn get-all-schedules []
  (generate-string {
                    :data-list (gp-sched/get-schedules)
                    })
  )

(defresource get-scheds
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (get-all-schedules))
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"])


(defroutes appt-routes
  (GET "/appts-calendar" [] (appointments-page))
  (GET "/get-schedules" request get-scheds))

