(ns gp.routes.misc-utils
  (:require [compojure.core :refer :all]
            [noir.session :as session]
            [clojure.string :as str]
            [utils.err-wrap :refer :all]))

(def safe-session-get (wrap-in-ehandler session/get [ClassCastException]))
(defn ssget [kw]
  (or (safe-session-get kw) nil))

(defn subst-keys
  "Return a new version of p-map with keys in (keys xlations) changed to the
corresponding value in (vals xlations)."
  [p-map xlations]
  (let [x-seq (seq xlations)]
    (loop [pm (transient p-map) xlat (first xlations) r (rest xlations)]
      (if (not xlat) (persistent! pm)
          (do
            (let [v (get pm (key xlat))]
              (when v
                  (assoc! (dissoc! pm (key xlat)) (val xlat) v)))
          (recur pm (first r) (rest r)))))
      )
    )

(defn db-name-matches
  "Match a camel case string."
 [db-name]
 (filter #(not (nil? %1))
               (rest (re-find #"(\p{Lower}+)(\p{Upper}\p{Lower}*)?" (name db-name))))
 )

(defn db-name-to-id
  "Convert camel case strings to keywords in snake case."
  [db-name]
  (let
      [matches (db-name-matches db-name)]
    (clojure.string/join (interpose "-" (map clojure.string/lower-case matches)))))

(defn db-name-to-heading
  [db-name]
  (let
      [matches (db-name-matches db-name)]
    (clojure.string/join (interpose " " (map clojure.string/capitalize matches)))))

(def patient-id-map
  {
   :patient/firstName         {:id (db-name-to-id :patient/firstName)
                               :heading (db-name-to-heading :patient/firstName)},
   :patient/city              {:id (db-name-to-id :patient/city)
                               :heading (db-name-to-heading :patient/city)},
   :patient/diagnosis         {:id (db-name-to-id :patient/diagnosis)
                               :heading (db-name-to-heading :patient/diagnosis)},
   :patient/postCode          {:id (db-name-to-id :patient/postCode)
                               :heading (db-name-to-heading :patient/postCode)},
   :patient/phoneNum          {:id (db-name-to-id :patient/phoneNum)
                               :heading (db-name-to-heading :patient/phoneNum)},
   :patient/fullName          {:id (db-name-to-id :patient/fullName)
                               :heading (db-name-to-heading :patient/fullName)},
   :patient/consultNotes      {:id (db-name-to-id :patient/consultNotes)
                               :heading (db-name-to-heading :patient/consultNotes)},
   :patient/pid               {:id (db-name-to-id :patient/pid)
                               :heading (db-name-to-heading :patient/pid)},
   :patient/title             {:id (db-name-to-id :patient/title)
                               :heading (db-name-to-heading :patient/title)},
   :patient/streetAddress     {:id (db-name-to-id :patient/streetAddress)
                               :heading (db-name-to-heading :patient/streetAddress)},
   :patient/email             {:id (db-name-to-id :patient/email)
                               :heading (db-name-to-heading :patient/email)},
   :patient/dob               {:id (db-name-to-id :patient/dob)
                               :heading (db-name-to-heading :patient/dob)},
   :patient/billing           {:id (db-name-to-id :patient/billing)
                               :heading (db-name-to-heading :patient/billing)},
   :patient/lastName          {:id (db-name-to-id :patient/lastName)
                               :heading (db-name-to-heading :patient/lastName)},
   :patient/state             {:id (db-name-to-id :patient/state)
                               :heading (db-name-to-heading :patient/state)},
   :patient/gender            {:id (db-name-to-id :patient/gender)
                               :heading (db-name-to-heading :patient/gender)},
   :patient/testResults       {:id (db-name-to-id :patient/testResults)
                               :heading (db-name-to-heading :patient/testResults)},
   :patient/correspondence    {:id (db-name-to-id :patient/correspondence)
                               :heading (db-name-to-heading :patient/correspondence)},
   :patient/startDate         {:id (db-name-to-id :patient/startDate)
                               :heading (db-name-to-heading :patient/startDate)},
   :patient/country           {:id (db-name-to-id :patient/country)
                               :heading (db-name-to-heading :patient/country)},
   :patient/medicareNumber    {:id (db-name-to-id :patient/medicareNumber)
                               :heading (db-name-to-heading :patient/medicareNumber)},
   :patient/sortName          {:id (db-name-to-id :patient/sortName)
                               :heading (db-name-to-heading :patient/sortName)}
   })

