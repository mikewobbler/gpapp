(ns gp.routes.patients
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]
            [noir.response :refer [redirect]]
            [noir.session :as session]
            [clojure.pprint :as pp]
            [liberator.core
             :refer [defresource resource request-method-in]]
            [cheshire.core :refer :all]
            [taoensso.timbre :as timbre
              :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]
            [gp.models.db :as db]
            [gp.views.layout :as layout]
            [gp.routes.misc-utils :as mu]
            [config.setup-db :refer [conn]]
            )
  (:import (java.io StringWriter)))

(defn add-patient-page
  "Add a new patient page."
  []
  (let
      [template-file "add-patient.html"
       user (mu/ssget :user)]
    (timbre/info "In new add-patient route.")
    (if user
      (layout/common template-file
                     {:title "Add Patient"
                      :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                      :js-file "add-patient.js"
                      })
      (redirect "/login"))))

(defn patient-list-page
  "Display the list of patients."
  []
  (let
      [template-file "GPAppEntityList.html"
       user (mu/ssget :user)]
    (timbre/info "In patient-list route.")
    (if user
      (layout/common template-file
                     {:title "Patient List"
                      :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                      :js-file "patientList.js"
                      :heading "Patient List: "
                      :find-entity "Find Patient"
                      :entity-heading "Patient List:"
                      :plist "plist"})
      (redirect "/login"))))

(defn get-patient-list
  "Return a list of patients from the database, for display by the client."
  []
  (let
      [plist
       (db/get-patient-list conn
        [:patient/fullName :patient/phoneNum :patient/sortName :patient/pid :patient/medicareNumber])]
    (generate-string {
                      :table-id "plist"
                      :sort-key :patient/sortName
                      :link :patient/pid
                      :columns {:patient/fullName "Name" :patient/phoneNum "Phone Number"}
                      :data-list plist})))

(defn patient-record
  "Return the record of the patient with patient id = pid."
  [pid]
  (let
      [prec (db/get-patient-details-pid conn (as-uuid pid))
       template-file "patient-record.html"
       user (mu/ssget :user)]
    ;;(timbre/info (str (generate-string (reduce #(assoc %1 (key %2) (val %2)) {} prec))))
    (if user
      (layout/common template-file
                     {:title "Patient Record"
                      :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                      :js-file "patientRecord.js"
                      :heading "Patient Record:"
                      :pat-data  (generate-string 
                                       (reduce #(assoc %1 (key %2) (val %2)) {} prec))
                      })
      (redirect "/login")
      )
    )
  )

(defresource get-patients
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (get-patient-list))
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"])

(defn handle-save-consult
  [id content]
  (timbre/info (str "Handle Consult: "
                    "Patient Id = " id
                    " Content = " content))
  (db/set-patient-consult-notes conn id content)
  )

(defn get-pat-summary
  [pat-id]
  (let
      [psum (db/get-patient-fields
             conn
             pat-id
             [:patient/fullName :patient/title :patient/dob :patient/phoneNum])]
    (generate-string
     psum
     ))
  )

(defresource pat-summary
  [pat-id]
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (timbre/info (str "About to call get-pat-summary with: " pat-id))
               (get-pat-summary pat-id))
  :etag "fixed-etag"
  :available-media-types ["application/json"])

(defresource remove-patient
  [pat-id]
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :post
  :allowed-methods [:post]
  :malformed? (fn [context]
                false)
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :post! (fn [_]
           (timbre/info (str "remove-patient: post! - pid = " pat-id))
           (db/del-patient conn pat-id))
  :handle-created (fn [_] (get-patient-list))
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"]
  )

(defn add-new-patient
  [params]
  (let
      [xlate {
              "patientName"     :full-name,
              "patientGender"   :gender,
              "patientDob"      :dob,
              "medicareNum"     :medicare-number,
              "patientTitle"    :title,
              "patientPhone"    :phone,
              "streetAddress"   :street-address,
              "pCity"           :city,
              "stateAddress"    :state,
              "pPostCode"       :post-code,
              "pEmail"          :email
              }
       new-params (mu/subst-keys params xlate)
       name-list (clojure.string/split (get new-params :full-name) #"\s+")
       add-fields {:first-name (first name-list) :last-name (last name-list)}
       final-params (conj new-params add-fields)]
    (timbre/info (str "New params are: " new-params))
    (db/add-patient conn final-params)
  ))

(defresource new-patient
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :post
  :allowed-methods [:post]
  :malformed? (fn [context]
                (let [params (get-in context [:request :form-params])]
                  (empty? (get params "patientName"))
                  ))
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :post! (fn [context]
           (let [params (get-in context [:request :form-params])]
             (timbre/info (str "new-patient: post! - params = " params ))
             (add-new-patient params))

           )
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"])

(defroutes patient-routes
  (GET "/patient-list" [] (patient-list-page))
  (GET "/patient-record/:patient-id" [patient-id] (patient-record patient-id))  
  (GET "/get-patients" request get-patients) ;; ajax call
  (GET "/patient-summary/:pat-id" [pat-id] (pat-summary pat-id))
  (GET "/add-patient" [] (add-patient-page))
  (POST "/remove-patient" [pat] (remove-patient pat))
  (POST "/add-new-patient" request new-patient)
  (POST "/patient/consult-note" [id content]
        (handle-save-consult id content)))

