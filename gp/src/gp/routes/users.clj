(ns gp.routes.users
  (:require [compojure.core :refer :all]
            [compojure.coercions :refer :all]
            [noir.response :refer [redirect]]
            [noir.session :as session]
            [clojure.pprint :as pp]
            [liberator.core
             :refer [defresource resource request-method-in]]
            [cheshire.core :refer :all]
            [taoensso.timbre :as timbre
              :refer (log  trace  debug  info  warn  error  fatal  report
                           logf tracef debugf infof warnf errorf fatalf reportf
                           spy get-env log-env)]
            [gp.models.db :as db]
            [gp.views.layout :as layout]
            [gp.routes.misc-utils :as mu]
            [config.setup-db :refer [conn]]
            )
  (:import (java.io StringWriter)))

(defn manage-users-debug
  "Display the list of users."
  []
  (let
      [template-file "GpAppUsersList.html"
       user (mu/ssget :user)]
    (timbre/info "In manage users route.")
    (if user
      (layout/debug {:title "Manage Users"
                     :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                     :js-file "manageUsers.js"
                     :heading "User List: "
                     :find-entity "Find User"
                     :entity-heading "User List:"
                     :plist "ulist"})
      (redirect "/login")
      )))

(defn manage-users-page
  "Display the list of users."
  []
  (let
      [template-file "GpAppUsersList.html"
       user (mu/ssget :user)]
    (timbre/info "In manage users route.")
    (if user
      (layout/common template-file
                     {:title "Manage Users"
                      :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                      :js-file "manageUsers.js"
                      :heading "User List: "
                      :find-entity "Find User"
                      :entity-heading "User List:"
                      :plist "ulist"})
      (redirect "/login"))))

(defn get-user-details
  [uid-string]
  (let
      [e (db/get-user-details-uid conn uid-string)]
    (generate-string e))
  )

(defn add-user-page
  "Add a new user page."
  []
  (let
      [template-file "add-user.html"
       user (mu/ssget :user)]
    (timbre/info "In new add-user route.")
    (if user
      (layout/common template-file
                     {:title "Add User"
                      :css ["/css/bootstrap.min.css" "/css/gpApp.css"]
                      :js-file "add-user.js"
                      })
      (redirect "/login"))))

;; (defn td
;;   [roles]
;;   (let [r (map #(keyword (str "user.role/" %1)) (or (and (vector? roles) roles) [roles]))]
;;     (println (str "roles " roles))
;;     ;;(println (str "r " r))
;;     r
;;     ))  

(defn add-user
  [u-name l-name pass rs]
  (let
      [roles (map #(keyword (str "user.role/" %1)) (or (and (vector? rs) rs) [rs]))]
      (timbre/info "Add User: Params are - ")
      (timbre/info (str "User name = " u-name))
      (timbre/info (str "login name = " l-name))
      (timbre/info (str "password = " pass))
      (db/add-user conn u-name l-name pass roles)
      (redirect "/manage-users")
      )
  )

(defn get-current-logins
  []
  (let
      [log-list
       (db/get-user-list conn [:user/loginName])]
    (generate-string {:login-list log-list}))
  )
  
(defresource current-logins
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (get-current-logins))
  :etag "fixed-etag"
  :available-media-types ["application/json"])

(defn get-user-list
  "Return a list of users from the database, for display by the client."
  []
  (let
      [ulist
       (db/get-user-list
        conn
        [:user/loginName :user/fullName :user/phoneNum :user/sortName :user/uid])]
    (generate-string {
                      :table-id "ulist"
                      :sort-key :user/sortName
                      :link :user/uid
                      :columns {:user/fullName "Name" :user/loginName "Login"}
                      :data-list ulist})))

(defresource get-users
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :get at present
  :allowed-methods [:get]
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :handle-ok (fn [_]
               (get-user-list))
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"])

(defresource remove-user
  ;; Is the service available - can replace true with a boolean function.
  :service-available? true
  :handle-service-not-available
  "service is currently unavailable."
  ;; Only allow :post
  :allowed-methods [:post]
  :malformed? (fn [context]
                (let [params (get-in context [:request :form-params])]
                  (empty? (get params "uid"))))
  ;; Handler for if the method is not allowed.
  :handle-method-not-allowed
  (fn [context]
    (str (get-in context [:request :request-method]) " is not allowed."))
  :post! (fn [context]
           (let [params (get-in context [:request :form-params])
                 uid (get params "uid")]
             (timbre/info (str "remove-user: post! - uid = " uid ))
             (db/del-user-uid conn uid))
           )
  :handle-created (fn [_] (get-user-list))
  :etag "fixed-etag"
  :available-media-types ["text/plain" "application/json"])

(defroutes add-user-routes
  (GET "/manage-users" [] (manage-users-page))
  (GET "/add-user" [] (add-user-page))
  (POST "/add-user" [userName loginName inputPassword role]
        (add-user userName loginName inputPassword role))
  (POST "/remove-user" request remove-user)  
  (GET "/get-users" request get-users) ;; ajax call
  (GET "/user-select/:user-id" [user-id] (get-user-details user-id))
  (GET "/current-logins" request current-logins)  
  )
  ;; (POST "/patient/consult-note" [id content]
  ;;       (handle-save-consult id content)))
