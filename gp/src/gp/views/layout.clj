(ns gp.views.layout
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.element :refer :all]
            [selmer.parser :refer :all]
            [utils.misc-utils :refer :all]))

;; Set the selmer resource path to the projects resources/templates directory
(set-resource-path! (clojure.java.io/resource "templates"))

(defn common-error [& body]
  (html5
   [:head
    [:title "Error: "]
     (include-css "/css/bootstrap.min.css")
    (include-css "/css/gpApp.css")]
   [:body.full
    [:h1 "Error"] body]))


(defn common [temp-name context-map]
  (render-file temp-name context-map))

(defn debug [context-map]
  (render "{% debug %}"
          context-map))

(defn l-form [title & body]
  (html5
   [:head]
   [:title title]
   (include-css "/css/bootstrap.min.css")
   (include-css "/css/gpApp.css")
   [:body.full body]
   ))

