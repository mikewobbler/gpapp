;;
;; Create html for the various nav's on a page.
;;
(ns gp.views.navs
  (:require [hiccup.page :refer [html5 include-css include-js]]
            [hiccup.element :refer :all]
            [utils.misc-utils :refer :all]))

(defn sidebar-head [title]
  [:head
   [:meta{:charset "utf-8"}]
   [:meta {:http-equiv "X-UA-Compatible"  :content "IE=edge"}]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
   [:meta {:name "description" :content ""}]
   [:meta {:name "author" :content ""}]
   [:title title]
   (include-css "/bower_components/bootstrap/dist/css/bootstrap.min.css")
   (include-css "/css/gpApp.css")
   (include-js ["/bower_components/bootstrap/dist/bootstrap.js"
                "/bower_components/jquery/dist/jquery.js"])
   ]
  )

(defn sidebar-brand [href title]
  [:li.sidebar-brand
   [:a {:href href} title]])

(defn sidebar-item [href title]
  [:li
   [:a {:href href} title]])

(defn sidebar-nav []
  [:div#sidebar-wrapper
   [:ul.sidebar-nav
    (sidebar-brand "#" "Tasks:")
    (sidebar-item "#" "Patient Consultation")
    (sidebar-item "#" "Appointments")
    (sidebar-item "#" "Register new user")
    (sidebar-item "#" "Review Records")
    (sidebar-item "/login" "Login as different user")    
    ]])

(defn common-sidebar [title & body]
  (html5
   (sidebar-head title)
   [:body.full
    [:div#wrapper.container
     (sidebar-nav)
     body]]))


;;
;; We want to create the navbar with a list of links, so we need
;; a list of:
;;
;; menu-item = {:title link1-title :url link1-uri}
;;
;; Some navbar items will have dropdowns which are themselves menu-items.
;;
;; Start with a couple of navbar def maps:
;;
(defn dropdown
  [item]
  [:li.dropdown
   [:a.dropdown-toggle {:data-toggle "dropdown" :href "#"
                        :id (:dropdown-id item)} (:title item)
    [:span.caret]]
   [:ul.dropdown-menu {:aria-labelledby (:dropdown-id item)}
    (for [x (:dropdown item)]
      [:li [:a {:href (:href x)} (:title x)]])]])

(defn navbar-item
  "Create the a navbar list element for the supplied item."
  [item]
  (if (contains? item :dropdown)
    (dropdown item)
    [:li [:a {:href (:href item)} (:title item)]]
    ))

(defn navbar-top
  "Create a top of the page nav-bar containing the items decribed in ns."
  [ns]
  (let
      [brand (first ns) rest-items (rest ns)]
    [:div {:class "navbar navbar-default navbar-fixed-top"}
     [:div.container
      [:div.navbar-header
       [:a.navbar-brand {:href (:href brand)} (:title brand)]
       [:button {:type "button" :class "navbar-toggle collapsed"
                 :data-toggle "collapse" :data-target "#navbar"
                 :aria-expanded "false" :aria-controls "navbar"}
        [:span.sr-only "Toggle navigation"]
        [:span.icon-bar] [:span.icon-bar] [:span.icon-bar]]]
      [:div {:class "navbar-collapse collapse" :id "navbar-main"}
       [:ul {:class "nav navbar-nav"}
        (for [item rest-items] (navbar-item item))
        ]
       ]]]))
