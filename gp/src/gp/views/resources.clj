;;
;; Create html strings for including javascript, css and other resources in
;; html files.
;;
;; FIXME: Probably don't need this anymore.
(ns gp.views.resources
  (:require [hiccup.page :refer [html5 include-css]]
            [me.raynes.fs :as fs]
            [hiccup.element :refer :all]
            [utils.fileUtils :refer :all]))

(def js-path "js")
(def requirejs (get-path [js-path "require.js"]))

(defn add-js-require
  "Return a <script data-main=extensionless src=require.js></script> html
  element for including require and a javascript file."
  [mainjs]
  [:script
   {:data-main (get-path [(first (fs/split mainjs)) (fs/name mainjs)]) :src requirejs}]
  )


