(ns patient-db.patdb
  (:require [datomic.api :as d :exclusions [joda-time]]
            [clojure.pprint :refer (pprint)]
            [clojure.instant :as inst]
            [joda-time :as t]
            [clojure.string :as cstr]
            [utils.err-wrap :as err-handler]
            ;;[clj-time.core :as t]
            )
  (:gen-class))

(defn get-conf
  "Read in the databse configuration from the file specified by path."
  [path]
  (read-string (slurp path)))

(def conn nil)

(defn sortable [name]
  "Rearrange a name so that sorting can occur starting with the surname."
  (let
      [name-list (cstr/split name #" ")
       surname (last name-list)]
    (cstr/join " " (conj (drop-last name-list) surname))
    )
  )

;; Patient Map should look like this:
;;
;; {
;;     :name  "Patient's name",
;;     :gender  - Enumerated value
;;     :dob - Date of birth
;;     :phoneNum  - Phone Number
;;     :address - Adress.
;;     :medicareNumber - The medicare number of the patient.
;; }
;;
;; NOTE: Some of the fields can be empty, including phone, address and
;; medicareNumber
;;
;; Database attributes we need to generate include:
;; :patient/startDate - Today's date.
;; :patient/pid - A squuid
;; :consultNotes - An empty or arbitrary string.
;; :testResults - An empty string - or just "Test Results"
;; :correspondence - An empty string or just "Correspondence"
;; :billing - An empty string or just "Billing"
;; :appointments - empty - don't do anything.
;;
(defn add-patient
  "Add a patient to the patient database."
  [patient-map]
  {:pre [(and (map? patient-map)
              (contains? patient-map :last-name)
              (contains? patient-map :gender))]
   :post [true]}
  (let [
        first-name (patient-map :first-name "")
        last-name  (patient-map :last-name)
        full-name (str first-name " " last-name)
        gender (cond (= (patient-map :gender) "Male") :person.gender/Male
                     (= (patient-map :gender) "Female") :person.gender/Female
                     :else :person.gender/Other)
        dob (java.util.Date. (patient-map :dob))
        start-date (java.util.Date. (patient-map :start-date))
        pid (patient-map :pid (d/squuid))
        ]
    @(d/transact conn [{:db/id (d/tempid :db.part/user)
                        :patient/firstName first-name
                        :patient/lastName  last-name
                        :patient/fullName full-name
                        :patient/sortName (sortable full-name)
                        :patient/title    (patient-map :title "")
                        :patient/gender   gender
                        :patient/phoneNum (patient-map :phone "")
                        :patient/streetAddress (patient-map :street-address "")
                        :patient/city     (patient-map :city "")
                        :patient/state    (patient-map :state "")
                        :patient/country  (patient-map :country "")
                        :patient/postCode (patient-map :post-code "")
                        :patient/email    (patient-map :email "")
                        :patient/medicareNumber (patient-map :medicare-number "")
                        :patient/dob     dob
                        :patient/startDate start-date
                        :patient/consultNotes    (patient-map :consult-notes "")
                        :patient/testResults     (patient-map :test-results "")
                        :patient/diagnosis       (patient-map :diagnosis "")
                        :patient/correspondence  (patient-map :correspondence "")
                        :patient/billing         (patient-map :billing "")
                        :patient/pid pid
                        }])))

(defn get-patient-names
  "List out all patient names."
  []
  (seq
   (d/q '[:find ?name
              :where [?p :patient/fullName ?name]]
            (d/db conn)
            ))
  )

(defn get-patient-list
  "List out the values of all the given attributes."
  [attr]
  (seq
   (d/q '[:find ?name
          :in $ ?attr
          :where [_ ?attr ?name]]
        (d/db conn)
        attr
        ))
  )

;; Postgresql connection management.
(defn get-connection
  "Return a database connection using the uri supplied in the db-config."
  [db-config]
  (let
      [uri (:transactor-uri db-config)]
    (d/connect uri)
    ))

(defn add-user-temp
  "Add a user to the patient database."
  [user-name pword]
  @(d/transact conn [{:db/id (d/tempid :db.part/user)
                      :user/loginName user-name
                      :user/password pword}]))


(defn load-schema
  "Load the database schema from db-config into the database."
  [db-config]
  (let
      [uri (:transactor-uri db-config)
       conn (d/connect uri)
       patient-schema (load-file (:schema-file db-config))
       user-schema (load-file (:user-schema-file db-config))]
       (d/transact conn patient-schema)
       (d/transact conn user-schema)))

(def safe-get-conn
  "Wrap get-connection in an exception handler."
  (err-handler/wrap-in-ehandler get-connection
     [java.util.concurrent.ExecutionException java.sql.SQLException Exception]))

(defn set-conn
  "Set the connection object."
  ([path] (def conn (safe-get-conn (get-conf path)))))


;; Only for creation.
(defn create-new-psql-db
  "Create a new database in postgress with the schema and uri from db-config."
  [db-config]
  (let
      [uri (:transactor-ufi db-config)]
    (d/create-database uri)
    (let [conn (d/connect uri)
          schema (load-file (:schema-file db-config))]
      (d/transact conn schema)
      (set-conn conn)
      )))

;; For development and testing.
(defn create-empty-in-memory-db []
  (let [uri "datomic:mem://patient-db"]
    (d/delete-database uri)
    (d/create-database uri)
    (let [conn (d/connect uri)
          schema (load-file "resources/schema.edn")]
      (d/transact conn schema)
      conn
      )))
