(ns utils.db-utils
  (:require
   [clojure.java.jdbc :as jdbc]
   [datomic.api :as d :exclusions [joda-time]]
   [clojure.pprint :refer (pprint)]
   [clj-commons-exec :as exec]
   [clojure.java.io :as io]   
   [utils.fileUtils :as fu]
   [utils.shell-utils :refer :all]
   [config.app-config :refer [gp-config]]
   )
  )

(def db-spec {
              :classname   "org.postgresql.Driver"
              :subprotocol "postgresql"
              :subname     "//localhost:5432/postgres"
              })

;; NOTE: can also use this.
;;(def db-spec "jdbc:postgresql://localhost:5432/postgres?user=postgres&password=postgres")

(defn db-con [] (jdbc/get-connection db-spec))

(defn get-current-dbs
  "Return a list of databases containeed in the postgres instance."
  [db-con db-spec]
  (jdbc/with-db-connection [db-con db-spec]
    (map #(:datname %1) (jdbc/query db-con ["SELECT datname FROM pg_database"]))    
    )
  )

(defn show-current-dbs
  []
  (pprint (get-current-dbs (db-con) db-spec)))

(defn show-db-status
  [db-name]
  (let
      [db {
           :classname   "org.postgresql.Driver"
           :subprotocol "postgresql"
           :subname     (str "//localhost:5432/" db-name)
           }
       conn (jdbc/get-connection db)]
    (jdbc/with-db-connection [conn db]
      (pprint 
       (jdbc/query conn (str "SELECT * FROM pg_stat_activity WHERE datname='" db-name "';"))))
  ))

(defn role-exists?
  "Return a list of databases containeed in the postgres instance."
  [db-con db-spec role]
  (jdbc/with-db-connection [db-con db-spec]
    (let
        [roles (map #(:datname %1) (jdbc/query db-con ["SELECT datname FROM pg_database"]))]
      (some #{role} roles))
      )
    )

(defn create-db-stmnt [db-name]
  (str "CREATE DATABASE " db-name
       " WITH OWNER = postgres
      TEMPLATE template0
      ENCODING = 'UTF8'
      TABLESPACE = pg_default
      LC_COLLATE = 'en_US.UTF-8'
      LC_CTYPE = 'en_US.UTF-8'
      CONNECTION LIMIT = -1;")
  )

(defn create-new-datomic-db
  "Create a new database for use by datomic."
  [conn db new-db-name]
  (jdbc/with-db-connection [conn db]
    (let [existing (map #(:datname %1) (jdbc/query conn ["SELECT datname FROM pg_database"]))]
      (if (some #{new-db-name} existing) (println (str "Error: " new-db-name " already exists.!"))
          (do
            (println "Create the Database")
            (jdbc/execute! conn (create-db-stmnt new-db-name) {:transaction? false}))
        )))
  )

(defn drop-db
  "Drop the named db."
  [db-name]
  (let [existing (map #(:datname %1) (jdbc/query db-spec ["SELECT datname FROM pg_database"]))]
      (if (some #{db-name} existing)
        (jdbc/execute! db-spec (str "DROP DATABASE " db-name ";") {:transaction? false})
        (println (str "Error: " db-name " does not exist!"))
        )
      ))

(def ct-datomic-table
  (str
   "CREATE TABLE datomic_kvs
   (
       id text NOT NULL,
       rev integer,
       map text,
       val bytea,
       CONSTRAINT pk_id PRIMARY KEY (id )
     )
     WITH (
     OIDS=FALSE
     );")
  )

(def at-datomic-table
  (str
   "ALTER TABLE datomic_kvs
    OWNER TO postgres;
    GRANT ALL ON TABLE datomic_kvs TO postgres;
    GRANT ALL ON TABLE datomic_kvs TO public;"
   ))

(def dt-user
  (str "CREATE ROLE datomic LOGIN PASSWORD 'datomic';"))

(defn create-datomic-table
   "Create a new datomic table."
  [db-name]
  (let [db {
                 :classname   "org.postgresql.Driver"
                 :subprotocol "postgresql"
                 :subname     (str "//localhost:5432/" db-name)
                 }
        conn (jdbc/get-connection db)]
    (jdbc/with-db-connection [conn db]
      (jdbc/execute! conn ct-datomic-table {:transaction? false})
      (jdbc/execute! conn at-datomic-table {:transaction? false})
      (when (not (role-exists? conn db "datomic")) 
        (jdbc/execute! conn dt-user {:transaction? true}))
      ))
  )

(defn find-connection [db-name]
  (let
      [db {
           :classname   "org.postgresql.Driver"
           :subprotocol "postgresql"
           :subname     (str "//localhost:5432/" db-name)
           }]
    (jdbc/db-find-connection db)
    )
  )

;; new test datomic database
(defn create-new-pgres-test-db [new-db-name]
  (let
      [db-conn (jdbc/get-connection db-spec)]
    (if (some #{new-db-name} (get-current-dbs db-conn db-spec))
      (println "Db exists - do nothing.")
      (do
        (println "Db doesn't exist - create it.")
        (if (create-new-datomic-db db-conn db-spec new-db-name)
          (let
              [db {
                 :classname   "org.postgresql.Driver"
                 :subprotocol "postgresql"
                 :subname     (str "//localhost:5432/" new-db-name)
                   }
               conn (jdbc/get-connection db)]
            (jdbc/with-db-connection [conn db]
              (jdbc/execute! conn ct-datomic-table {:transaction? false})
              (jdbc/execute! conn at-datomic-table {:transaction? false})
              (when (not (role-exists? conn db "datomic")) 
                (jdbc/execute! conn dt-user {:transaction? true}))
              
              ))
          (println (str "Failed to create " new-db-name)))
        ))))

(defn replace-db-name
  [db-name s]
  (let
      [match (re-matches #"(^sql-url.*)/\w*" s)]
    (if match (str (nth match 1) "/" db-name "\n") 
        (str s "\n"))))


(defn create-transactor-props
  "Create a transactor properties file for database db-name."
  [db-name if of]
  (fu/pmap-file (partial replace-db-name db-name) if of))


;; (defn start-datomic-new-db
;;   [new-db-name]
;;   (create-new-pgres-test-db new-db-name)
;;   (let
;;       [new-props-name (str (:transactor-props gp-config) "." new-db-name)
;;        dt-dir  (eval (:transactor-dir gp-config))
;;        dt-log  (:transactor-log gp-config)
;;        ostream (io/output-stream dt-log)
;;        transactor-uri
;;        (clojure.string/replace (:transactor-uri gp-config) #"/datomic\?" (str "/" new-db-name "?"))]
;;     (create-transactor-props new-db-name
;;                              (str dt-dir "/"(:transactor-props gp-config))
;;                              (str dt-dir "/" new-props-name))
;;     (kill-procs "datomic-transactor")
;;     (exec/sh [(fu/get-path ["bin" "transactor"]) new-props-name]
;;              {:dir dt-dir :out ostream :err ostream
;;               :close-out? true :close-err? true
;;               :shutdown true}
;;              )
;;     ;; note need a delay here:
;;     (Thread/sleep 20000)
;;     (d/create-database transactor-uri)
;;     (def conn (d/connect transactor-uri))
;;     ))

