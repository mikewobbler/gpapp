(ns utils.fileStore
  (:use clj-jgit.porcelain)
  (:use clj-jgit.querying)
  (:use clj-jgit.internal)
  (:use clj-jgit.util)
  (:use utils.fileUtils)
  (:use utils.jgit-utils)
  (:require [me.raynes.fs :as fs])
  (:require [clojure.java.io :as io])
  (:import java.io.File)
  (:import org.eclipse.jgit.lib.Constants)
  (:import org.eclipse.jgit.treewalk.filter.PathFilter)
  (:gen-class))

(defn validate-git-repo
  "Report on whether pathString points to a valid Git repository."
  [repo-path]
  {:pre [(fs/exists? (get-path [repo-path ".git"]))] :post [true]}
  (if
      (try
        (load-repo repo-path)
        true
        (catch java.io.FileNotFoundException e
          (println (str "validate-git-repo: Path error: " (.getMessage e))))
        (catch Exception e
          (println (str "validate-git-repo: Exception: " (.getMessage e))))
        )
    true
    false
    )
  )

(defn create-git-repo
  "Create a new empty local git repository with the supplied uri.
  The uri should be a file scheme uri."
  [repo-path]
  {:pre [(not (fs/exists? repo-path))]
   :post [(fs/exists? (get-path [repo-path ".git"])),
          (validate-git-repo repo-path)]
   }
  (if-let
      [f (File. repo-path)]
    (do
      (io/make-parents f)
      (git-init f)
      (let [gr (load-repo f)]
        (git-commit gr (format "Added empty repository.")
                    {:name "med-files" :email "mikewobbler@gmail.com"})
        gr))
  false))


(defn delete-git-repo
  "Delete a git repo. Used for testing."
  [git-repo]
  {:pre [(or (and (string? git-repo) (fs/exists? git-repo)
                  (fs/exists? (get-path [git-repo ".git"])))
             (instance? org.eclipse.jgit.api.Git git-repo)
             true)]
   :post [(or (and (string? git-repo) (not (fs/exists? git-repo)))
              (and (instance? org.eclipse.jgit.api.Git git-repo)
                   (not (fs/exists? git-repo))))]
   }
  (cond (string? git-repo) (delTree git-repo)
        (instance? org.eclipse.jgit.api.Git git-repo)
        (delTree (.getParent (.getDirectory (.getRepository git-repo))))
        :else false)
  )

(defn in-repo
  "Determine if fpath is in the git repository supplied in gitRepo."
  [repo-path fpath]
  {:pre [(fs/exists? (get-path [repo-path ".git"]))]
   :post [true]}
  (let
      [
       t-walk (get-tree-walks-for-repo repo-path)
       fp (get-relative-path repo-path fpath)
       ]
    ;; Just return true, until we can work out why t-walk isn't working.
    (if true true
        (do (add-filter t-walk (PathFilter/create fp))
            (try
              (.next t-walk)
              (catch org.eclipse.jgit.errors.MissingObjectException e
                (println (str "Caught Missing Object Exception: " (.getMessage e)))
                false
                )
              (catch org.eclipse.jgit.errors.IncorrectObjectTypeException e
                (println (str "Caught Incorrect Object Type Exception: " (.getMessage e)))
                false)
              (catch org.eclipse.jgit.errors.CorruptObjectException e
                (println (str "Caught Corrupt Object Exception: " (.getMessage e)))
                false)
              (catch java.io.IOException e
                (println (str "Caught IO Exception: " (.getMessage e)))
                false)
              )))))

(defn add-file-to-git
  "Add the file supplied in dir-list to the git repo."
  [repo-path dir-list]
  (let [gr (load-repo repo-path) fpath (get-path dir-list)]
    (and
     (fs/create (fs/file fpath))
     (git-add gr fpath)
     (git-commit gr (format "Added Empty File %s." fpath)
                 {:name "med-files" :email "mikewobbler@gmail.com"})
     )
    )
  )

(defn add-dir-to-git
  "Add a directory to the git repo."
  [repo-path dir-path]
  {:pre [(and (fs/exists? (get-path [repo-path ".git"]))
              (not (fs/exists? dir-path))
              ;;(not (in-repo repo-path (get-path [dir-path "Readme.md"])))
              )]
   :post [(in-repo repo-path (get-path [dir-path "Readme.md"]))]}
  (let [gr (load-repo repo-path)]
    (fs/mkdir dir-path)
    (add-file-to-git repo-path [dir-path "Readme.md"])
    (git-add gr dir-path)
    (git-commit gr (format "Added directory %s." dir-path)
                {:name "med-files" :email "mikewobbler@gmail.com"}))
  )

  
;; (defn t []
;;   (let [gr-path (get-path (list "/tmp" "tg"))
;;         dir (get-path (list gr-path "testDir"))
;;         fl  (get-path (list dir "testFile.txt"))
;;         gr (create-git-repo gr-path)]
;;     (if (and
;;          (add-dir-to-git gr-path dir)
;;          (add-file-to-git gr-path [fl])
;;          )
;;       (delete-git-repo gr-path)
;;       false
;;       )
;;     ))
