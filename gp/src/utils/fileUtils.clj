(ns utils.fileUtils
  (:require [me.raynes.fs :as fs])
  (:require [clojure.java.io :as io])
  (:require [clojure.string :as str])  
  (:import java.io.File)
  (:import java.nio.file.Files)
  (:import java.nio.file.Paths)
  (:gen-class))

(defn collectTree
  "Collect all directories and files from dirs and files into a map."
  [root dirs files]
  {:pre [(= java.io.File (class root)),
         (= clojure.lang.PersistentHashSet (class dirs)),
         (= clojure.lang.PersistentHashSet (class files))]
   :post [true]
   }
  {:root root :dirs dirs :files files}
  )

(defn delTree
  "Delete all files and directories under root - including root itself."
  [rootDir]
  {:pre [(fs/exists? rootDir)]
   :post [(not (fs/exists? rootDir))] }
  (let
      [dirMap (reverse (fs/walk collectTree rootDir))]
    ;;(clojure.pprint/pprint dirMap)
    (doseq [dirInfo dirMap]
      (let [rootString (.getPath (get dirInfo :root))]
        (doseq [file (get dirInfo :files)]
          (fs/delete (Paths/get rootString (into-array String [file]))))
        (doseq [dir (get dirInfo :dirs)]
          (fs/delete-dir (Paths/get rootString (into-array String [dir]))))
        )
      )
    (fs/delete-dir rootDir)
    )
  true
  )

(defn get-path
  "Return the path created by joining the elements in fList in a filesystem
  appropriate way."
  [fList]
  {:pre [(reduce #(and (string? %2) %1) true fList)]
   :post [(string? %)]}
  (let [[f & rest] fList]
    (.toString (Paths/get f (into-array String rest)))))

(defn get-relative-path
  "If the base-path is a parent of target-path, return the difference, that is
 return the string created by subtracting base-path from target-path.

NOTE: We're assumeing that target-path is only potentially a descendent of
 base-path if it and base-path are absolute paths."
  [base-path target-path]
  (let
      [bp (str/replace base-path #"[^/]$" #(str %1 "/"))]
      (str/replace-first target-path bp "")
    )
  )

(defn pmap-file
  "Process input-file in parallel, applying processing-fn to each row, and
  outputting into the output-file"
  [processing-fn input-file output-file]
  (with-open [rdr (io/reader input-file)
              wtr (io/writer output-file)]
    (let [lines (line-seq rdr)]
      (dorun
       (map #(.write wtr %1)
            (pmap processing-fn lines))))))


