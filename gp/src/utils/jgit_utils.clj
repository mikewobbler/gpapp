(ns utils.jgit-utils
  (:use clj-jgit.internal)
  (:use clj-jgit.porcelain)
  (:use clj-jgit.querying)
  (:use utils.fileUtils)  
  (:require [me.raynes.fs :as fs])
  (:require [clojure.java.io :as io])
  (:import java.io.File)  
  (:import org.eclipse.jgit.lib.Constants)
  (:import org.eclipse.jgit.revwalk.RevWalk)
  (:import org.eclipse.jgit.revwalk.RevTree)
  (:import org.eclipse.jgit.revwalk.RevCommit)
  (:import org.eclipse.jgit.treewalk.TreeWalk)
  (:import org.eclipse.jgit.treewalk.filter.PathFilter)
  (:import org.eclipse.jgit.treewalk.filter.TreeFilter)  
  (:gen-class))

(defn get-repo
  "Returns the repository object for the git repository located at repo-dir."
  [repo-dir]
  (.getRepository (load-repo repo-dir))
  )


(defn get-last-commit-id
  "Returns the last commit-id of repo."
  [repo]
  (.resolve repo Constants/HEAD)
  )

(defn get-rev-walk
  "Returns a revision walk object."
  [repo]
  (RevWalk. repo)
  )

(defn get-commit
  "Returns the commit object corresponding to the supplied commit id."
  [rev-walk commit-id]
  (.parseCommit rev-walk commit-id)
  )

(defn get-tree
  "Return the RevTree for the supplied commit."
  [commit]
  (.getTree commit)
  )

(defn get-tree-walk
  "Return a tree walker."
  [repo]
  (TreeWalk. repo)
  )

(defn add-tree
  "Add RevTree tree to tree-walk."
  [tree-walk tree]
  (.addTree tree-walk tree)
  )

(defn get-parent-commits
  "Return RevTree's for each of the parents of commit."
  [commit rev-walk]
  (map #(.parseCommit rev-walk %1) (.getParents commit))
  )

(defn add-trees
  "Recursively add RevTrees for all Commits in commits and all parents of those
  commits."
  [tree-walk rev-walk commits]
  (when (not-empty commits)
    (let [[c & more] commits]
      (add-tree tree-walk (get-tree c))
      (add-trees tree-walk rev-walk (get-parent-commits c rev-walk))
      (when more (recur tree-walk rev-walk more))
      ))
  )

(defn add-filter
  "Add a filter to a tree-walk."
  [tree-walk fltr]
  (.setFilter tree-walk fltr)
  )

(defn get-tree-walks-for-repo
  "Return a TreeWalk with all RevTree's starting from the HEAD attached to
it."
  [repo-path]
  (let
      [repo (get-repo repo-path)
       last-commit-id (get-last-commit-id repo)
       r-walk (get-rev-walk repo)
       commit (get-commit r-walk last-commit-id)
       t-walk (get-tree-walk repo)
       ]
    (.setRecursive t-walk true)
    (add-trees t-walk r-walk [commit])
    ;;(.enterSubtree t-walk)
    t-walk
    )
  )

(defn get-tree-walk-for-repo
    "Return a TreeWalk with the HEAD RevTree attached to it."
     [repo-path]
     (let
         [repo (get-repo repo-path)
          last-commit-id (get-last-commit-id repo)
          r-walk (get-rev-walk repo)
          commit (get-commit r-walk last-commit-id)
          t-walk (get-tree-walk repo)
          ]
       (.setRecursive t-walk true)
       (add-tree t-walk (get-tree commit))
       ;;(.enterSubtree t-walk)
       t-walk
       )
     )


(defn enum-tree
  "For debugging - print out all paths found."
  [tree-walk]
  ;;(.enterSubtree tree-walk)
  (.setRecursive tree-walk true)
  (loop [next (.next tree-walk)]
    (when next
      (do
        (when (.isSubtree tree-walk) (.enterSubtree tree-walk))
        (printf "IsSubtree: %s - Got: %s\n" (.isSubtree tree-walk) (.getPathString tree-walk))
        (recur (.next tree-walk))))
    )
  )
