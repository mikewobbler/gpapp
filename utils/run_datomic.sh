## Start postgres
pg_ctl start -D /usr/local/var/postgres > /tmp/pgress.log 2>&1 &

## start gpg-agent
#eval $(gpg-agent --daemon)
sleep 10
## start datomic transactor
(cd ../external/datomic-pro-0.9.5350 && bin/transactor sql-transactor.properties > /tmp/transactor.txt 2>&1 &)


