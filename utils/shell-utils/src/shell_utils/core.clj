(ns shell-utils.core
  (:require
   [clj-commons-exec :as exec]
   [clojure.string :as str])
  )

(defn get-procs
  "Return a sequence containing the lines output by ps -ef that match the
 supplied regular expression rexp."
  [rexp]
  (let
      [procs (exec/sh ["ps" "-ef"])
       re-pat (re-pattern (str "(?im)^.*" rexp ".*$"))]
    (re-seq re-pat (:out @procs))))
  

(defn show-procs
  "Print ps output that matches the regular expression rexp to the console."
  [rexp]
  (println (str/join "\n" (get-procs rexp)))) 
  

(defn kill-procs
  "Kill each process that matches the regexp rexp in ps output."
  [rexp]
  (let [procs (get-procs rexp)
        pids (map #(nth (str/split % #"\s+") 2) procs)
        cmd-prefix ["kill" "-9"]]
    (doseq [pid pids]
      (exec/sh (conj cmd-prefix pid)))))


(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
